﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("property")]
    public class Property
    {
        public string key { get; set; }
        public string value { get; set; }

        public Property() { }


    }
}
