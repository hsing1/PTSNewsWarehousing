﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using NewsBDArchive.Models;

namespace NewsBDArchive
{
    public partial class BDArchiveForm : Form
    {
        delegate void PrintHandler(TextBox tb, string text);
        delegate void ClearHandler(TextBox tb);
        private object _syncRoot = new object();

        private System.Threading.Timer _ThreadTimerJobs = null;

        WebServiceControl ws = null;
        public string basePathString { set; get; }
        //string uriBasePath  = "http://SERVERNAME:PORT/library/basePaths?applicationId=1eaed24c-8ff8-4deb-a595-990b6a1a73bd";
        string uriBasePath = "http://SERVERNAME:PORT/library/basePaths?applicationId=APPLICATIONID";
        string uriFile = "http://SERVERNAME:PORT/library/files?applicationId=APPLICATIONID";
        string uriCartridge = "http://SERVERNAME:PORT/library/cartridges?applicationId=APPLICATIONID";
        string uriCatalog1 = "http://SERVERNAME:PORT/library/catalogs?cartridgeId=&isRecursive=false&applicationId=APPLICATIONID";
        string uriCatalog2 = "http://SERVERNAME:PORT/library/catalogs?cartridgeId=&parentId=&isRecursive=false&applicationId=APPLICATIONID";
        string uriCatalog3 = "http://SERVERNAME:PORT/library/catalogs?applicationId=APPLICATIONID";
        string uriJobGroup = "http://SERVERNAME:PORT/library/jobGroups?sortName=createTime&isAscending=false&applicationId=APPLICATIONID";
        string uriTray = "http://SERVERNAME:PORT/library/tray?applicationId=APPLICATIONID";
        string uriDrive = "http://SERVERNAME:PORT/library/drives?applicationId=APPLICATIONID";
        string uriSystem = "http://SERVERNAME:PORT/library/systems?applicationId=APPLICATIONID";
        string uriJob = "http://SERVERNAME:PORT/library/jobs?sortName=createTime&isAscending=false&applicationId=APPLICATIONID";

        public BDArchiveForm()
        {
            InitializeComponent();

            initWebService();

            _ThreadTimerJobs = new System.Threading.Timer(new System.Threading.TimerCallback(GetJobsStatus), null, 0, 1000 * 60 * 3);
        }

        private bool initWebService()
        {
            if (ws == null)
                ws = new WebServiceControl();

            string apID = "0D8CDAD8-82B9-4362-8008-ACAEE9983911";
            string pn = "8080";
            string sn = "10.13.210.203";
            uriBasePath = uriBasePath.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriFile = uriFile.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriCartridge = uriCartridge.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriCatalog1 = uriCatalog1.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriCatalog2 = uriCatalog2.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriCatalog3 = uriCatalog3.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriJobGroup = uriJobGroup.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriTray = uriTray.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriDrive = uriDrive.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriSystem = uriSystem.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);
            uriJob = uriJob.Replace("SERVERNAME", sn).Replace("PORT", pn).Replace("APPLICATIONID", apID);

            ws.MessageShow += Ws_MessageShow; ;

            return true;
        }

        private void Ws_MessageShow(object sender, MessageEventArgs e)
        {
            PrintMessage(txtMessage, e.Message);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            await Task.Run(() => RunBurnBD());

            ((Button)sender).Enabled = true;


        }

        void RunGetBDInfo()
        {
            if (initWebService() == false)
                return;

            CartridgeList list = ws.getCartriges(uriCartridge, "");

            if (list != null)
            {
                var cartridge = new List<CartridgeInfo>();

                var barcodes = list.cartridges.Select(x => x.barcode).ToList();

                using (var blue = new BlueArchiveEntities())
                {
                    var ori = blue.CartridgeInfo.Where(x => barcodes.Contains(x.Barcode)).Select(x => x).ToList();

                    foreach (Cartridge c in list.cartridges)
                    {
                        var item = ori.Where(x => x.Barcode == c.barcode).Select(x => x).FirstOrDefault();

                        if (item == null)
                        {
                            cartridge.Add(new CartridgeInfo()
                            {
                                FileManagerID = int.Parse(c.id),
                                Barcode = c.barcode,
                                MediaId = c.mediaId,
                                FreeSpace = long.Parse(c.free),
                                TotalSpace = long.Parse(c.capacity),
                                Location = c.location,
                                Kind = c.kind,
                                ArchiveType = c.barcode.Substring(c.barcode.Length - 1, 1)
                            });
                        }
                        else
                        {
                            item.FreeSpace = long.Parse(c.free);
                            item.Location = c.location;
                            item.ArchiveType = c.barcode.Substring(c.barcode.Length - 1, 1);
                        }
                    }

                    if (cartridge.Count > 0)
                    {
                        blue.CartridgeInfo.AddRange(cartridge);
                    }

                    blue.SaveChanges();
                }
            }
        }

        void RunBurnBD()
        {
            if (initWebService() == false)
                return;

            Task t = Task.Run(() => RunGetBDInfo());
            t.Wait();

            var blue = new BlueArchiveEntities();
            var news = new PTSNewsWarehousingEntities();

            var bdid = blue.CartridgeInfo
                           .AsNoTracking()
                           .Where(x => x.Location == "STORAGE SLOT" && x.Kind == "OTHERS")
                           .Where(x => x.Barcode.Substring(0, 2) == "HN")
                           .Where(x => x.ArchiveType == "A")
                           .OrderBy(x => x.Barcode)
                           .Select(x => x)
                           .ToList();

            if (bdid == null || bdid.Count == 0) return;

            var files = news.VideoMeta
                            .Where(x => x.ArchivePosition == 2 && x.BDBurnStatus == 0)
                            .Select(x => x)
                            .ToList();

            if (files == null || files.Count == 0) return;

            var i = 1;

            foreach (var bd in bdid.OrderBy(x => x.FreeSpace))
            {
                if (i >= 2) break;

                PrintMessage(txtMessage, $"{bd.Barcode}:{FormatSize(bd.FreeSpace)}");

                var preBurn = new List<VideoMeta>();
                var fileList = new List<string>();
                long size = 0;

                foreach (var file in files)
                {
                    var info = new FileInfo(Path.Combine(file.ArchivePath, file.ArchiveName));

                    size += info.Length;

                    PrintMessage(txtMessage, $"{file.ArchiveName}:{FormatSize(info.Length)} => {FormatSize(bd.FreeSpace)} - {FormatSize(size)} = {FormatSize(bd.FreeSpace - size)}");

                    if (size > (bd.FreeSpace * 0.95)) break;
                    else
                    {
                        file.BDBarcode = bd.Barcode;
                        
                        file.ArchivePosition = 3;
                        file.BDBurnStatus = 1;

                        preBurn.Add(file);

                        PrintMessage(txtMessage, file.ArchivePath.Replace(@"\\10.13.200.30\", @"/").Replace("\\", "/") + "/" + file.ArchiveName);
                        fileList.Add(file.ArchivePath.Replace(@"\\10.13.200.30\", @"/").Replace("\\", "/") + "/" + file.ArchiveName);
                    }
                }

                if (preBurn == null || preBurn.Count == 0) continue;

                CatalogAddRequest request = new CatalogAddRequest();
                request.cartridgeId = bd.FileManagerID.ToString();
                request.catalogId = "";
                request.filePaths = fileList;

                CatalogList catalogList = ws.addCatalogs(uriCatalog3, request);
                if (catalogList != null)
                {
                    string jobId = catalogList.catalogs[0].jobId;
                    Job job = ws.getJob(uriJob, jobId);

                    news.ArchiveJob.Add(new ArchiveJob()
                    {
                        JobGroupID = job.groupId,
                        JobStatus = job.status,
                        JobType = job.type,
                        CreateTime = utcToLocalTime(job.createTime),
                        StartTime = utcToLocalTime(job.startTime),
                        EndTime = utcToLocalTime(job.endTime)
                    });


                    news.SaveChanges();

                    files = files.Except(preBurn).ToList();
                    i++;
                    PrintMessage(txtMessage, "Archive job was submitted. job group Id:" + job.groupId);

                }
                else
                {
                    PrintMessage(txtMessage, "Archive job was not submitted");
                }

                

            }
            

            blue.Dispose();
            news.Dispose();


            Task tt = Task.Run(() => GetJobsStatus());
            tt.Wait();
        }

        void GetJobsStatus(object State = null)
        {
            if (initWebService() == false)
                return;
            ClearMessage(txtMessage);
            PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");

            var DB = new PTSNewsWarehousingEntities();

            List<string> res;

            if (State is List<string>) res = (List<string>)State;
            else
            {

                res = DB.ArchiveJob
                        .Where(x => x.JobType == "ARCHIVE")
                        .Where(x => x.IsFinal == false)
                        .Select(x => x.JobGroupID)
                        .ToList();

            }


            foreach (var id in res)
            {
                PrintMessage(txtMessage, $"JobID:{id}");

                JobGroupList list = ws.getJobGroups(uriJobGroup);

                var jobgroup = list.jobGroups.Where(x => x.id == id).Select(x => x).FirstOrDefault();

                if (jobgroup != null)
                {


                    var item = DB.ArchiveJob
                                 .Where(x => x.JobGroupID == id)
                                 .Select(x => x)
                                 .FirstOrDefault();

                    if (item != null)
                    {
                        item.JobStatus = jobgroup.status;
                        item.StartTime = utcToLocalTime(jobgroup.startTime);
                        item.EndTime = utcToLocalTime(jobgroup.endTime);

                        if (!item.IsFinal)
                        {
                            if (jobgroup.status == "FINISHED" || jobgroup.status == "PARTIALLY FINISHED" || jobgroup.status == "FAILED")
                            {
                                item.IsFinal = true;
                            }
                        }

                        DB.SaveChanges();
                    }


                    string URL = uriJob.Replace("jobs?", "jobs?groupId=" + id + "&");
                    JobList joblist = ws.getJobs(URL);
                    if (joblist != null)
                    {

                        var items = joblist.jobs.OrderBy(x => x.progress).Select(x => x.catalogName).ToList();


                        foreach (var video in DB.VideoMeta.Where(x => items.Contains(x.ArchiveName)).Select(x => x))
                        {
                            var job = joblist.jobs.Where(x => x.catalogName == video.ArchiveName).Select(x => x).FirstOrDefault();

                            switch (job.status.ToUpper())
                            {
                                case "REGISTERED":
                                case "STANDBY":
                                    video.BDBurnStatus = 2;
                                    break;
                                case "PROCESSING":
                                    video.BDBurnStatus = 3;
                                    break;
                                case "FINISHED":
                                    video.BDBurnStatus = 4;
                                    break;
                                case "FAILED":
                                default:
                                    video.BDBurnStatus = 99;
                                    break;
                            }

                            PrintMessage(txtMessage, job.catalogName + "--" + job.progress + "--" + job.status);
                        }

                        DB.SaveChanges();

                    }


                }
            }
        }

        DateTime? utcToLocalTime(string UTC)
        {
            if (UTC == null || UTC == "")
                return null;

            DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            baseTime = baseTime.AddMilliseconds(Int64.Parse(UTC));
            TimeZoneInfo tzi = TimeZoneInfo.Local;
            return TimeZoneInfo.ConvertTimeFromUtc(baseTime, tzi);
        }
        string FormatSize(long num)
        {
            return Math.Round(num / 1024.0 / 1024.0 / 1024.0, 3, MidpointRounding.AwayFromZero).ToString() + "GB";
        }
        void PrintMessage(TextBox tb, string text)
        {
            //判斷這個物件是否在同一個執行緒上
            if (tb.InvokeRequired)
            {
                //當InvokeRequired為true時，表示在不同的執行緒上，所以進行委派的動作!!
                PrintHandler ph = new PrintHandler(PrintMessage);
                tb.Invoke(ph, tb, text);
            }
            else
            {
                lock (_syncRoot)
                {
                    //表示在同一個執行緒上了，所以可以正常的呼叫到這個物件
                    tb.AppendText(text + Environment.NewLine);
                }

            }
        }
        void ClearMessage(TextBox tb)
        {
            //判斷這個物件是否在同一個執行緒上
            if (tb.InvokeRequired)
            {
                //當InvokeRequired為true時，表示在不同的執行緒上，所以進行委派的動作!!
                ClearHandler ch = new ClearHandler(ClearMessage);
                tb.Invoke(ch, tb);
            }
            else
            {
                lock (_syncRoot)
                {
                    //表示在同一個執行緒上了，所以可以正常的呼叫到這個物件
                    tb.Text = "";
                }

            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            List<string> value = null;
            
            if(!string.IsNullOrEmpty(textBox1.Text)) value = textBox1.Text.Split(',').ToList();

            await Task.Run(() => GetJobsStatus(value));

            ((Button)sender).Enabled = true;
        }
    }
}
