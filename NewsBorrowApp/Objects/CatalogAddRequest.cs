﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("catalogAddRequest")]
    public class CatalogAddRequest
    {
        public string cartridgeId { get; set; }

        [System.Xml.Serialization.XmlArrayItem("filePath")]
        //[System.Xml.Serialization.XmlElement("filePath")]
        public List<string> filePaths { get; set; }
        public string catalogId { get; set; }

        [System.Xml.Serialization.XmlArrayItem("property")]
        //[System.Xml.Serialization.XmlElement("property")]
        public List<Property> properties { get; set; }

        public CatalogAddRequest() { }
    }
}
