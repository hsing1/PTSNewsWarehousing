﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PTS.Modules.DBUtility;
using System.Web.UI.HtmlControls;

namespace NewsWarehousingWeb
{
    public partial class OpenWin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            
            this.Class.Value = Request["class"].ToString();
            DBOperate db = new DBOperate();
            var sql = @"SELECT ItemValue,
	                           ItemName
                        FROM SysReference
                        WHERE ItemType ='500'
                        Order BY OrderNo";
            GridView1.DataSource = db.DBGetTable(sql);
            GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string strSn;
            string strNM;
            var c = Class.Value;
            HtmlInputButton btnSelect;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                strSn = GridView1.DataKeys[e.Row.RowIndex].Values[0].ToString();
                strNM = GridView1.DataKeys[e.Row.RowIndex].Values[1].ToString();
                btnSelect = (HtmlInputButton)e.Row.Cells[0].FindControl("btnSelect");
                btnSelect.Attributes["onclick"] = "senddata('" + strSn + "','" + strNM + "','" + c + "'); ";
            }
        }
    }
}