﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("tray")]
    public class Tray
    {
        public string lockStatus { get; set; }
        public string jobId { get; set; }
    }
}
