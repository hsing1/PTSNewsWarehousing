﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("cartridge")]
    public class Cartridge
    {
        public Cartridge()
        {
        }
        public Cartridge(string id, string title, string mediaId, string type, string status, string location, string freeSpace, string capacity, string ejectRequestStatus, string message)
        {
            this.id = id;
            this.title = title;
            this.mediaId = mediaId;
            this.kind = type;
            this.status = status;
            this.location = location;
            this.free = freeSpace;
            this.capacity = capacity;
            this.ejectRequestStatus = ejectRequestStatus;
            this.errorMessage = message;
        }


        public string id { get; set; }
        public string mediaId { get; set; }
        public string barcode { get; set; }
        public string title { get; set; }
        public string kind { get; set; }
        public string location { get; set; }
        public string deviceId { get; set; }
        public string trayId { get; set; }
        public string shelfLocation { get; set; }
        public string volumeModifyId { get; set; }
        public string rfTag { get; set; }
        public string free { get; set; }
        public string capacity { get; set; }
        public string ejectRequestStatus { get; set; }
        public string lamp { get; set; }
        public string status { get; set; }
        public string isArchivePermitted { get; set; }
        public string isRetrievePermitted { get; set; }
        public string isDeletePermitted { get; set; }
        public string isFormatPermitted { get; set; }
        public string isFinalizePermitted { get; set; }
        public string isWriteProtectOnPermitted { get; set; }
        public string isWriteProtectOffPermitted { get; set; }
        public string isEjectCartridgePermitted { get; set; }
        public string isMetadataEditPermitted { get; set; }
        public string errorKey { get; set; }
        public string errorMessage { get; set; }
        public string jobId { get; set; }
        //public String writeProtectReason{ get; set; }
    }
}
