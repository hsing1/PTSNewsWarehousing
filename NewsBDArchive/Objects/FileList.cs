﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("files")]
    public class FileList
    {
        public FileList()
        {
        }

        public string totalCount { get; set; }
        public string count { get; set; }
        public string totalSize { get; set; }
        public string size { get; set; }


        [System.Xml.Serialization.XmlElement("file")]
        public List<File> files { get; set; }
    }
}
