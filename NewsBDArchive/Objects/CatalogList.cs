﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
#if true
    [System.Xml.Serialization.XmlRoot("catalogs")] // v1.00
#else
    [System.Xml.Serialization.XmlRoot("catgalogs")] // GM4
#endif
    public class CatalogList
    {
        public CatalogList()
        {
        }

        public string totalCount { get; set; }
        public string count { get; set; }
        public string totalSize { get; set; }
        public string size { get; set; }
        [System.Xml.Serialization.XmlElement("catalog")]
        public List<Catalog> catalogs = null;

    }
}
