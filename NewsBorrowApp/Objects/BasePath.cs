﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
#if true
    [System.Xml.Serialization.XmlRoot("basePath")] // v1.00
#else
    [System.Xml.Serialization.XmlRoot("bathPath")] // GM4
#endif
    public class BasePath
    {
        public string virtualPath { get; set; }
        public string physicalPath { get; set; }
        public string isFileToBeDeleted { get; set; }

        public BasePath(string virtualPath, string physicalPath, string isFileToBeDeleted)
        {
            this.virtualPath = virtualPath;
            this.physicalPath = physicalPath;
            this.isFileToBeDeleted = isFileToBeDeleted;

        }
        public BasePath()
        {
        }
    }
}
