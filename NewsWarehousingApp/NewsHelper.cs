﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace NewsWarehousingApp
{
    public class NewsHelper
    {
        public string iNewsFileGet(string Source, string Target)
        {
            var retuenValue = "";

            try
            {
                ProcessStartInfo psi = new ProcessStartInfo(@"C:\Windows\System32\cmd");
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;

                Process process = Process.Start(psi);
                string strPath = Environment.CurrentDirectory;
                string cmdForTunnel = strPath + $"\\plink 10.1.163.1 -l newscg -pw p@ssw0rd \"/exc/doc -g `cat /list/{Source}`\" > {Target}.txt ";
                process.StandardInput.WriteLine(cmdForTunnel);
                //process.WaitForExit();
                //Thread.Sleep(15000);
                SpinWait.SpinUntil(() => false, 30000);

                //DoBusinessLogic();
                process.StandardInput.WriteLine("logout");
                SpinWait.SpinUntil(() => false, 30000);

                if (process.HasExited)
                {
                    process.Close();
                    process.Dispose();
                }

                retuenValue = File.ReadAllText(Path.Combine(strPath, Target + ".txt"));

                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    var item = new iNewsBackup() { iNewsRaw = retuenValue, NewsType = Source, CreateDate = DateTime.Now };

                    DB.iNewsBackup.Add(item);

                    DB.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Utility.SaveException("iNewsFileGet", ex);
            }

            return retuenValue;


        }

        public bool iNewsFileResolve(ref string Document, string DirName, string NewsTime, string NewsType)
        {
            string title = "", tmpDoc = "", storyid = "", body = "", slogan = "";

            try
            {
                PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

                while (Document.Replace("\r\n", "").Length > 0)
                {
                    tmpDoc = Document.Substring(0, Document.IndexOf("</nsml>") + 7);
                    Document = Document.Substring(Document.IndexOf("</nsml>") + 7, Document.Length - Document.IndexOf("</nsml>") - 7);
                    
                    if (tmpDoc.IndexOf("(SOT)") == -1) continue;

                    body = tmpDoc.Substring(tmpDoc.IndexOf("<body>"), tmpDoc.IndexOf("</body>") - (tmpDoc.IndexOf("<body>")));

                    body = body.Replace("<pi>-1", String.Empty).Replace("<cc>-1", String.Empty);

                    body = Regex.Replace(body, @"<[^>]*>", String.Empty);

                    body = Regex.Replace(body, @"\[\[[^\]]+\]\]", String.Empty);

                    body = Regex.Replace(body, @"\<[^\>]+\>", String.Empty);

                    body = body.Replace("&&", String.Empty);

                    body = body.Replace("##", String.Empty);

                    body = body.Replace("&amp;", String.Empty);

                    if (body.Replace(" ", "").Replace("\r\n", "").Length == 0) continue;

                    var news = new NewsMeta();

                    news.Body = body;
                    
                    storyid = tmpDoc.Substring(tmpDoc.IndexOf("<storyid>") + 9, tmpDoc.IndexOf("</storyid>") - (tmpDoc.IndexOf("<storyid>") + 9));
                    storyid = Regex.Replace(storyid, @"<[^>]*>", String.Empty);

                    news.StoryID = storyid;

                    slogan = tmpDoc.Substring(tmpDoc.IndexOf("\"title\">") + 8, tmpDoc.IndexOf("</string>", tmpDoc.IndexOf("\"title\">")) - (tmpDoc.IndexOf("\"title\">") + 8));
                    slogan = Regex.Replace(slogan, @"<[^>]*>", String.Empty);

                    news.Slogan = slogan;

                    news.NewsDate = DateTime.Now.ToString("yyyyMMdd");

                    var q = DB.NewsMeta.Where(x => x.StoryID == storyid).Select(x => x).FirstOrDefault();

                    if (q != null) continue;

                    DB.NewsMeta.Add(news);

                    DB.SaveChanges();

                    var mos = tmpDoc;

                    while (true)
                    {
                        if (mos.IndexOf("<mos>") == -1) break;
                        if (mos.IndexOf("<clipPath>") == -1) break;

                        //mos = mos.Replace("\r\n", "").Replace(" ","");

                        var start = mos.IndexOf("<mos>");
                        var end = mos.IndexOf("</mos>") + 6;

                        var tmpMos = mos.Substring(start, end - start);

                        mos = mos.Substring(end, mos.Length - end);

                        if (tmpMos.IndexOf("(SOT)") == -1) continue;

                        var obj = new VideoMeta();

                        try
                        {
                            obj.OriginalFile = tmpMos.Substring(tmpMos.IndexOf("<clipPath>"), tmpMos.IndexOf("</clipPath>") + 11 - tmpMos.IndexOf("<clipPath>")).Replace("<clipPath>", "").Replace("</clipPath>", "");

                            if (tmpMos.IndexOf("<objProxyPath techDescription=\"QT\">") > 0)
                            {
                                obj.OriginalLow = tmpMos.Substring(tmpMos.IndexOf("\"QT\">"), tmpMos.IndexOf("</objProxyPath>") + 15 - tmpMos.IndexOf("\"QT\">")).Replace("\"QT\">", "").Replace("</objProxyPath>", "");
                            }
                        }
                        catch (Exception ex)
                        {
                            Utility.SaveException("iNewsFileResolve_ParserMos", ex);
                            obj.OriginalFile = "";
                            obj.OriginalLow = "";
                            obj.FileStstus = 98;
                        }

                        obj.NewsID = news.NewsID;

                        obj.NewsType = NewsType;

                        obj.CreateDate = DateTime.Now;
                        
                        DB.VideoMeta.Add(obj);

                        DB.SaveChanges();

                        break;

                    }

                }

                DB.Dispose();
            }
            catch (Exception ex)
            {
                Utility.SaveException("iNewsFileResolve", ex);
                return false;
            }

            return true;
        }

        public bool NewsResolve()
        {

            string tmpDoc = "", storyid = "", body = "";

            try
            {
                PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

                var items = DB.PreArchive
                              .Where(x => x.ArchiveConfirm == true && x.ArchiveStatus == 2)
                              .Select(x => x).ToList();

                foreach (var item in items)
                {

                    tmpDoc = item.NewsRaw;


                    body = tmpDoc.Substring(tmpDoc.IndexOf("<body>"), tmpDoc.IndexOf("</body>") - (tmpDoc.IndexOf("<body>")));

                    body = body.Replace("<pi>-1", String.Empty).Replace("<cc>-1", String.Empty);

                    body = Regex.Replace(body, @"<[^>]*>", String.Empty);

                    body = Regex.Replace(body, @"\[\[[^\]]+\]\]", String.Empty);

                    body = Regex.Replace(body, @"\<[^\>]+\>", String.Empty);

                    body = body.Replace("&&", String.Empty);

                    body = body.Replace("##", String.Empty);

                    body = body.Replace("&amp;", String.Empty);

                    //body = body.Replace(" ", String.Empty);

                    //if (body.Replace(" ", "").Replace("\r\n", "").Length == 0) continue;

                    var news = new NewsMeta();

                    news.Body = body;

                    news.StoryID = item.Storyid;

                    news.Slogan = item.Slogan;

                    news.NewsDate = DateTime.Now.ToString("yyyyMMdd");

                    DB.NewsMeta.Add(news);

                    DB.SaveChanges();
                    
                    var obj = new VideoMeta();

                    obj.OriginalFile = $"/ONAIR/{item.MXFName.Trim()}";
                    
                    obj.OriginalLow = $"\\\\PROXYSVR\\proxy\\{item.MP4Name.Trim()}\\proxy.mp4";

                    obj.NewsID = news.NewsID;

                    obj.NewsType = item.NewsType;

                    obj.CreateDate = DateTime.Now;

                    obj.PreArchiveID = item.PreArchiveID;

                    item.ArchiveStatus = 3;

                    DB.VideoMeta.Add(obj);

                    DB.SaveChanges();


                }

                DB.Dispose();
            }
            catch (Exception ex)
            {
                Utility.SaveException("NewsResolve", ex);
                return false;
            }

            return true;
        }

        public bool NewsResolve(int id)
        {

            string tmpDoc = "", storyid = "", body = "";

            try
            {
                PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

                var items = DB.PreArchive
                              .Where(x => x.PreArchiveID == id)
                              .Where(x => x.ArchiveConfirm == true && x.ArchiveStatus == 2)
                              .Select(x => x).ToList();

                foreach (var item in items)
                {

                    tmpDoc = item.NewsRaw;


                    body = tmpDoc.Substring(tmpDoc.IndexOf("<body>"), tmpDoc.IndexOf("</body>") - (tmpDoc.IndexOf("<body>")));

                    body = body.Replace("<pi>-1", String.Empty).Replace("<cc>-1", String.Empty);

                    body = Regex.Replace(body, @"<[^>]*>", String.Empty);

                    body = Regex.Replace(body, @"\[\[[^\]]+\]\]", String.Empty);

                    body = Regex.Replace(body, @"\<[^\>]+\>", String.Empty);

                    body = body.Replace("&&", String.Empty);

                    body = body.Replace("##", String.Empty);

                    body = body.Replace("&amp;", String.Empty);

                    body = body.Replace(" ", String.Empty);

                    //if (body.Replace(" ", "").Replace("\r\n", "").Length == 0) continue;

                    var news = new NewsMeta();

                    news.Body = body;

                    news.StoryID = item.Storyid;

                    news.Slogan = item.Slogan;

                    news.NewsDate = DateTime.Now.ToString("yyyyMMdd");

                    DB.NewsMeta.Add(news);

                    DB.SaveChanges();

                    var obj = new VideoMeta();

                    obj.OriginalFile = $"/ONAIR/{item.MXFName.Trim()}";

                    obj.OriginalLow = $"\\\\PROXYSVR\\proxy\\{item.MP4Name.Trim()}\\proxy.mp4";

                    obj.NewsID = news.NewsID;

                    obj.NewsType = item.NewsType;

                    obj.CreateDate = DateTime.Now;

                    obj.PreArchiveID = item.PreArchiveID;

                    item.ArchiveStatus = 3;

                    DB.VideoMeta.Add(obj);

                    DB.SaveChanges();


                }

                DB.Dispose();
            }
            catch (Exception ex)
            {
                Utility.SaveException("NewsResolve", ex);
                return false;
            }

            return true;
        }
    }
}
