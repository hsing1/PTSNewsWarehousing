﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsWarehousingApp
{
    class FTPWork
    {
        public void Run(VideoMeta item, string appPath)
        {
            var DB = new PTSNewsWarehousingEntities();

            DB.Entry(item).State = EntityState.Modified;

            var preItem = DB.PreArchive.Find(item.PreArchiveID);

            try
            {
                var filename = Guid.NewGuid().ToString("N");

                var NewsArchiveFolder = Properties.Settings.Default["NewsArchiveFolder"].ToString();
                

                item.ArchiveName = filename + ".mxf";
                

                switch (item.NewsType)
                {
                    case "9001":
                    case "9002":
                        item.ArchivePath = Path.Combine(NewsArchiveFolder, "PTS");
                        break;
                    case "9003":
                        item.ArchivePath = Path.Combine(NewsArchiveFolder, "Hakka");
                        break;
                    case "9005":
                        item.ArchivePath = Path.Combine(NewsArchiveFolder, "HakkaMgz");
                        break;
                    case "9004":
                        item.ArchivePath = Path.Combine(NewsArchiveFolder, "Macroview");
                        break;
                }
                

                var server = FTPServer.FtpServerIP
                                      .Where(x => x.NewsType == item.NewsType)
                                      .Select(x => x.IP).FirstOrDefault();

                item.FileStstus = 1;
                preItem.ArchiveStatus = 4;
                DB.SaveChanges();

                var fullName = Path.Combine(item.ArchivePath, item.ArchiveName);

                var obj = new FileHelper();

                obj.GetMXF("ftp://" + server + "/" + item.OriginalFile, fullName);

                FileInfo fi = new FileInfo(fullName);

                if(!fi.Exists)
                {
                    item.FileStstus = 990;
                    preItem.ArchiveStatus = 99;
                    DB.SaveChanges();
                    DB.Dispose();
                    return;
                }
                else if(fi.Length <= 0)
                {
                    item.FileStstus = 991;
                    preItem.ArchiveStatus = 99;
                    DB.SaveChanges();
                    DB.Dispose();
                    return;
                }


                var ffprobePath = Path.Combine(appPath, "ffprobe.exe");

                var cmd = $" -v quiet -print_format json -show_format -show_streams -pretty {fullName}";

                ProcessStartInfo processStartInfo = new ProcessStartInfo(ffprobePath, cmd)
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    WorkingDirectory = Path.GetDirectoryName(appPath),
                    RedirectStandardInput = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };
                Process processPriority = Process.Start(processStartInfo);
                processPriority.BeginErrorReadLine();

                item.MediaInfo = processPriority.StandardOutput.ReadToEnd();

                MediaFileInfo info;

                try
                {
                    info = JsonConvert.DeserializeObject<MediaFileInfo>(item.MediaInfo);
                } 
                catch (Exception ex)
                {
                    Utility.SaveException("JsonConvert.DeserializeObject", ex);

                    item.FileStstus = 992;
                    preItem.ArchiveStatus = 99;
                    DB.SaveChanges();
                    DB.Dispose();
                    return;
                }

                item.Duration = info.streams[0].duration;
                item.Timecode = info.format.tags.timecode;

                processPriority.WaitForExit();
                processPriority.Close();

                var NewsMP4Folder = Properties.Settings.Default["NewsMP4Folder"].ToString();

                item.MP4Path = NewsMP4Folder;
                item.MP4Name = filename + ".mp4";

                obj.GetMP4(item.OriginalLow, NewsMP4Folder, item.MP4Name);

                item.FileStstus = 2;
                preItem.ArchiveStatus = 5;
                DB.SaveChanges();
            }
            catch (Exception ex)
            {
                Utility.SaveException("FTPWorker1_DoWork", ex);
                item.FileStstus = 99;
                preItem.ArchiveStatus = 99;
                DB.SaveChanges();
            }
            finally
            {
                DB.Dispose();
            }
        }
    }
}
