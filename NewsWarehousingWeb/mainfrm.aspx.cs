﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PTS.Modules.DBUtility;
using PTS.Modules.Utilities;
using PTS.Modules.ExtMethods;
using PTS.Modules.Employee;

namespace NewsWarehousingWeb
{
    public partial class mainfrm : System.Web.UI.Page
        
    {
        DBOperate db = new DBOperate();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                foreach (GridViewRow row in GridView2.Rows)
                {
                    ((Label)row.FindControl("lblMsg")).Text = "";
                }
            }
            else
            {
                QueryData();
            }
            
        }

        void QueryData()
        {
            var dic = new Dictionary<string, string>();
            var sql = @"SELECT NewsMeta.Slogan,
                       CameraDate,
	                   (case when len(Body) > 80 then SUBSTRING(Body,0,80)else Body end ) Body,
                       VideoMeta.VideoID
                FROM VideoMeta
                JOIN NewsMeta on NewsMeta.NewsID = VideoMeta.NewsID
                WHERE 1 = 1 ";
            if (this.CalendarB.Text != "")
            {
                if (this.CalendarE.Text != "")
                {
                    sql += "  AND CameraDate between @CalendarB and @CalendarE ";
                    dic.Add("CalendarB", CalendarB.Text);
                    dic.Add("CalendarE", CalendarE.Text);
                }
                else
                {
                    sql += "  AND CameraDate >= @CalendarB ";
                    dic.Add("CalendarB", CalendarB.Text);
                }
            }
            sql += " ORDER BY CameraDate ";
            GridView2.DataSource = db.DBGetTable(sql,dic);
            GridView2.DataBind();
        }
        void GetDetailData(string VideoID)
        {
            var sql = @"  SELECT Mp4Name
                          FROM VideoMeta
                          WHERE VideoMeta.VideoID = @VideoID";
            var data = db.DBGetTable(sql, new Dictionary<string, string> { { "VideoID", VideoID } }).ExTableToDictionary();

            var v = "http://10.1.253.192/NewsWarehousing/mp4/" + data["Mp4Name"].ToString();
            lallowVideo.Text = $"<video id=\"lowVideo\" autoplay controls  width=\"431\" height=\"307\" ><source src=\"" + v + "\"type =\"video/mp4\"></video>";

        }
        protected void btnQuery_Click(object sender, EventArgs e)
        {
            QueryData();
        }

        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView2.PageIndex = e.NewPageIndex;
            QueryData();
        }

        protected void GridView2_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Preview")
            {  
                // GridView1.DataKeys[pk_index].Value.ToString()
                pkIndex.Value = e.CommandArgument.ToString();
                GetDetailData(pkIndex.Value);               
            }
            else if(e.CommandName == "Borrow") 
            {
                var i = db.DBGetValue<int>("SELECT COUNT(*) FROM BorrowList WHERE VideoID = @VideoID AND BorrowStatus = 0 ", new Dictionary<string, string> { { "VideoID", e.CommandArgument.ToString() } });

                var btn = (LinkButton)e.CommandSource;
                var c = (Label)btn.NamingContainer.FindControl("lblMsg");

                if (i > 0)
                {
                    c.Text = "排程中";
                }
                else
                {
                    var objEmp = new EmployeeInfo();

                    db.DBInsert(new Dictionary<string, string> { { "VideoID", e.CommandArgument.ToString() }
                                                               , { "UserID", objEmp.EmployeeID }}, "BorrowList");

                    c.Text = "加入排程";
                }

                
            }
            else
            {
                lallowVideo.Text = "";
            }
        }
    }
}