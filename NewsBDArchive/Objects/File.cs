﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("file")]
    public class File
    {
        public string name { get; set; }
        public string path { get; set; }
        public string type { get; set; }
        public string size { get; set; }
        public string updateTime { get; set; }
        public string jobId { get; set; }

        public File()
        {
        }

    }
}
