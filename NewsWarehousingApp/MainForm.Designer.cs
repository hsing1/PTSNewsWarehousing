﻿namespace NewsWarehousingApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            this.FTPWorker1 = new System.ComponentModel.BackgroundWorker();
            this.FTPWorker2 = new System.ComponentModel.BackgroundWorker();
            this.iNewsWorker = new System.ComponentModel.BackgroundWorker();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.ReLoadWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ReLoadWorker2 = new System.ComponentModel.BackgroundWorker();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.BorrowWorker1 = new System.ComponentModel.BackgroundWorker();
            this.BorrowWorker2 = new System.ComponentModel.BackgroundWorker();
            this.BorrowWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.BorrowTimer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PreArchiveTimer = new System.Windows.Forms.Timer(this.components);
            this.PreArchiveWorker = new System.ComponentModel.BackgroundWorker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FTPWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 694);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1377, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblTime
            // 
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(0, 17);
            // 
            // MainTimer
            // 
            this.MainTimer.Enabled = true;
            this.MainTimer.Interval = 1200000;
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // FTPWorker1
            // 
            this.FTPWorker1.WorkerReportsProgress = true;
            this.FTPWorker1.WorkerSupportsCancellation = true;
            this.FTPWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FTPWorker1_DoWork);
            this.FTPWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.FTPWorker1_ProgressChanged);
            this.FTPWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.FTPWorker1_RunWorkerCompleted);
            // 
            // FTPWorker2
            // 
            this.FTPWorker2.WorkerReportsProgress = true;
            this.FTPWorker2.WorkerSupportsCancellation = true;
            this.FTPWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FTPWorker2_DoWork);
            this.FTPWorker2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.FTPWorker2_ProgressChanged);
            this.FTPWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.FTPWorker2_RunWorkerCompleted);
            // 
            // iNewsWorker
            // 
            this.iNewsWorker.WorkerReportsProgress = true;
            this.iNewsWorker.WorkerSupportsCancellation = true;
            this.iNewsWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.iNewsWorker_DoWork);
            this.iNewsWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.iNewsWorker_ProgressChanged);
            this.iNewsWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.iNewsWorker_RunWorkerCompleted);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(877, 113);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "9002";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(439, 71);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(396, 258);
            this.textBox1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(877, 163);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(650, 435);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(212, 437);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(396, 22);
            this.textBox2.TabIndex = 5;
            // 
            // ReLoadWorker1
            // 
            this.ReLoadWorker1.WorkerReportsProgress = true;
            this.ReLoadWorker1.WorkerSupportsCancellation = true;
            this.ReLoadWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ReLoadWorker1_DoWork);
            this.ReLoadWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ReLoadWorker1_ProgressChanged);
            this.ReLoadWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ReLoadWorker1_RunWorkerCompleted);
            // 
            // ReLoadWorker2
            // 
            this.ReLoadWorker2.WorkerReportsProgress = true;
            this.ReLoadWorker2.WorkerSupportsCancellation = true;
            this.ReLoadWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ReLoadWorker2_DoWork);
            this.ReLoadWorker2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ReLoadWorker2_ProgressChanged);
            this.ReLoadWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ReLoadWorker2_RunWorkerCompleted);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(977, 113);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "9003";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1011, 306);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 7;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // BorrowWorker1
            // 
            this.BorrowWorker1.WorkerReportsProgress = true;
            this.BorrowWorker1.WorkerSupportsCancellation = true;
            this.BorrowWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BorrowWorker1_DoWork);
            this.BorrowWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BorrowWorker1_ProgressChanged);
            this.BorrowWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BorrowWorker1_RunWorkerCompleted);
            // 
            // BorrowWorker2
            // 
            this.BorrowWorker2.WorkerReportsProgress = true;
            this.BorrowWorker2.WorkerSupportsCancellation = true;
            this.BorrowWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BorrowWorker2_DoWork);
            this.BorrowWorker2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BorrowWorker2_ProgressChanged);
            this.BorrowWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BorrowWorker2_RunWorkerCompleted);
            // 
            // BorrowWorkerMain
            // 
            this.BorrowWorkerMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BorrowWorkerMain_DoWork);
            this.BorrowWorkerMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BorrowWorkerMain_RunWorkerCompleted);
            // 
            // BorrowTimer
            // 
            this.BorrowTimer.Interval = 60000;
            this.BorrowTimer.Tick += new System.EventHandler(this.BorrowTimer_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "App Start Time :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "label2";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(986, 451);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 10;
            this.button6.Text = "Test";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(683, 540);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 11;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(814, 540);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 12;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(939, 540);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 13;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(1104, 541);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 22);
            this.button10.TabIndex = 14;
            this.button10.Text = "BStop";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "PreArchiveTimer:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(146, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 12);
            this.label4.TabIndex = 16;
            // 
            // PreArchiveTimer
            // 
            this.PreArchiveTimer.Enabled = true;
            this.PreArchiveTimer.Interval = 300000;
            this.PreArchiveTimer.Tick += new System.EventHandler(this.PreArchiveTimer_Tick);
            // 
            // PreArchiveWorker
            // 
            this.PreArchiveWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.PreArchiveWorker_DoWork);
            this.PreArchiveWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.PreArchiveWorker_RunWorkerCompleted);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(58, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "BorrowTimer:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(136, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 12);
            this.label6.TabIndex = 18;
            // 
            // FTPWorkerMain
            // 
            this.FTPWorkerMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FTPWorkerMain_DoWork);
            this.FTPWorkerMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.FTPWorkerMain_RunWorkerCompleted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(58, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "MainTimer:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(124, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 12);
            this.label8.TabIndex = 20;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(814, 597);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 21;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(895, 597);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 22);
            this.textBox3.TabIndex = 22;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1377, 716);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MainForm";
            this.Text = "新聞片庫入庫系統";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblTime;
        private System.Windows.Forms.Timer MainTimer;
        private System.ComponentModel.BackgroundWorker FTPWorker1;
        private System.ComponentModel.BackgroundWorker FTPWorker2;
        private System.ComponentModel.BackgroundWorker iNewsWorker;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.ComponentModel.BackgroundWorker ReLoadWorker1;
        private System.ComponentModel.BackgroundWorker ReLoadWorker2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.ComponentModel.BackgroundWorker BorrowWorker1;
        private System.ComponentModel.BackgroundWorker BorrowWorker2;
        private System.ComponentModel.BackgroundWorker BorrowWorkerMain;
        private System.Windows.Forms.Timer BorrowTimer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer PreArchiveTimer;
        private System.ComponentModel.BackgroundWorker PreArchiveWorker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.ComponentModel.BackgroundWorker FTPWorkerMain;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox3;
    }
}