﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
#if true
    [System.Xml.Serialization.XmlRoot("trayRequest")] // v1.00
#else
    [System.Xml.Serialization.XmlRoot("TrayRequest")] // GM4
#endif
    public class TrayRequest
    {
        public string command { get; set; }
    }
}
