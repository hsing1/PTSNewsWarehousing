﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NewsWarehousingWeb.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="App_CSS/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    ↑使用上方的選單開始作業。

    <div id="dialog" title="注意">
       
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script src="App_Js/jquery-ui.min.js"></script>
    <script>

        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        $(function () {
            $("#dialog").html('<p>系統偵測到您使用的是IE瀏覽器，請改用Chrome 或 FireFox</p><p>在Chrome 或 FireFox網址列直接輸入</p><p>newsmam.pts.org.tw 即可使用</p>');


            $("#dialog").dialog({
                autoOpen: false,
                width: 500,
                draggable: false,
                modal: true,
                position: { my: "center", at: "top+200px", of: window }
            });

            if (isIE) {
                
                $("#dialog").dialog("open");
            }
        });
    </script>
</asp:Content>
