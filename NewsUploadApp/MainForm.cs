﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using PTS.News.Utilities;
using System.Diagnostics;
using Newtonsoft.Json;

namespace NewsUploadApp
{
    public partial class MainForm : Form
    {
        private const string VERSION = "20171124.2";

        string NewsArchivePath, NewsMp4Path, NewsMp4Source, NewsMp4IISPath, NewsUploadPath, NewsPreConvertPath, NewsConvertPath, appPath;

        public MainForm()
        {
            InitializeComponent();

            CustomizedInit();
        }

        protected void CustomizedInit()
        {
            sslVersion.Text = "版本：" + VERSION;

            NewsArchivePath = @"\\10.13.200.15\Archive";
            NewsUploadPath = @"\\10.13.200.15\Upload";
            NewsPreConvertPath = @"\\10.13.200.15\PreConvert";
            NewsConvertPath = @"\\10.13.200.56\source_mxf";
            NewsMp4Source = @"\\10.13.200.56\output_mp4";
            NewsMp4Path = @"\\10.13.200.15\Archive\MP4";
            NewsMp4IISPath = @"\\10.13.200.58\MP4";

            appPath = Application.StartupPath;

            NetHelper.NetUse(@"\\10.13.200.15", "admin", "pts870701");
            
        }

        protected override void OnLoad(EventArgs e)
        {
            labelTime1.Text = SearchFolderTimer.Enabled.ToString();
            labelTime2.Text = PreConvertTimer.Enabled.ToString();
            labelTime3.Text = ToConvertTimer.Enabled.ToString();
            labelTime4.Text = ToWarehouseTimer.Enabled.ToString();

            labelRun1.Text = "Idle";
            labelRun2.Text = "Idle";
            labelRun3.Text = "Idle";
            labelRun4.Text = "Idle";

            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {

            if(SearchFolderTimer.Enabled || PreConvertTimer.Enabled || ToConvertTimer.Enabled || ToWarehouseTimer.Enabled)
            {
                MessageBox.Show("還有排程在進行中，無法關閉!");
                e.Cancel = true;
            }

            if(SearchFolderWorker.IsBusy || PreConvertWorker.IsBusy || ToConvertWorker.IsBusy || ToWarehouseWorker.IsBusy)
            {
                MessageBox.Show("還有背景程序在進行中，請稍後關閉!");
                e.Cancel = true;
            }
            
            base.OnClosing(e);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            await Task.Run(() => GetPathFiles());

            ((Button)sender).Enabled = true;
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            await Task.Run(() => MoveToPreConvert());

            ((Button)sender).Enabled = true;
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            await Task.Run(() => MoveToConvert());

            ((Button)sender).Enabled = true;
        }
        private async void button4_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            await Task.Run(() => MoveToWarehouse());

            ((Button)sender).Enabled = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button1.Enabled = !button1.Enabled;
            button2.Enabled = !button2.Enabled;
            button3.Enabled = !button3.Enabled;
            button4.Enabled = !button4.Enabled;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SearchFolderTimer.Enabled = false;
            PreConvertTimer.Enabled = false;
            ToConvertTimer.Enabled = false;
            ToWarehouseTimer.Enabled = false;

            labelTime1.Text = SearchFolderTimer.Enabled.ToString();
            labelTime2.Text = PreConvertTimer.Enabled.ToString();
            labelTime3.Text = ToConvertTimer.Enabled.ToString();
            labelTime4.Text = ToWarehouseTimer.Enabled.ToString();
        }

        private void SearchFolderTimer_Tick(object sender, EventArgs e)
        {
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : SearchFolderTimer_Tick()\r\n");

            if (!SearchFolderWorker.IsBusy)
            {
                textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : SearchFolderWorker.RunWorkerAsync()\r\n");
                labelRun1.Text = "Run";
                SearchFolderWorker.RunWorkerAsync();
            }
        }

        private void SearchFolderWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            GetPathFiles();
        }

        private void SearchFolderWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            labelRun1.Text = "Idle";
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : SearchFolderWorker.RunWorkerCompleted()\r\n");
        }

        private void PreConvertTimer_Tick(object sender, EventArgs e)
        {
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : PreConvertTimer_Tick()\r\n");

            if (!PreConvertWorker.IsBusy)
            {
                textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : PreConvertWorker.RunWorkerAsync()\r\n");
                labelRun2.Text = "Run";
                PreConvertWorker.RunWorkerAsync();
            }
        }

        private void PreConvertWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MoveToPreConvert();
        }

        private void PreConvertWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            labelRun2.Text = "Idle";
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : PreConvertWorker_RunWorkerCompleted()\r\n");
        }

        private void ToConvertTimer_Tick(object sender, EventArgs e)
        {
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : ToConvertTimer_Tick()\r\n");

            if (!ToConvertWorker.IsBusy)
            {
                textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : ToConvertWorker.RunWorkerAsync()\r\n");
                labelRun3.Text = "Run";
                ToConvertWorker.RunWorkerAsync();
            }
        }
        private void ToConvertWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MoveToConvert();
        }
        private void ToConvertWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            labelRun3.Text = "Idle";
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : ToConvertWorker_RunWorkerCompleted()\r\n");
        }

        private void ToWarehouseTimer_Tick(object sender, EventArgs e)
        {
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : ToWarehouseTimer_Tick()\r\n");

            if (!ToWarehouseWorker.IsBusy)
            {
                textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : ToWarehouseWorker.RunWorkerAsync()\r\n");
                labelRun4.Text = "Run";
                ToWarehouseWorker.RunWorkerAsync();
            }
        }

        private void ToWarehouseWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MoveToWarehouse();
        }

        private void ToWarehouseWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            labelRun4.Text = "Idle";
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : ToWarehouseWorker.RunWorkerCompleted()\r\n");
        }


        protected void GetPathFiles()
        {
            // 尋找\\10.13.200.15\Upload下所有.mxf

            var uploadTemp = new List<UploadTemp>();

            DirectoryInfo di = new DirectoryInfo(NewsUploadPath);

            PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

            var dbFile = DB.UploadTemp
                           .Where(x => x.ConfirmMark == false)
                           .Select(x => new { x.UploadID, x.VideoName })
                           .ToList();

            foreach (var fi in di.GetFiles("*.mxf"))
            {
                if (!IsFileLocked(fi))
                {
                    
                    if (dbFile.Where(x => x.VideoName == fi.Name).FirstOrDefault() != null) continue;
                    
                    var strInfo = GetMediaInfo(fi.FullName);

                    var item = new UploadTemp()
                    {
                        VideoName = fi.Name,
                        VideoSize = fi.Length,
                        MediaInfo = strInfo,
                        CreateDate = DateTime.Now
                    };
                    
                    try
                    {
                        if (!string.IsNullOrEmpty(strInfo))
                        {
                            var info = JsonConvert.DeserializeObject<MediaFileInfo>(strInfo);

                            item.VideoTime = info.streams[0].duration;
                        }
                        else
                        {
                            item.FileStatus = 991;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.SaveException("GetPathFiles()", fi.FullName, ex);

                        item.FileStatus = 991;
                    }

                    if (item.FileStatus != 991)
                    {
                        TimeSpan Fifteen = new TimeSpan(0, 15, 0);

                        if (TimeSpan.TryParse(item.VideoTime.Split('.')[0], out TimeSpan tVideo))
                        {
                            if (tVideo.CompareTo(Fifteen) > 0)
                                item.FileStatus = 9;
                        }
                        else
                        {
                            item.FileStatus = 99;
                        }
                    }

                    uploadTemp.Add(item);


                }
                
            }

            foreach (var item in dbFile)
            {
                if (!File.Exists(Path.Combine(NewsUploadPath, item.VideoName)))
                {
                    DB.Entry(new UploadTemp { UploadID = item.UploadID }).State = System.Data.Entity.EntityState.Deleted;
                }
            }

            DB.UploadTemp.AddRange(uploadTemp);

            DB.SaveChanges();

            DB.Dispose();
            
        }

        protected string GetMediaInfo(string fullFile)
        {
            string aLine = "", returnString = "";
            string end = "";

            try
            {

                var ffprobePath = Path.Combine(appPath, "ffprobe.exe");

                var cmd = $" -v quiet -print_format json -show_format -show_streams -pretty {fullFile}";

                ProcessStartInfo processStartInfo = new ProcessStartInfo(ffprobePath, cmd)
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    WorkingDirectory = Path.GetDirectoryName(appPath),
                    RedirectStandardInput = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };
                Process processPriority = Process.Start(processStartInfo);
                processPriority.BeginErrorReadLine();
                end = processPriority.StandardOutput.ReadToEnd();
                processPriority.WaitForExit();
                processPriority.Close();

                /* 以下是處理json中file_package_name & material_package_name 最後的＂會被吃掉,通常發生在被GV編輯過的檔案 */
                StringReader strReader = new StringReader(end);
                
                while (true)
                {
                    aLine = strReader.ReadLine();
                    if (aLine != null)
                    {
                        if ((aLine.IndexOf("file_package_name") != -1 || aLine.IndexOf("material_package_name") != -1) && aLine.Substring(aLine.Length - 2, 2) != "\",")
                        {
                            StringBuilder temp = new StringBuilder(aLine);
                            aLine = temp.Insert(temp.Length - 1, '"').ToString();
                        }

                        returnString = returnString + aLine + "\r\n";
                    }
                    else
                    {
                        returnString = returnString + "\r\n";
                        break;
                    }
                }
                /*****************************************************************/

            }
            catch (Exception ex)
            {
                Utility.SaveException("GetMediaInfo()", ex);
                
            }

            return returnString;

        }

        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }


        protected void MoveToPreConvert()
        {
            try
            {
                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    var list = DB.UploadTemp
                                 .Where(x => x.ConfirmMark == true && x.ConverterStatus == 0)
                                 .Select(x => x)
                                 .ToList();


                    foreach (var item in list)
                    {
                        var name = Guid.NewGuid().ToString("N") + ".mxf";

                        if (File.Exists(Path.Combine(NewsUploadPath, item.VideoName)))
                        {
                            File.Move(Path.Combine(NewsUploadPath, item.VideoName), Path.Combine(NewsPreConvertPath, name));

                            item.FileStatus = 1;
                            item.ConverterStatus = 1;
                            item.ArchiveName = name;
                        }
                        else
                        {
                            item.ConverterStatus = 991;
                        }


                        DB.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                Utility.SaveException("MoveToPreConvert()", ex);
            }
        }

        protected void MoveToConvert()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(NewsConvertPath);

                var size = di.GetFiles().ToList().Sum(x => x.Length);

                if (size > 2199023255552) return;

                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    var list = DB.UploadTemp
                                 .Where(x => x.FileStatus == 1 && x.ConverterStatus == 1)
                                 .Select(x => x)
                                 .ToList();


                    foreach (var item in list)
                    {

                        //var code = DB.ConvertCode
                        //             .Where(x => x.IsRunning == false)
                        //             .OrderBy(x => x.ConvertID)
                        //             .Select(x => x)
                        //             .FirstOrDefault();


                        //if (code != null)
                        //{

                        //    code.IsRunning = true;

                        //  DB.SaveChanges();


                        File.Copy(Path.Combine(NewsPreConvertPath, item.ArchiveName), Path.Combine(NewsConvertPath, item.ArchiveName));

                        item.ConverterStatus = 2;
                        item.ConvertFolder = NewsConvertPath;

                        DB.SaveChanges();

                        //}
                    }

                }
            }
            catch (Exception ex)
            {
                Utility.SaveException("MoveToConvert()", ex);
            }
        }

        protected void MoveToWarehouse()
        {
            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                var list = DB.UploadTemp
                             .Where(x => x.FileStatus == 1 && x.ConverterStatus == 4)
                             .Select(x => x)
                             .ToList();

              

                foreach (var item in list)
                {

                    try
                    {
                        File.Move(Path.Combine(NewsPreConvertPath, item.ArchiveName), Path.Combine(NewsArchivePath, item.Location, item.ArchiveName));

                        var mp4Name = item.ArchiveName.Split('.')[0] + ".mp4";

                        File.Move(Path.Combine(NewsMp4Source, mp4Name), Path.Combine(this.NewsMp4Path, mp4Name));

                        File.Copy(Path.Combine(NewsMp4Path, mp4Name), Path.Combine(this.NewsMp4IISPath, mp4Name));

                        item.FileStatus = 2;

                        var news = new NewsMeta()
                        {
                            NewsDate = item.CreateDate.ToString("yyyyMMdd"),
                            Slogan = item.VideoName.Split('.')[0]
                        };

                        DB.NewsMeta.Add(news);

                        DB.SaveChanges();

                        var newsType = "";

                        switch (item.Location)
                        {
                            case "PTS":
                                newsType = "9002";
                                break;
                            case "Hakka":
                                newsType = "9003";
                                break;
                            case "Macroview":
                                newsType = "9004";
                                break;
                            case "HakkaMgz":
                                newsType = "9005";
                                break;
                        }

                        var video = new VideoMeta()
                        {
                             NewsID = news.NewsID,
                             FileStstus = 2,
                             ArchivePath = Path.Combine(NewsArchivePath, item.Location),
                             ArchiveName = item.ArchiveName,
                             MP4Path = NewsMp4Path,
                             MP4Name = mp4Name,
                             CreateDate = DateTime.Now,
                             NewsType = newsType,
                             MediaInfo = item.MediaInfo
                        };

                        var info = JsonConvert.DeserializeObject<MediaFileInfo>(item.MediaInfo);

                        video.Duration = info.format.duration;
                        video.Timecode = info.format.tags.timecode;

                        DB.VideoMeta.Add(video);

                        DB.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Utility.SaveException("MoveToConvert()", item.VideoName, ex);
                    }
                    


                }

            }
        }
    }
}
