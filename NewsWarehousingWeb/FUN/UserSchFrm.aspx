﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserSchFrm.aspx.cs" Inherits="NewsWarehousingWeb.FUN.UserSchFrm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="../App_CSS/Style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table class="tblBase-Blue" align="center">
            <tr>
                <td>
                    <table width="500px">
                        <tr>
                            <td class="tdValue-Blue" style="width: 80%">
                                姓名或帳號：
                                <asp:TextBox ID="txtSearchField" runat="server" Width="200px"></asp:TextBox>
                                <asp:HiddenField ID="hfObjectName1" runat="server" />
                            </td>
                            <td class="tdValue-Blue" style="width: 20%">
                                <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                    <table width="500px">
                        <tr>
                            <td class="tdValue-Blue">
                                <asp:Panel ID="Panel1" runat="server" Height="420px" ScrollBars="Auto" Width="500px">
                                    <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#E7E7FF"
                                        BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                                        GridLines="Horizontal" Width="100%"
                                        AutoGenerateColumns="False" onrowdatabound="GridView1_RowDataBound">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="功能">
                                                <ItemTemplate>
                                                    <asp:Literal ID="lalSelect" runat="server" EnableViewState="False"></asp:Literal>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LoginID" HeaderText="帳號">
                                                <ItemStyle Width="120px" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CName" HeaderText="姓名">
                                                <ItemStyle Width="120px" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DeptName" HeaderText="部門">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>    
    </div>
    </form>
    <script type="text/javascript" src="<%= ResolveUrl("~/App_Js/jquery-1.11.1.min.js") %>"></script>
    <script language="javascript" type="text/javascript">
        function ReturnValue(Fields) {

            window.opener.document.getElementById($('#<% = hfObjectName1.ClientID %>').val()).value = Fields;
            window.opener.__doPostBack($('#<% = hfObjectName1.ClientID %>').val(), 'CHANGE');

            window.close();
        }  
    </script>
</body>
</html>
