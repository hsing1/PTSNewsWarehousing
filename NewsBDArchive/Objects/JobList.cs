﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("jobs")]
    public class JobList
    {
        public string totalCount { get; set; }
        public string count { get; set; }
        [System.Xml.Serialization.XmlElement("job")]
        public List<Job> jobs { get; set; }

        public JobList() { }
    }
}
