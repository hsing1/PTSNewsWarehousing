﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("fileAddRequest")]
    public class FileAddRequest
    {

        public string cartridgeId { get; set; }
        [System.Xml.Serialization.XmlArrayItem("catalogId")]
        public List<string> catalogIds { get; set; }
        public string filePath { get; set; }

        public FileAddRequest() { }
    }
}
