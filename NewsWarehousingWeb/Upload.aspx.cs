﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.DBUtility;
using PTS.Modules.Employee;
using Newtonsoft.Json;
using NewsWarehousingWeb.Models;
using System.Globalization;

namespace NewsWarehousingWeb
{
    public partial class Upload : System.Web.UI.Page
    {
        EmployeeInfo objEmp = new EmployeeInfo();
        int maxNumber;
        UserRule.Rule rule;

        private TimeSpan limitTime15 = new TimeSpan(0, 15, 0);
        private TimeSpan limitTime30 = new TimeSpan(0, 30, 0);
        private TimeSpan limitTime60 = new TimeSpan(0, 60, 0);
        protected void Page_Load(object sender, EventArgs e)
        {
            
            rule = JsonConvert.DeserializeObject<UserRule.Rule>(objEmp.Rule);

            SetBiggerLimit(this.lalBiggerMsg);

            if (IsPostBack) return;
            
            SetPageData();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SetPageData();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            var items = JsonConvert.DeserializeObject<List<UploadItem>>(hfValues.Value);

            var num = items.Where(x => x.BiggerType > 15).Count();

            var db = new DBOperate();

            if (items.Count > 0)
            {
                var m = new List<Dictionary<string, string>>();

                foreach (var item in items)
                {
                    m.Add(new Dictionary<string, string> { { "UploadID", item.Id.ToString() }, { "ConfirmMark", "1" }, { "Location", item.Location }, { "ConfirmUser", objEmp.EmployeeID }, { "ConfirmDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") } });
                }

                db.DBMultiUpdate(ref m, "UploadTemp", "UploadID");


            }

            

            if (num > 0 && (rule.IsUploadBigger30 == true || rule.IsUploadBigger60 == true))
            {

                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {

                    var limit = DB.BiggerLimit.Where(x => x.UserID == objEmp.EmployeeID).Select(x => x).FirstOrDefault();

                    limit.MaxNum += num;

                    DB.SaveChanges();

                }
            }

            SetPageData();

        }
        
        void SetBiggerLimit(Literal lal)
        {
            if (rule.IsUploadBigger30 || rule.IsUploadBigger60)
            {

                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    var years = DateTime.Now.Year.ToString();
                    var weeks = new TaiwanCalendar().GetWeekOfYear(Convert.ToDateTime(DateTime.Now), CalendarWeekRule.FirstDay, DayOfWeek.Sunday);

                    var limit = DB.BiggerLimit
                                  .Where(x => x.Years == years && x.Weeks == weeks)
                                  .Where(x => x.UserID == objEmp.EmployeeID)
                                  .Select(x => x)
                                  .FirstOrDefault();

                    if (limit == null)
                    {
                        limit = new BiggerLimit() { UserID = objEmp.EmployeeID, Years = years, Weeks = weeks, MaxNum = 0 };

                        DB.BiggerLimit.Add(limit);

                        DB.SaveChanges();
                    }

                    maxNumber = limit.MaxNum;

                    lal.Text = $"<span id=\"limitnum\" data-limit_num=\"{20 - limit.MaxNum}\">本週已上傳{limit.MaxNum}大型檔案，還可上傳{20 - limit.MaxNum}個。<span>";
                }
            }
        }
        void SetPageData()
        {


            var SQL = @"SELECT *
                          FROM (SELECT UploadID
                                      ,VideoName
	                                  ,CAST(CAST(ROUND(VideoSize / 1024.0 / 1024.0 / 1024.0, 2) AS DECIMAL(8,2)) AS NVARCHAR) + 'GB' VideoSize
                                      ,VideoTime
                                      ,FileStatus
                                      ,sr1.ItemName ConverterTitle
                                      ,ISNULL(sr2.ItemName, '未定') LocationName
                                      ,CreateDate
                                      ,ConfirmMark
                                      ,ConfirmUser
                                      ,ConfirmDate
							          ,1 SrNo
                                  FROM UploadTemp 
                                       JOIN SysReference sr1
                                            ON sr1.ItemType = 330
                                           AND sr1.ItemValue = ConverterStatus
                                       LEFT OUTER JOIN SysReference sr2
                                            ON sr2.ItemType = 200
                                           AND sr2.ItemValue = Location
						         WHERE FileStatus NOT IN (1, 2)
						           AND ConfirmMark = 0
                                 UNION
                                SELECT TOP 100 UploadID
                                      ,VideoName
	                                  ,CAST(CAST(ROUND(VideoSize / 1024.0 / 1024.0 / 1024.0, 2) AS DECIMAL(8,2)) AS NVARCHAR) + 'GB' VideoSize
                                      ,VideoTime
                                      ,FileStatus
                                      ,sr1.ItemName ConverterTitle
                                      ,sr2.ItemName LocationName
                                      ,CreateDate
                                      ,ConfirmMark
                                      ,ConfirmUser
                                      ,ConfirmDate
							          ,2 SrNo
                                  FROM UploadTemp 
                                       JOIN SysReference sr1
                                            ON sr1.ItemType = 330
                                           AND sr1.ItemValue = ConverterStatus
                                       JOIN SysReference sr2
                                            ON sr2.ItemType = 200
                                           AND sr2.ItemValue = Location
						         WHERE ConfirmMark = 1 
						           AND ConfirmUser = @ConfirmUser
						         ) tmp
						   ORDER BY tmp.SrNo , tmp.CreateDate DESC ";

            if (cbConfirm.Checked)
            {
                SQL = @"SELECT TOP 250 UploadID
                                      ,VideoName
	                                  ,CAST(CAST(ROUND(VideoSize / 1024.0 / 1024.0 / 1024.0, 2) AS DECIMAL(8, 2)) AS NVARCHAR) + 'GB' VideoSize
                                      ,VideoTime
                                      ,FileStatus
                                      ,sr1.ItemName ConverterTitle
                                      ,sr2.ItemName LocationName
                                      ,CreateDate
                                      ,ConfirmMark
                                      ,ConfirmUser
                                      ,ConfirmDate
                                  FROM UploadTemp
                                       JOIN SysReference sr1
                                            ON sr1.ItemType = 330
                                           AND sr1.ItemValue = ConverterStatus
                                       JOIN SysReference sr2
                                            ON sr2.ItemType = 200
                                           AND sr2.ItemValue = Location
                                 WHERE ConfirmMark = 1
                                   AND ConfirmUser = @ConfirmUser";


            }

            var dic = new Dictionary<string, string>();



            dic.Add("ConfirmUser", objEmp.EmployeeID);
            
            var db = new DBOperate();

            gvResults.DataSource = db.DBGetTable(SQL, dic);
            gvResults.DataBind();
            
        }

        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.FindControl("lalCheck") is Literal c)
                {
                    var id = DataBinder.Eval(e.Row.DataItem, "UploadID").ToString();
                    var status = DataBinder.Eval(e.Row.DataItem, "FileStatus").ToString();
                    var confirm = (bool)DataBinder.Eval(e.Row.DataItem, "ConfirmMark");
                    var videotime = DataBinder.Eval(e.Row.DataItem, "VideoTime").ToString();
                    var biggetype = "";
                    
                    TimeSpan.TryParse(videotime.Split('.')[0], out TimeSpan tVideo);

                    if (tVideo.CompareTo(limitTime15) <= 0) biggetype = "15";
                    else if (tVideo.CompareTo(limitTime15) > 0 && tVideo.CompareTo(limitTime30) <= 0) biggetype = "30";
                    else biggetype = "60";

                    if (confirm)
                    {
                        c.Text = "已確認";
                    }
                    else
                    {
                        var html = $"<input type=\"checkbox\" ID=\"cbConfirm_{id}\" data-biggetype=\"{biggetype}\" value=\"{id}\" onchange=\"countMax(this)\" /><label for=\"cbConfirm_{id}\">確認</label>";
                        html += $"<select id=\"Location_{id}\"><option value=\"\">請選擇</option><option value=\"PTS\">公視</option><option value=\"Hakka\">客台</option><option value=\"Macroview\">宏觀</option><option value=\"HakkaMgz\">客雜</option></select> ";

                        if (status == "0")
                        {
                            c.Text = html;
                        }
                        else if (status == "9")
                        {
                         
                            if(rule.IsUploadBigger30 || rule.IsUploadBigger60)
                            {

                                if(maxNumber >= 20)
                                {
                                    c.Text = "影片超時";
                                }
                                else
                                {
                                    TimeSpan limitTime;

                                    if (rule.IsUploadBigger30) limitTime = limitTime30;
                                    else limitTime = limitTime60;

                                    if (tVideo.CompareTo(limitTime) > 0)
                                    {
                                        c.Text = "影片超時";
                                    }
                                    else
                                    {
                                        c.Text = html;
                                    }
                                }

                                
                                
                               
                            }
                            else
                            {
                                c.Text = "影片超時";
                            }

                            e.Row.Attributes.Add("style", "background-color:#b30000; color:#ffffe6");
                        }
                        else if (status == "99")
                        {
                            
                            c.Text = "無法解析影片長度";
                          
                            e.Row.Attributes.Add("style", "background-color:#b33c00; color:#ffffe6");

                        }
                        else if (status == "991")
                        {
                            c.Text = "無法解析影片資訊";
                            e.Row.Attributes.Add("style", "background-color:#ff5500; color:#ffffe6");
                        }
                    }


                }

            }
        }

    }
}