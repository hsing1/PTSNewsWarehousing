﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace NewsWarehousingWeb
{
    public class NewsHelper
    {
        /// <summary>
        /// 取得iNews文稿檔
        /// </summary>
        /// <param name="Source">文稿類型</param>
        /// <param name="Target">檔案名稱(包含檔名)</param>
        /// <returns></returns>
        public string GetNewsFile(string Source, string Target)
        {
            WriteLog(DateTime.Now.ToString());
            WriteLog("Start!");
            try
            {
                var FullFile = Path.Combine(HttpContext.Current.Server.MapPath("~/TempFiles/"), Target);

                WriteLog(FullFile);

                ProcessStartInfo psi = new ProcessStartInfo(@"C:\Windows\System32\cmd");
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = ProcessWindowStyle.Minimized;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;

                Process process = Process.Start(psi);
                string cmdForTunnel = HttpContext.Current.Server.MapPath("~/Bin/plink") + " 10.1.163.1 -l newscg -pw p@ssw0rd \"/exc/doc -g `cat /list/" + Source + "`\" > " + FullFile;

                WriteLog("AAA!");
                WriteLog(cmdForTunnel);

                process.StandardInput.WriteLine(cmdForTunnel);
                //process.WaitForExit();
                //SpinWait.SpinUntil(() => false, 100000);
                Thread.Sleep(80000);

                WriteLog("BBB!");

                //DoBusinessLogic();
                process.StandardInput.WriteLine("exit");
                //SpinWait.SpinUntil(() => false, 10000);
                Thread.Sleep(10000);

                WriteLog("CCC!");

                //string output = process.StandardOutput.ReadToEnd();

                //process.WaitForExit();

                if (process.HasExited)
                {
                    process.Close();
                    process.Dispose();
                }

                WriteLog("Return!");
                return FullFile;

                
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        protected void WriteLog(string strLine)
        {
            string strFileName = HttpContext.Current.Server.MapPath("~/TempFiles/Log.txt");

            StreamWriter file = new StreamWriter(strFileName, true);

            file.WriteLine("\r\n" + strLine);

            file.Close();
        }

        /// <summary>
        /// 解析文稿內容
        /// </summary>
        /// <param name="FullFile">文稿本文檔名</param>
        /// <param name="type">文稿類型</param>
        /// <param name="tag">新聞參數</param>
        /// <returns>List<Dictionary<string, string>>() 物件</returns>
        public List<Dictionary<string, string>> NewsResolve(string FullFile, string NewsType)
        {
            int i = 0;

            var info = new FileInfo(FullFile);

            while (IsFileLocked(info))
            {
                if (i >= 20) break;

                SpinWait.SpinUntil(() => false, 2000);
                i++;
            }

            if (i >= 20) return null;

            string Document = File.ReadAllText(FullFile);

            string tmpDoc = "", storyid = "", slogan = "";

            var liData = new List<Dictionary<string, string>>();
            Dictionary<string, string> dicDetails;

            while (Document.Replace("\r\n", "").Length > 0)
            {
                tmpDoc = Document.Substring(0, Document.IndexOf("</nsml>") + 7);
                Document = Document.Substring(Document.IndexOf("</nsml>") + 7, Document.Length - Document.IndexOf("</nsml>") - 7);

                dicDetails = new Dictionary<string, string>();

                dicDetails.Add("NewsRaw", tmpDoc);

                storyid = tmpDoc.Substring(tmpDoc.IndexOf("<storyid>") + 9, tmpDoc.IndexOf("</storyid>") - (tmpDoc.IndexOf("<storyid>") + 9));
                storyid = Regex.Replace(storyid, @"<[^>]*>", String.Empty);
                dicDetails.Add("storyid", storyid);

                slogan = tmpDoc.Substring(tmpDoc.IndexOf("\"title\">") + 8, tmpDoc.IndexOf("</string>", tmpDoc.IndexOf("\"title\">")) - (tmpDoc.IndexOf("\"title\">") + 8));
                slogan = Regex.Replace(slogan, @"<[^>]*>", String.Empty);


                dicDetails.Add("Slogan", slogan);
                dicDetails.Add("NewsType", NewsType);
                
                liData.Add(dicDetails);

            }

            Document = "";
            return liData;
        }

        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
    }
}