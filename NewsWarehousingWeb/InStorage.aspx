﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="InStorage.aspx.cs" Inherits="NewsWarehousingWeb.InStorage" %>

<%@ Register Assembly="PTS.Controls" Namespace="PTS.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        article, aside, figure, figcaption, footer, header, hgroup, menu, nav, section {
            display: block;
        }


        .auto-style2 {
            float: left;
            width: 50%;
            height: 276px;
        }

        .auto-style6 {
            height: 113px;
        }

        .auto-style7 {
            height: 203px;
            width: 208px;
        }

        .auto-style8 {
            height: 24px;
        }

        .auto-style9 {
            float: left;
            width: 50%;
            height: 264px;
        }

        .auto-style10 {
            height: 125px;
        }

        .td1 {
            width: 20%;
        }

        .td2 {
            width: 60%;
            text-align: center;
        }

        .td3 {
            width: 25%;
            text-align: right;
        }

        .auto-style11 {
            padding: 3px;
            BACKGROUND-COLOR: #f4dfe5;
            COLOR: #a80054;
            FONT-FAMILY: "新細明體","Verdana", "Arial", "Helvetica", "sans-serif";
            FONT-SIZE: 8pt;
            LETTER-SPACING: 1px;
            LINE-HEIGHT: 130%;
            TEXT-ALIGN: left;
            border: 1px solid #ebc9d2;
            width: 1572px;
        }
        .auto-style12 {
            border: 1px solid #abd0e0;
            BACKGROUND-COLOR: #E0EEF7;
            COLOR: #010305;
            FONT-FAMILY: "新細明體";
            FONT-SIZE: 9pt;
            text-align: left;
            LETTER-SPACING: 1px;
            PADDING-BOTTOM: 1px;
            PADDING-LEFT: 3px;
            PADDING-RIGHT: 1px;
            PADDING-TOP: 2px;
            TEXT-ALIGN: left;
            width: 20%;
        }
        .auto-style13 {
            width: 1113px;
        }

        .auto-style14 {
            width: 1573px;
        }
        .auto-style15 {
            float: left;
            width: 618px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table class="auto-style11">
        <tr>
            <td>
                <table class="auto-style13">
                    <tr>
                        <td class="tdHeader-Blue" style="width: 10%">標題</td>
                        <td class="tdValue-Blue" style="width: 20%">
                            <asp:TextBox ID="QrySlogan" Style="width: 180px" runat="server"></asp:TextBox>
                        </td>
                        <td class="tdHeader-Blue" style="width: 10%">日期</td>
                        <td class="tdValue-Blue" style="width: 30%">
                            <asp:TextBox ID="CalendarB" Style="width: 100px" runat="server" MaxLength="8"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarB_CalendarExtender" runat="server" TargetControlID="CalendarB" Format="yyyyMMdd" DaysModeTitleFormat="MM, yyyy" TodaysDateFormat="yyyy/MM/dd" />
                            ~
                            <asp:TextBox ID="CalendarE" Style="width: 100px" runat="server" MaxLength="8"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarE_CalendarExtender" runat="server" TargetControlID="CalendarE" Format="yyyyMMdd" DaysModeTitleFormat="MM, yyyy" TodaysDateFormat="yyyy/MM/dd" PopupButtonID="imgCalendar2" />
                        </td>
                        <td class="tdHeader-Blue" style="width: 10%">入庫位置</td>
                        <td class="auto-style12">
                            <asp:DropDownList ID="ddlNewsType" runat="server" DataValueField="ItemValue" DataTextField="ItemName">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeader-Blue" style="width: 10%">僅未核可項目</td>
                        <td class="tdValue-Blue" style="width: 20%">
                            <asp:CheckBox ID="cbNonApproveStatus" runat="server" />
                        </td>
                        <td class="tdHeader-Blue" style="text-align: left" colspan ="4">
                            <table style="width:100%; border-collapse:collapse;">
                                <tr>
                                    <td style="padding:0 0 0 0; text-align:left; width:50%;"><asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" /></td>
                                    <td style="padding:0 0 0 0; text-align:right; width:50%;">
                                        <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_Click" Text="產生Excel(客台)" OnClientClick="return CheckDate();" Visible="False" />
                                        <asp:Button ID="btnDelete" runat="server" Text="刪除" OnClientClick="return GetDelete();" OnClick="btnDelete_Click" /></td>
                                </tr>
                            </table>
                            
                            
                        </td>
                        
                    </tr>
                    <tr>
                        <td colspan="6" style="height: 297px">
                            <asp:GridView ID="GridView1" runat="server" Height="297px" Width="1101px" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="VideoID" EmptyDataText="查無資料" OnRowCommand="GridView1_RowCommand" OnPageIndexChanged="GridView1_PageIndexChanged" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" Text="選取" CommandArgument='<%# Eval("VideoID") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="系統編號">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="td1"></td>
                                                    <td class="td2">系統編號</td>
                                                    <td class="td3">
                                                        <asp:ImageButton ID="Sort_VideoID" runat="server" ImageUrl="~/App_Images/arrowup.png" CommandArgument="VideoID" OnCommand="Sort_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("VideoID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="標題">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="td1"></td>
                                                    <td class="td2">標題</td>
                                                    <td class="td3">
                                                        <asp:ImageButton ID="Sort_Slogan" runat="server" ImageUrl="~/App_Images/arrowup.png" CommandArgument="Slogan" OnCommand="Sort_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Slogan") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="180px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="製播/拍攝日期">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="td1"></td>
                                                    <td class="td2">製播/拍攝<br />
                                                        日期</td>
                                                    <td class="td3">
                                                        <asp:ImageButton ID="Sort_CameraDate" runat="server" ImageUrl="~/App_Images/arrowup.png" CommandArgument="CameraDate" OnCommand="Sort_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("CameraDate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="來源">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="td1"></td>
                                                    <td class="td2">來源</td>
                                                    <td class="td3">
                                                        <asp:ImageButton ID="Sort_DeptName" runat="server" ImageUrl="~/App_Images/arrowup.png" CommandArgument="DeptName" OnCommand="Sort_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="建檔日期">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="td1"></td>
                                                    <td class="td2">建檔日期</td>
                                                    <td class="td3">
                                                        <asp:ImageButton ID="Sort_CreateDate" runat="server" ImageUrl="~/App_Images/arrowup.png" CommandArgument="CreateDate" OnCommand="Sort_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Literal ID="lalCreateDate" runat="server"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="審核">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="td1"></td>
                                                    <td class="td2">審核<br />
                                                        結果</td>
                                                    <td class="td3">
                                                        <asp:ImageButton ID="Sort_Approve" runat="server" ImageUrl="~/App_Images/arrowup.png" CommandArgument="Approve" OnCommand="Sort_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("Approve") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="審核日期">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="td1"></td>
                                                    <td class="td2">審核日期</td>
                                                    <td class="td3">
                                                        <asp:ImageButton ID="Sort_ApproveDate" runat="server" ImageUrl="~/App_Images/arrowup.png" CommandArgument="ApproveDate" OnCommand="Sort_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Literal ID="lalApproveDate" runat="server"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="審核者" DataField="ApproveUser">
                                        <ItemStyle />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="檔案狀態" DataField="FileStstus" />
                                    <asp:TemplateField HeaderText="刪除">
                                        <ItemTemplate>
                                            <asp:Literal ID="lalDelete" runat="server"></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#FFCC99" />
                                <RowStyle Font-Names="微軟正黑體" Font-Size="Small" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <SelectedRowStyle BackColor="White" />
                            </asp:GridView>

                        </td>
                    </tr>
                </table>

            </td>
            <td>
                <table class="tblBase-Red" style="width: 440px;">
                    <tr>
                        <td class="tdHeader-Green">入庫影像</td>
                    </tr>
                    <tr>
                        <td style="height: 309px">
                            <asp:Literal ID="lallowVideo" runat="server"></asp:Literal>
                            <span id="currentFrame">0</span>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
    </table>


    <asp:HiddenField ID="pkIndex" runat="server" />
    <asp:HiddenField ID="hfNewsID" runat="server" />
    <asp:HiddenField ID="hfPage" runat="server" />
    <asp:HiddenField ID="hfDelete" runat="server" />



    <asp:Panel runat="server" ID="Panel1">
        <div class="auto-style14">
            <table style="width: 100%" class="tblBase-Red">
                <tr>
                    <td style="width: 3%">
                        <asp:Button runat="server" Text="儲存詮釋資料" ID="btnSave" OnClick="btnSave_Click" /></td>
                    <td>

                        <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red" Font-Size="Small"></asp:Label>

                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 30%; float: left;">
            <div>
                <table style="width: 100%" class="tblBase-Red">
                    <tr>
                        <td class="tdHeader-Blue" style="width: 12%">標題</td>
                        <td>
                            <cc1:PTSTextBox ID="Slogan" Style="width: 100%" DBColumn="Slogan" runat="server"></cc1:PTSTextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="float: left; width: 50%">
                <table style="width: 100%" class="tblBase-Red">
                    <tr>
                        <td class="tdHeader-Green">製播/拍攝日期</td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:PTSTextBox ID="YY" Style="width: 25%" DBColumn="YY" runat="server"></cc1:PTSTextBox>
                            年<cc1:PTSTextBox ID="MM" Style="width: 15%" DBColumn="MM" runat="server"></cc1:PTSTextBox>
                            月<cc1:PTSTextBox ID="DD" Style="width: 15%" DBColumn="DD" runat="server"></cc1:PTSTextBox>
                            日</td>
                    </tr>
                    <tr>
                        <td>
                            <input id="btnBeforeDay" type="button" value="前天" onclick="CalculateDate(0);" />&nbsp;
                                    <input id="btnYesterday" type="button" value="昨天" onclick="CalculateDate(1);" />&nbsp;
                                    <input id="btnToday" type="button" value="今天" onclick="CalculateDate(2);" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeader-Green">分類號及名稱</td>
                    </tr>
                    <tr>
                        <td>1.<cc1:PTSTextBox ID="class1" Style="width: 15%; margin-bottom: 5px" DBColumn="Class1" runat="server" onclick="openwin(this);"></cc1:PTSTextBox>&nbsp;<cc1:PTSTextBox ID="class1Name" Style="margin-bottom: 5px; width: 130px" DBColumn="className1" runat="server"></cc1:PTSTextBox><br />
                            2.<cc1:PTSTextBox ID="class2" Style="width: 15%; margin-bottom: 5px" DBColumn="Class2" runat="server" onclick="openwin(this);"></cc1:PTSTextBox>&nbsp;<cc1:PTSTextBox ID="class2Name" Style="margin-bottom: 5px; width: 130px" DBColumn="className2" runat="server"></cc1:PTSTextBox><br />
                            3.<cc1:PTSTextBox ID="class3" Style="width: 15%; margin-bottom: 5px" DBColumn="Class3" runat="server" onclick="openwin(this);"></cc1:PTSTextBox>&nbsp;<cc1:PTSTextBox ID="class3Name" Style="margin-bottom: 5px; width: 130px" DBColumn="className3" runat="server"></cc1:PTSTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeader-Green">影像來源及性質</td>
                    </tr>
                    <tr>
                        <td style="height: 77px">頻道別:
                          <cc1:PTSDropDownList ID="Channel" runat="server" Style="width: 50%; margin-bottom: 5px" DBColumn="Channel" InitialFields="Chanel" AppendDataBoundItems="True">
                              <asp:ListItem></asp:ListItem>
                          </cc1:PTSDropDownList>
                            <br />
                            來源:  &nbsp;&nbsp;
                          <cc1:PTSDropDownList ID="Dept" runat="server" Style="width: 50%; margin-bottom: 5px" DBColumn="Dept" InitialFields="Dept" AppendDataBoundItems="True">
                              <asp:ListItem></asp:ListItem>
                          </cc1:PTSDropDownList>
                            <br />
                            性質:  &nbsp;&nbsp;
                          <cc1:PTSDropDownList ID="Nature" runat="server" Style="width: 50%;" DBColumn="Nature" InitialFields="Nature" AppendDataBoundItems="True">
                              <asp:ListItem></asp:ListItem>
                          </cc1:PTSDropDownList>
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="auto-style2">
                <table runat="server" class="tblBase-Red" style="width: 100%">
                    <tr>
                        <td class="tdHeader-Green" colspan="2">關鍵詞及摘要</td>
                    </tr>
                    <tr>
                        <td style="width: 21%">摘要:&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <cc1:PTSTextBox ID="Summary" runat="server" dbcolum="Summary" Rows="11" Columns="16" TextMode="MultiLine" DBColumn="Summary"></cc1:PTSTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>關鍵詞:</td>
                        <td>
                            <cc1:PTSTextBox ID="KeyWord" DBColumn="KeyWord" runat="server"></cc1:PTSTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeader-Green" colspan="2">採訪地點</td>
                    </tr>
                    <tr>
                        <td style="text-indent: 15%; height: 33px" colspan="2">
                            <cc1:PTSTextBox ID="Location" DBColumn="Location" runat="server"></cc1:PTSTextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="width: 30%; float: left;">
            <div style="float: left; width: 50%">
                <table runat="server" class="tblBase-Red" style="width: 100%">
                    <tr>
                        <td class="tdHeader-Green">音軌</td>
                    </tr>
                    <tr>
                        <td>1.<cc1:PTSDropDownList ID="Tracks1" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks1" InitialFields="Track" AppendDataBoundItems="True" onchange="checkList(1);">
                            <asp:ListItem></asp:ListItem>
                        </cc1:PTSDropDownList>
                            <cc1:PTSDropDownList ID="TracksLang1" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks1_Lang" InitialFields="TacksLang" AppendDataBoundItems="True">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <br />
                            2.<cc1:PTSDropDownList ID="Tracks2" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks2" InitialFields="Track" AppendDataBoundItems="True" onchange="checkList(2);">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <cc1:PTSDropDownList ID="TracksLang2" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks2_Lang" InitialFields="TacksLang" AppendDataBoundItems="True">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <br />
                            3.<cc1:PTSDropDownList ID="Tracks3" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks3" InitialFields="Track" AppendDataBoundItems="True" onchange="checkList(3);">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <cc1:PTSDropDownList ID="TracksLang3" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks3_Lang" InitialFields="TacksLang" AppendDataBoundItems="True">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <br />
                            4.<cc1:PTSDropDownList ID="Tracks4" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks4" InitialFields="Track" AppendDataBoundItems="True" onchange="checkList(4);">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <cc1:PTSDropDownList ID="TracksLang4" runat="server" Style="width: 40%; margin-bottom: 5px" DBColumn="Tracks4_Lang" InitialFields="TacksLang" AppendDataBoundItems="True">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeader-Green">記者</td>
                    </tr>
                    <tr>
                        <td><a class="tdValue-Blue" style="line-height: 25px">屬性</a>
                            <cc1:PTSDropDownList ID="Report" runat="server" Style="width: 36%; margin-bottom: 5px" DBColumn="Report" InitialFields="Report" AppendDataBoundItems="True">
                                <asp:ListItem></asp:ListItem>
                            </cc1:PTSDropDownList>
                            <a class="tdValue-Blue" style="line-height: 25px">名稱</a>
                            <cc1:PTSTextBox ID="ReportName" DBColumn="ReportName" Style="width: 28%; margin-bottom: 5px" runat="server"></cc1:PTSTextBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style6">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" OnClientClick="if(checkData()==false){return false;};" Text="加入" />
                                    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="None" DataKeyNames="ReporterID" Font-Names="微軟正黑體" Font-Size="Small" OnPageIndexChanged="GridView2_PageIndexChanged" OnPageIndexChanging="GridView2_PageIndexChanging" PageSize="3" Width="207px" OnRowCommand="GridView2_RowCommand">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="ReportDel" runat="server" CommandArgument='<%# Eval("ReporterID") %>'>刪除</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ReporterType" HeaderText="屬性">
                                                <ControlStyle Width="30px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ReporterName" HeaderText="名稱">
                                                <ControlStyle Width="50px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ReporterID" InsertVisible="False" ShowHeader="False" Visible="False" />
                                        </Columns>
                                        <PagerStyle Font-Names="微軟正黑體" Font-Size="X-Small" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <RowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="auto-style9">
                <table runat="server" class="tblBase-Red" style="width: 100%">
                    <tr>
                        <td class="tdHeader-Green">倉儲資訊及版權</td>
                    </tr>
                    <tr>
                        <td class="auto-style8"><a class="tdValue-Blue" style="line-height: 25px">版權範圍</a>
                            <cc1:PTSRadioButtonList ID="Copyright" runat="server" Font-Size="8pt" ForeColor="#A80054" RepeatDirection="Horizontal">
                                <asp:ListItem Value="0">完全</asp:ListItem>
                                <asp:ListItem Value="1">部分</asp:ListItem>
                                <asp:ListItem Value="2">無權利</asp:ListItem>
                            </cc1:PTSRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeader-Green">版權描述</td>
                    </tr>
                    <tr>
                        <td class="auto-style10">
                            <cc1:PTSTextBox ID="Description" runat="server" class="auto-style7" TextMode="MultiLine" DBColumn="Description" Height="213px"></cc1:PTSTextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="auto-style15">
            <table class="tblBase-Red" style="width: 100%">
                <tr>
                    <td class="tdHeader-Green">iNews 文稿</td>
                </tr>
                <tr>
                    <td>
                        <cc1:PTSTextBox ID="Body" runat="server" DBColumn="Body" Columns="68" Rows="18" TextMode="MultiLine" Height="288px"></cc1:PTSTextBox>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server">
        <table>
            <tr>
                <td>入庫審核
                </td>
                <td>
                    <asp:RadioButtonList ID="rblApprove" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>

                </td>
                <td>
                    <asp:Button ID="btnApprove" runat="server" Text="確認" OnClick="btnApprove_Click" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script src="App_Js/VideoFrame.min.js"></script>

    <script>
        var myVar = setInterval(ClearMsg, 5000);

        function ClearMsg() {
            $('#ContentPlaceHolder1_lblMsg').html('');
        }

        var m = document.getElementById("lowVideo");

        
        if (m != null) {
            var currentFrame = $('#currentFrame');
            var video = VideoFrame({
                id: 'lowVideo',
                frameRate: 29.97,
                callback: function (frame) {
                    currentFrame.html(frame);
                }
            });

        }

        function GetDelete() {
            
            deleteItems = [];


            $("input[id^='cbDelete_']").each(function () {
                if ($(this).prop("checked")) {
                    deleteItems.push($(this).prop("value"));
                }
            });

            if (deleteItems.length === 0) {
                alert('請最少選擇一個項目!');
                return false;
            } else {
                $('#<% =hfDelete.ClientID %>').val(JSON.stringify(deleteItems));
                return true;
            }
        }

        function CheckDate() {

            var msg = "";

            if ($('#<% =CalendarB.ClientID %>').val() == "") {
                msg += "請輸入起日!\r\n";
            }

            if ($('#<% =CalendarE.ClientID %>').val() == "") {
                msg += "請輸入迄日!\r\n";
            }

            if(msg == "") {
                return true;
            } else {
                alert(msg);
                return false;
            }

            
        }

        $(document).ready(function () {
            if (m != null) {

                video.listen('frame');
            }
            if ($('#<% =Tracks1.ClientID %>').val() == "") {
                $('#<% =Tracks2.ClientID %>').prop('disabled', true);
                $('#<% =TracksLang2.ClientID %>').prop('disabled', true);
            } else if ($('#<% =Tracks1.ClientID %>').val() == "A") {
                $('#<% =TracksLang1.ClientID %>').prop('disabled', true);
            }
            if ($('#<% =Tracks2.ClientID %>').val() == "") {
                $('#<% =Tracks3.ClientID %>').prop('disabled', true);
                $('#<% =TracksLang3.ClientID %>').prop('disabled', true);
            } else if ($('#<% =Tracks2.ClientID %>').val() == "A") {
                $('#<% =TracksLang2.ClientID %>').prop('disabled', true);
            }
            if ($('#<% =Tracks3.ClientID %>').val() == "") {
                $('#<% =Tracks4.ClientID %>').prop('disabled', true);
                $('#<% =TracksLang4.ClientID %>').prop('disabled', true);
            } else if ($('#<% =Tracks3.ClientID %>').val() == "A") {
                $('#<% =TracksLang3.ClientID %>').prop('disabled', true);
            }
            if ($('#<% =Tracks4.ClientID %>').val() == "A") {
                $('#<% =TracksLang4.ClientID %>').prop('disabled', true);
            }
            $('#<% =class1Name.ClientID %>').prop('disabled', true);
            $('#<% =class2Name.ClientID %>').prop('disabled', true);
            $('#<% =class3Name.ClientID %>').prop('disabled', true);
        });

        function checkList(num) {
            if ($('#ContentPlaceHolder1_Tracks' + num).val() == 'A') {
                $('#ContentPlaceHolder1_TracksLang' + num).val("");
                $('#ContentPlaceHolder1_TracksLang' + num).prop('disabled', true);

                $('#ContentPlaceHolder1_Tracks' + (num + 1)).prop('disabled', false);
                $('#ContentPlaceHolder1_TracksLang' + (num + 1)).prop('disabled', false);

            } else {
                $('#ContentPlaceHolder1_TracksLang' + num).prop('disabled', false);

                if ($('#ContentPlaceHolder1_Tracks' + num).val() != "") {
                    $('#ContentPlaceHolder1_Tracks' + (num + 1)).prop('disabled', false);
                    $('#ContentPlaceHolder1_TracksLang' + (num + 1)).prop('disabled', false);
                } else {
                    $('#ContentPlaceHolder1_Tracks' + (num + 1)).prop('disabled', true);
                    $('#ContentPlaceHolder1_TracksLang' + (num + 1)).prop('disabled', true);
                }
            }
        }
        function openwin(obj) {

            var id = $(obj).prop('id');


            window.open("OpenWin.aspx?class=" + id, "分類號及名稱", "width=450,height=300");
        }

        function CalculateDate(num) {
            //0 前天
            //1 昨天
            //2 今天
            var today = new Date();
            $('#ContentPlaceHolder1_YY').val(today.getFullYear());
            if (num == 0) {
                var newday = addDate(today.getFullYear(), today.getMonth(), today.getDate(), 2);
                $('#ContentPlaceHolder1_MM').val(newday.getMonth() + 1 < 10 ? '0' + (newday.getMonth() + 1) : (newday.getMonth() + 1));
                $('#ContentPlaceHolder1_DD').val(newday.getDate() < 10 ? '0' + newday.getDate() : newday.getDate());
            }
            if (num == 1) {
                var newday = addDate(today.getFullYear(), today.getMonth(), today.getDate(), 1);
                $('#ContentPlaceHolder1_MM').val(newday.getMonth() + 1 < 10 ? '0' + (newday.getMonth() + 1) : (newday.getMonth() + 1));
                $('#ContentPlaceHolder1_DD').val(newday.getDate() < 10 ? '0' + newday.getDate() : newday.getDate());
            }
            if (num == 2) {
                $('#ContentPlaceHolder1_MM').val(today.getMonth() + 1 < 10 ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1));
                $('#ContentPlaceHolder1_DD').val(today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
            }
        }

        function addDate(dy, dmomth, dd, dadd) {
            var myDate = new Date(dy, dmomth, dd)
            var dayOfMonth = myDate.getDate();
            myDate.setDate(dayOfMonth - dadd);
            return myDate;
        }

        function checkData() {
            if ($('#ContentPlaceHolder1_Report').val() == "" || $('#ContentPlaceHolder1_ReportName').val().Trim() == "") {
                alert("請確實填寫記者屬性或名稱");
                return false;
            }
        }
    </script>
</asp:Content>
