﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("catalogDetails")]
    public class CatalogDetails : Catalog
    {
        public CatalogDetails()
        {
        }

        public List<Property> properties;

    }
}
