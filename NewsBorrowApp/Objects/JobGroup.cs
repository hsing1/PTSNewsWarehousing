﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("jobGroup")]
    public class JobGroup
    {

        public string id { get; set; }
        public string type { get; set; }
        public string createTime { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string usersId { get; set; }
        public string status { get; set; }
        public string progress { get; set; }
        public string cancelable { get; set; }
        public string cancelRequired { get; set; }
        public string jobCount { get; set; }

        public JobGroup() { }

        public JobGroup(string id, string type, string status, string progress, string usersId, string createTime, string startTime, string endTime)
        {
            this.id = id;
            this.type = type;
            this.status = status;
            this.progress = progress;
            this.usersId = usersId;
            this.createTime = createTime;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }
}
