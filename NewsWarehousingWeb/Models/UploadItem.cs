﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsWarehousingWeb.Models
{
    public class UploadItem
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public int BiggerType { get; set; }
    }
}