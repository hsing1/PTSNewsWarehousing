﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="NewsWarehousingWeb.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="App_CSS/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>
                <table class="tblBase-Blue" style="width: 1120px;">
                    <tr>
                        <td class="tdHeader-Blue">查詢條件</td>
                        <td class="tdValue-Blue">
                            <asp:TextBox ID="txtKeyWord" runat="server" Width="223px"></asp:TextBox>
                            <span style="color:red">多個關鍵字時，請用+號分隔</span>
                        </td>
                        <td class="tdHeader-Blue">播映日期</td>
                        <td class="tdValue-Blue">
                            <asp:TextBox ID="CalendarB" Style="width: 100px" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarB_CalendarExtender" runat="server" TargetControlID="CalendarB" Format="yyyyMMdd" DaysModeTitleFormat="MM, yyyy" TodaysDateFormat="yyyy/MM/dd" />
                            ~
                        <asp:TextBox ID="CalendarE" Style="width: 100px" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarE_CalendarExtender" runat="server" TargetControlID="CalendarE" Format="yyyyMMdd" DaysModeTitleFormat="MM, yyyy" TodaysDateFormat="yyyy/MM/dd" />
                            &nbsp;
                        <asp:Button ID="btnQuery" runat="server" Text="搜尋" OnClick="btnQuery_Click" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="tdValue-Blue">
                            <div style="width: 100%; height: 800px; overflow: auto;">
                                <asp:GridView ID="gvResult" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                                    Width="100%" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" OnRowDataBound="gvResult_RowDataBound" OnPageIndexChanged="gvResult_PageIndexChanged" OnPageIndexChanging="gvResult_PageIndexChanging" ValidateRequestMode="Disabled" ViewStateMode="Disabled">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <a href="javascript: preView('<%# Eval("VideoID") %>', '<%# Eval("Mp4Name") %>');">預覽</a>
                                                <asp:Literal ID="lalTimeCode" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                            <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="標題">
                                            <ItemTemplate>
                                                <asp:Literal ID="lalSlogan" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="180px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CameraDate" HeaderText="撥映日期">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="80px"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="版權">
                                            <ItemTemplate>
                                                <asp:Literal ID="lalCopyright" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="長度" DataField="Duration" >
                                        <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="內容">
                                            <ItemTemplate>
                                                <div style="width: 100%; word-wrap: break-word; word-break: break-all;">
                                                    <asp:Literal ID="lalBody" runat="server"></asp:Literal>
                                                    <a href="javascript: openWindow('<%# Eval("NewsID") %>')">...more</a>
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <table style="border: 1px solid Black; border-collapse: collapse;">
                                                    <tr>
                                                        <td style="border: 1px solid Black; text-align: center">
                                                            <asp:Literal ID="LiteralA" runat="server"></asp:Literal>
                                                        </td>
                                                        <td style="border: 1px solid Black; text-align: center">
                                                            <asp:Literal ID="LiteralB" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <asp:Literal ID="lalMsg" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table class="tblBase-Blue" style="width: 440px;">
                    <tr>
                        <td class="tdHeader-Blue" style="text-align: center">入庫影像</td>
                    </tr>
                    <tr>
                        <td style="height: 439px">
                            <video id="lowVideo" autoplay controls width="431" height="307">
                            </video>
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td style="height: 439px">
                            開始時間:
                           01:<select id="startMinute">
                               <option>00</option>
                              </select>:<select id="startSecond">
                               <option>00</option>
                              </select>
                            結束時間:
                           01:<select id="endMinute">
                               <option>00</option>
                              </select>:<select id="endSecond">
                               <option>00</option>
                              </select>
                            <input id="Button1" type="button" value="加入調用清單" />
                        </td>
                        
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="dialog" title="內容">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script src="App_Js/jquery-ui.min.js"></script>
    <script>
        var preItem = {
            id: 0,
            color: ''
        };

        //var myVar = setInterval(ClearMsg, 5000);

        //function ClearMsg() {
        //    $("span[id^='Msg']").each(function () {
        //        $(this).html('');
        //    });
        //}

        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                width: 600,
                position: { my: "center+100px", at: "top+200px", of: window }
            });
        });

        var video = document.getElementById("lowVideo");

        function preView(id, name) {
            video.src = 'http://newsmam.pts.org.tw/mp4/' + name;
            video.play();

            if ($('#row_' + preItem.id) != null) {
                $('#row_' + preItem.id).css('backgroundColor', preItem.color);
            }

            preItem.id = id;
            preItem.color = $('#row_' + id).css('backgroundColor');

            $('#row_' + id).css('backgroundColor', '#ffffb3');

        }

        function openWindow(id) {
            $.ajax({
                type: 'post',
                async: false,
                url: "SRV/NewsBody.ashx",
                data: 'id=' + id,
                param: Math.random(),
                success: function (data) {
                    $("#dialog").html(data);
                    $("#dialog").dialog("open");
                }
            })
        }

        function toBorrow(id, type) {
            $.ajax({
                type: 'post',
                async: false,
                url: "SRV/Borrow.ashx",
                data: 'id=' + id + '&Type=' + type,
                param: Math.random(),
                success: function (data) {
                    $("#Msg_" + id).html(data);
                    setTimeout(function () { $('#Msg_' + id).html('') }, 2000);
                }
            })
        }
    </script>
</asp:Content>
