﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.Employee;
using NewsWarehousingWeb.Models;
using Newtonsoft.Json;

namespace NewsWarehousingWeb
{
    public partial class BorrowList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            SetPageDate();
        }

        void SetPageDate()
        {
            var db = new PTS.Modules.DBUtility.DBOperate();

            var objEmp = new EmployeeInfo();

            var dt = db.DBGetTable(@"SELECT BorrowList.BorrowID 
                                           , nm.Slogan 
                                           , sr.ItemName 
                                           , FilePosition 
                                           , BorrowList.BorrowStatus
                                           , CASE BorrowList.BorrowType WHEN 'A' THEN '自用'
                                                                        WHEN 'B' THEN '進GV'
                                             END BorrowType
                                       FROM BorrowList
                                            JOIN SysReference sr
                                                 ON sr.ItemType = 300
                                                AND sr.ItemValue = BorrowStatus
                                            JOIN VideoMeta vm
                                                 ON BorrowList.VideoID = vm.VideoID
                                            JOIN NewsMeta nm
                                                 ON vm.NewsID = nm.NewsID
                                      WHERE UserID = @UserID
                                      ORDER BY BorrowList.BorrowID DESC ", new Dictionary<string, string> { { "UserID", objEmp.EmployeeID } });

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            var items = JsonConvert.DeserializeObject<BorrowItems>(hfValue.Value);

            var db = new PTS.Modules.DBUtility.DBOperate();

            if (items.approveItems.Count > 0)
            {
                var m = new List<Dictionary<string, string>>();

                foreach (var item in items.approveItems)
                {
                    m.Add(new Dictionary<string, string> { { "BorrowID", item.ToString() }, { "BorrowStatus", "1" } });
                }

                db.DBMultiUpdate(ref m, "BorrowList", "BorrowID");
            }

            if (items.deleteItems.Count > 0)
            {
                var n = new List<Dictionary<string, string>>();

                foreach (var item in items.deleteItems)
                {
                    n.Add(new Dictionary<string, string> { { "BorrowID", item.ToString() } });
                }

                db.DBMultiDelete(ref n, "BorrowList");
            }

            SetPageDate();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.DataRow)
            {
                var c = e.Row.FindControl("lalMenu") as Literal;
                if(c != null)
                {
                    var id = DataBinder.Eval(e.Row.DataItem, "BorrowID").ToString();
                    var status = DataBinder.Eval(e.Row.DataItem, "BorrowStatus").ToString();

                    if (status == "0")
                    {

                        var html = "<table style=\"width: 100%\"><tr><td style=\"width: 50%; text-align:left;\">"
                                 + "<input type=\"checkbox\" ID=\"cbApprove_" + id + "\" value=\"" + id + "\" onchange=\"Change1(" + id + ");\" /><label for=\"cbApprove_" + id + "\">確認</label>"
                                 + "</td><td style=\"width: 50%; text-align:right;\">"
                                 + "<label for=\"cbDelete_" + id + "\">刪除</label><input type=\"checkbox\" onchange=\"Change2(" + id + ");\" ID=\"cbDelete_" + id + "\" value=\"" + id + "\" />"
                                 + "</td></tr></table>";
                        c.Text = html;
                    }
                    else
                    { 
                        c.Text = "已確認";
                            
                    }

                    
                }
            }
        }

        protected void btnReload_Click(object sender, EventArgs e)
        {
            SetPageDate();
        }
    }
}