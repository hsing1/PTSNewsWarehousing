﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PTS.News.Utilities;

namespace NewsRemoveApp
{
    public partial class RemoveForm : Form
    {
        private const string VERSION = "20180223.1";

        private static object _syncRoot = new object();
        delegate void PrintHandler(TextBox tb, string text);

        private int Delete4AMSec;
        private System.Threading.Timer _ThreadTimerDelete4AM = null;

        string NewsMp4IISPath;
        public RemoveForm()
        {
            InitializeComponent();

            CustomizedInit();
        }
        protected void CustomizedInit()
        {
            sslVersion.Text = "版本：" + VERSION;
            
            NewsMp4IISPath = @"\\10.13.200.58\MP4";
            
            NetHelper.NetUse(@"\\10.13.200.15", "admin", "pts870701");

            DateTime ToDay = DateTime.Now;

            var Delete4AM = Convert.ToDateTime(ToDay.AddDays(1).ToString("yyyy-MM-dd 04:00:00"));

            Delete4AMSec = (int)Math.Round((Delete4AM - ToDay).TotalSeconds, 0);

            _ThreadTimerDelete4AM = new System.Threading.Timer(new System.Threading.TimerCallback(RunAllDelete), null, 1000 * Delete4AMSec, 1000 * 60 * 60 * 24);

            textBox1.Text = $"執行時間:{Delete4AM.ToString()}";

            btnSatrt.Enabled = false;

        }

        void RunAllDelete(object state = null)
        {
            button1.Enabled = false;
            button2.Enabled = false;


            var task1 = Task.Factory.StartNew(() =>
            {
                DeleteBorrowFolder();
            });

            var task2 = Task.Factory.StartNew(() =>
            {
                DeleteDeleteMark();
            });

            Task.WaitAll(task1, task2);

            button1.Enabled = true;
            button2.Enabled = true;

        }

        private void btnSatrt_Click(object sender, EventArgs e)
        {
            DateTime ToDay = DateTime.Now;

            var Delete4AM = Convert.ToDateTime(ToDay.AddDays(1).ToString("yyyy-MM-dd 04:00:00"));

            Delete4AMSec = (int)Math.Round((Delete4AM - ToDay).TotalSeconds, 0);

            _ThreadTimerDelete4AM = new System.Threading.Timer(new System.Threading.TimerCallback(RunAllDelete), null, 1000 * Delete4AMSec, 1000 * 60 * 60 * 24);

            button1.Enabled = true;
            button2.Enabled = true;
            btnSatrt.Enabled = false;
            btnStop.Enabled = true;
        }


        private void btnStop_Click(object sender, EventArgs e)
        {
            _ThreadTimerDelete4AM.Change(-1, 0);
            button1.Enabled = true;
            button2.Enabled = true;
            btnSatrt.Enabled = true;
            btnStop.Enabled = false;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;
            await Task.Run(() => DeleteBorrowFolder());
            ((Button)sender).Enabled = true;
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;
            await Task.Run(() => DeleteDeleteMark());
            ((Button)sender).Enabled = true;
        }

        void DeleteBorrowFolder()
        {
            try
            {

                var newDate = DateTime.Now.AddDays(-7);

                string dirPath = @"\\10.13.200.15\ShareFolder\Borrow";

                //string dirPath = @"E:\2000";


                List<string> dirs = new List<string>(Directory.EnumerateDirectories(dirPath));

                foreach (var dir in dirs)
                {

                    List<string> dirsSon = new List<string>(Directory.EnumerateDirectories(dirPath + '\\' + dir.Substring(dir.LastIndexOf("\\") + 1)));

                    foreach (var item in dirsSon)
                    {
                        var ymd = item.Substring(item.LastIndexOf("\\") + 1);
                        var y = int.Parse(ymd.Substring(0, 4));
                        var m = int.Parse(ymd.Substring(4, 2));
                        var d = int.Parse(ymd.Substring(6, 2));

                        if (newDate.CompareTo(new DateTime(y, m, d)) > 0)
                        {

                            if (Directory.Exists(item))
                            {
                                Directory.Delete(item, true);
                                Print(this.textBox1, item);
                            }
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        void DeleteDeleteMark()
        {
            var deadline = DateTime.Now.AddDays(-7);
            var news = new List<NewsRaw>();

            var files = new List<FileInfo>();

            List<VideoMeta> results;

            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                results = DB.VideoMeta
                            .Where(x => x.DeleteMark == true && x.AlreadyDelete == false)
                            .Where(x => x.CreateDate < deadline)
                            .Select(x => x)
                            .ToList();


                foreach (var item in results)
                {


                    //news.Add(DB.NewsRaw.Find(item.NewsID));

                    if (string.IsNullOrEmpty(item.ArchiveName))
                    {
                        Print(this.textBox2, "ERROR : " + item.VideoID);
                        continue;
                    }

                    var task1 = Task.Factory.StartNew(() =>
                    {
                        File.Delete(Path.Combine(item.ArchivePath, item.ArchiveName));
                    });

                    var task2 = Task.Factory.StartNew(() =>
                    {
                        File.Delete(Path.Combine(item.MP4Path, item.MP4Name));
                    });

                    var task3 = Task.Factory.StartNew(() =>
                    {
                        File.Delete(Path.Combine(NewsMp4IISPath, item.MP4Name));
                    });

                    Task.WaitAll(task1, task2, task3);

                    //var file = new FileInfo(Path.Combine(item.ArchivePath, item.ArchiveName));

                    //files.Add(file);

                    //Print(this.textBox1, file.Name + " -- " + file.Length);

                    Print(this.textBox1, $"{item.VideoID} 完成!!");


                    item.AlreadyDelete = true;
                    DB.SaveChanges();
                }
            }

            //var size = files.Sum(x => x.Length) / 1024 / 1024 / 1024 / 1024;


            //Print(this.textBox1, size.ToString() + "T");


            Print(this.textBox1, "全部完成!!");
            //DB.VideoMeta.RemoveRange(results);
            //DB.NewsRaw.RemoveRange(news);

            //DB.SaveChanges();


        }
        
        void Print(TextBox tb, string text)
        {
            //判斷這個TextBox的物件是否在同一個執行緒上
            if (tb.InvokeRequired)
            {
                //當InvokeRequired為true時，表示在不同的執行緒上，所以進行委派的動作!!
                PrintHandler ph = new PrintHandler(Print);
                tb.Invoke(ph, tb, text);
            }
            else
            {
                lock (_syncRoot)
                {
                    //表示在同一個執行緒上了，所以可以正常的呼叫到這個TextBox物件
                    tb.Text += text + Environment.NewLine;
                    tb.SelectionStart = tb.Text.Length;
                    tb.ScrollToCaret();
                    
                }

            }
        }

    }
}
