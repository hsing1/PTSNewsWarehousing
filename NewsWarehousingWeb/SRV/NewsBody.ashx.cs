﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NewsWarehousingWeb.Models;

namespace NewsWarehousingWeb.SRV
{
    public class NewsBody : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var message = "";

            int id = 0;

            if (context.Request.Form["id"] == null)
            {
                message = "參數不正確!";
            }
            else
            {
                int.TryParse(context.Request.Form["id"], out id);

                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    var body = DB.NewsMeta.Where(x => x.NewsID == id).Select(x => x.Body).FirstOrDefault();

                    if (body != null)
                    {
                        message = body;
                    }
                    else
                    {
                        message = "找不到此則新聞!";
                    }
                }
            }
            

            context.Response.ContentType = "text/plain";
            context.Response.Write(message);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}