//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace NewsWarehousingApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class UploadTemp
    {
        public int UploadID { get; set; }
        public string VideoName { get; set; }
        public Nullable<long> VideoSize { get; set; }
        public string VideoTime { get; set; }
        public string ArchiveName { get; set; }
        public string MediaInfo { get; set; }
        public int FileStatus { get; set; }
        public int ConverterStatus { get; set; }
        public System.DateTime CreateDate { get; set; }
        public bool ConfirmMark { get; set; }
        public string Location { get; set; }
        public string ConfirmUser { get; set; }
        public Nullable<System.DateTime> ConfirmDate { get; set; }
        public string ConvertFolder { get; set; }
        public string JobGuid { get; set; }
        public string JobName { get; set; }
        public string Source { get; set; }
        public string DestinationName { get; set; }
        public string Errormessage { get; set; }
    }
}
