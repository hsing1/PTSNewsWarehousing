﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("drive")]
    public class Drive
    {
        public string number { get; set; }		// drive number
        public string cartridgeId { get; set; }
        public string mountPoint { get; set; }
        public string useFlag { get; set; }
        public string status { get; set; }
        public string errorMessage { get; set; }

        public Drive() { }

        public Drive(string number, string status, string cartridgeId, string useFlag, string mountPoint, string message)
        {
            this.number = number;
            this.status = status;
            this.cartridgeId = cartridgeId;
            this.useFlag = useFlag;
            this.mountPoint = mountPoint;
            this.errorMessage = message;
        }

    }
}
