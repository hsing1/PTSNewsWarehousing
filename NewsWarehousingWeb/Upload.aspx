﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="NewsWarehousingWeb.Upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="tblBase" style="width: 1200px; margin: 0 auto;">
        <tr>
            <td class="tdHeader-Green">
                <asp:Label ID="lblTitle" runat="server" Text="手動上傳" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdHeader-Blue" style="height:50px;text-align:center">
                <div style="font-size:x-large">請將檔案複製至 \\10.13.200.15\Upload</div>
                <div style="color: red; margin-top:10px;">影片長度需在15分鐘內，檔名不可以有空格，副檔名為.mxf</div>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tdHeader-Blue" style="width:10%; text-align: left">
                            <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                        </td>
                        <td class="tdHeader-Blue" style="width:10%; text-align: left">
                            <asp:Button ID="btnConfirm" runat="server" Text="確認上傳" OnClientClick="return GetValue();" OnClick="btnConfirm_Click" />
                            <asp:HiddenField ID="hfValues" runat="server" />
                        </td>
                        <td class="tdHeader-Blue" style="width:10%; text-align: left">
                            <asp:CheckBox ID="cbConfirm" runat="server" Text="僅顯示已確認" />
                        </td>
                        <td class="tdHeader-Blue" style="text-align: left">
                            <asp:Literal ID="lalBiggerMsg" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tdValue-Blue">
                <asp:GridView ID="gvResults" runat="server"
                    BackColor="White" BorderColor="#999999" BorderStyle="None"
                    BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%" ShowHeaderWhenEmpty="True"
                    AutoGenerateColumns="False" OnRowDataBound="gvResults_RowDataBound" ValidateRequestMode="Disabled" ViewStateMode="Disabled">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                        <asp:TemplateField HeaderText="匯入">
                            <ItemTemplate>
                                <asp:Literal ID="lalCheck" runat="server"></asp:Literal>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="系統編號" DataField="UploadID">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="影片名稱" DataField="VideoName">
                            <ItemStyle Width="250px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="入庫位置" DataField="LocationName">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="影片大小" DataField="VideoSize">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="80px" HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="轉檔狀態" DataField="ConverterTitle">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="影片長度" DataField="VideoTime">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="建檔時間" DataField="CreateDate" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="確認人員" DataField="ConfirmUser" >
                            <HeaderStyle Width="60px" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="確認時間" DataField="ConfirmDate" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script>

        var limit = Number($('#limitnum').data('limit_num'));
        var selectMaxNum = 0;

        function countMax(elm) {

            console.log(limit);

            var type = Number($(elm).data('biggetype'));

            if ($(elm).prop("checked")) {
                if (type > 15) selectMaxNum ++;
            } else {
                if (type > 15) selectMaxNum --;
            }

            console.log(type + ',' + selectMaxNum);

            if (limit - selectMaxNum === 0) {
                $("input[id^='cbConfirm_']").each(function () {

                    if ($(this).data('biggetype') > 15) {
                        if (!$(this).prop("checked")) {
                            $(this).prop("disabled", "disabled");
                        }
                    }

                });
            } else {
                $("input[id^='cbConfirm_']").each(function () {

                    if ($(this).data('biggetype') > 15) {
                        if (!$(this).prop("checked")) {
                            $(this).prop("disabled", "");
                        }
                    }

                });
            }

           
        }

        function GetValue() {
            var mark = false;
            var confirmItems = [];

            $("input[id^='cbConfirm_']").each(function () {
                if ($(this).prop("checked")) {

                    var id = $(this).prop("value");
                    var type = $(this).data("biggetype");


                    if ($("#Location_" + id).find(":selected").val() === '') {
                        mark = true;
                        return;
                    }

                    var item = {};

                    item.Id = id;

                    item.Location = $("#Location_" + item.Id).find(":selected").val();

                    item.BiggerType = type;

                    confirmItems.push(item);
                }
            });

            if (mark) {
                alert('選擇入庫位置!');
                return false;
            } else if (confirmItems.length === 0) {
                alert('請最少選擇一個項目!');
                return false;
            } else {
                $('#<% =hfValues.ClientID %>').val(JSON.stringify(confirmItems));
                    return true;
                }
        }

    </script>
</asp:Content>
