﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("systems")]
    public class SystemList
    {
        public string total { get; set; }
        [System.Xml.Serialization.XmlElement("system")]
        public List<System2> systems;

        public SystemList() { }
    }
}
