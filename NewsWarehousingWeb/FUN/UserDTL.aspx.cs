﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.DBUtility;
using PTS.Modules.ExtMethods;
using PTS.Modules.Utilities;

namespace NewsWarehousingWeb.FUN
{
    public partial class UserDTL : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            txtReturnValue.Text = "";

            if (Session["Type"] == null)
                Response.Redirect("~/FUN/User.aspx");


            if (Session["Type"].ToString() == "Modify")
            {
                lblTitle.Text = "修改使用者權限";

                if (Session["EmpID"] == null)
                    Response.Redirect("~/FUN/User.aspx");

                txtEmpID.Text = Session["EmpID"].ToString();

                btnNew.Enabled = false;
                btnOption.Visible = false;
                btnOption.ImageUrl = "~/App_Images/nonoptions.png";

                SetPageData();

                Session["LoginID"] = null;
                Session["Type"] = null;
                
            }
            else
            {
                lblTitle.Text = "新增使用者權限";

                btnModify.Enabled = false;
                btnOption.Visible = true;
            }
        }

        void SetPageData()
        {

            var strSQL = @"SELECT EmpID
                              ,EmpName
                              ,DomainID
                              ,UnitFullName
                              ,CASE IsEditor WHEN 1 THEN '1' ELSE '0' END IsEditor
							  ,CASE IsApprover WHEN 1 THEN '1' ELSE '0' END IsApprover
                              ,CASE IsPreArchive WHEN 1 THEN '1' ELSE '0' END IsPreArchive
							  ,CASE IsAdmin WHEN 1 THEN '1' ELSE '0' END IsAdmin
                          FROM Users
                         WHERE 1=1 
                           AND EmpID = @EmpID ";

            var objDBControl = new DBOperate();

            var dic = objDBControl.DBGetTable(strSQL, new Dictionary<string, string> { { "EmpID", txtEmpID.Text } }).ExTableToDictionary();

            FormUtility.SetFormData(this.Master.FindControl("ContentPlaceHolder1"), ref dic);
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            RunInsert();
        }

        protected void btnModify_Click(object sender, EventArgs e)
        {
            RunModify();
        }

        void RunInsert()
        {
            var dic = FormUtility.GetFormData(this.Master.FindControl("ContentPlaceHolder1"), "Insert");

            var objDBControl = new DBOperate();

            objDBControl.DBInsert(ref dic, "Users");

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "msg", "alert('新增成功!');location.href='http://" + Request.Url.Authority + ResolveUrl("~/FUN/User.aspx") + "';", true);
        }

        void RunModify()
        {
            var dic = FormUtility.GetFormData(this.Master.FindControl("ContentPlaceHolder1"), "Modify");

            var objDBControl = new DBOperate();

            objDBControl.DBUpdate(ref dic, "Users", "EmpID");

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "msg", "alert('修改成功!');location.href='http://" + Request.Url.Authority + ResolveUrl("~/FUN/User.aspx") + "';", true);
        }

        protected void txtReturnValue_TextChanged(object sender, EventArgs e)
        {
            var strSQL = @"SELECT EmpID
                              ,CName EmpName
                              ,NTAccount DomainID
                              ,UnitFullName
                          FROM vwEmpInfo
                         WHERE 1=1 
                           AND NTAccount = @NTAccount ";

            var objDBControl = new DBOperate("PTSHR");

            var dic = objDBControl.DBGetTable(strSQL, new Dictionary<string, string> { { "NTAccount", ((TextBox)sender).Text } }).ExTableToDictionary();

            FormUtility.SetFormData(this.Master.FindControl("ContentPlaceHolder1"), ref dic);
        }

    }
}