﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.DBUtility;
using PTS.Modules.Utilities;
using NewsWarehousingWeb.Models;
using Newtonsoft.Json;

namespace NewsWarehousingWeb
{
    public partial class PreArchive : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            if (IsPostBack) return;

            ControlInitial.Build(this.Master.FindControl("ContentPlaceHolder1"));

            base.OnInit(e); 
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SetPageData();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {

            try
            {
                var type = this.ddlNewsType.SelectedValue;

                var down = new NewsHelper();

                var fullName = down.GetNewsFile(type, Guid.NewGuid().ToString("N") + ".txt");

                var data = down.NewsResolve(fullName, type);

                if (data == null) return;

                var db = new DBOperate();

                if (data.Count > 0)
                {
                    db.DBMultiInsert(ref data, "PreArchive");
                }

                SetPageData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "msg", "alert('發生錯誤!請通知資訊人員!');", true);

            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            var items = JsonConvert.DeserializeObject<TransferItems>(hfValues.Value);

            List<Dictionary<string, string>> data = new List<Dictionary<string, string>>();

            var objEmp = new PTS.Modules.Employee.EmployeeInfo();

            var db = new DBOperate();

            if (items.approveItems.Count > 0)
            {
                var m = new List<Dictionary<string, string>>();

                foreach (var item in items.approveItems)
                {
                    var dic = new Dictionary<string, string>();

                    dic.Add("PreArchiveID", item.ToString());
                    dic.Add("ArchiveStatus", "1");
                    dic.Add("ArchiveConfirm", "true");
                    dic.Add("ConfirmUser", objEmp.EmployeeID);
                    dic.Add("ConfirmDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    m.Add(dic);
                }

                db.DBMultiUpdate(ref m, "PreArchive", "PreArchiveID");
            }

            if (items.deleteItems.Count > 0)
            {
                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    var ids = items.deleteItems.ToArray();
                    var resultsPre = DB.PreArchive.Where(x => ids.Contains(x.PreArchiveID)).Select(x => x).ToList();
                    var resultsMeta = DB.VideoMeta.Where(x => ids.Contains(x.PreArchiveID.Value)).Select(x => x).ToList();


                    foreach (var item in resultsPre)
                    {
                        var m = resultsMeta.Where(x => x.PreArchiveID == item.PreArchiveID).Select(x => x).FirstOrDefault();

                        if (m != null)
                        {
                            if (m.FileStstus >= 1) continue;
                            
                            m.DeleteMark = true;
                        }

                        DB.PreArchive.Remove(item);

                    }

                    DB.SaveChanges();

                }


            }

            SetPageData();
        }

        void SetPageData()
        {
            var sql = @"SELECT TOP 200 PreArchiveID
                              , sr1.ItemName NewsType
                              , Slogan
                              , Storyid
                              --, NewsRaw
                              , MXFName
                              , MP4Name
                              , ArchiveConfirm
                              , sr2.ItemName ArchiveStatusName
                              , ArchiveStatus
                              , CreateDate
                              , ConfirmUser
                              , ConfirmDate
                          FROM PreArchive 
                               JOIN SysReference sr1
                                    ON NewsType = sr1.ItemValue
                                   AND sr1.ItemType = '100'
                               JOIN SysReference sr2
                                    ON ArchiveStatus = CAST(sr2.ItemValue AS int)
                                   AND sr2.ItemType = '310'
                         WHERE 1 = 1
                           AND NewsType = @NewsType ";

            var dic = new Dictionary<string, string>();

            dic.Add("NewsType", this.ddlNewsType.SelectedValue);
            
            if (cbNonConfirm.Checked)
            {
                sql += "AND ArchiveConfirm = @ArchiveConfirm";
                dic.Add("ArchiveConfirm", "false");
            }

            sql += " ORDER BY PreArchiveID DESC";

            var db = new DBOperate();

            var dt = db.DBGetTable(sql, dic);

            gvResults.DataSource = dt;
            gvResults.DataBind();

        }

        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.DataRow)
            {
                var c = e.Row.FindControl("lalCheck") as Literal;

                if (c != null)
                {
                    var confirm = (bool)DataBinder.Eval(e.Row.DataItem, "ArchiveConfirm");
                    var id = DataBinder.Eval(e.Row.DataItem, "PreArchiveID").ToString();
                    var status = (int)DataBinder.Eval(e.Row.DataItem, "ArchiveStatus");
                    
                    var html = "";

                    //if(b)
                    //{
                    //    status = "disabled=\"disabled\" checked ";
                    //}

                    //c.Text = $"<input name=\"cbTransfer_{id}\" {status} id=\"cbTransfer_{id}\" type=\"checkbox\" value=\"{id}\"><label for=\"cbTransfer_{id}\">確認</label>";

                    if (confirm)
                    {
                        if (status > 3)
                        {
                            html = "已確認";
                        }
                        else
                        {
                            html = "<table style=\"width: 100%\"><tr><td style=\"width: 50%; text-align:left;\">";
                            html += $"已確認";
                            html += "</td><td style=\"width: 50%; text-align:right;\">";
                            html += $"<label for=\"cbDelete_{id}\">刪除</label><input type=\"checkbox\" id=\"cbDelete_{id}\" value=\"{id}\" onchange=\"Change2({id});\" />";
                            html += "</td></tr></table>";
                        }
                    }
                    else
                    {
                        html = "<table style=\"width: 100%\"><tr><td style=\"width: 50%; text-align:left;\">";
                        html += $"<input name=\"cbTransfer_{id}\" id=\"cbTransfer_{id}\" type=\"checkbox\" value=\"{id}\" onchange=\"Change1({id});\" /><label for=\"cbTransfer_{id}\">確認</label>";
                        html += "</td><td style=\"width: 50%; text-align:right;\">";
                        html += $"<label for=\"cbDelete_{id}\">刪除</label><input type=\"checkbox\" id=\"cbDelete_{id}\" value=\"{id}\" onchange=\"Change2({id});\" />";
                        html += "</td></tr></table>";
                    }
                    c.Text = html;
                    
                }
            }
        }
    }
}