﻿namespace NewsChangeLocation
{
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnMoveHAKKAFile = new System.Windows.Forms.Button();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.btnMoveBig = new System.Windows.Forms.Button();
            this.btnSatrt = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRunRightnow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMoveHAKKAFile
            // 
            this.btnMoveHAKKAFile.Enabled = false;
            this.btnMoveHAKKAFile.Location = new System.Drawing.Point(587, 55);
            this.btnMoveHAKKAFile.Name = "btnMoveHAKKAFile";
            this.btnMoveHAKKAFile.Size = new System.Drawing.Size(100, 23);
            this.btnMoveHAKKAFile.TabIndex = 0;
            this.btnMoveHAKKAFile.Text = "MoveHAKKAFile";
            this.btnMoveHAKKAFile.UseVisualStyleBackColor = true;
            this.btnMoveHAKKAFile.Visible = false;
            this.btnMoveHAKKAFile.Click += new System.EventHandler(this.btnMoveHAKKAFile_Click);
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(12, 99);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMessage.Size = new System.Drawing.Size(675, 257);
            this.txtMessage.TabIndex = 1;
            // 
            // btnMoveBig
            // 
            this.btnMoveBig.Location = new System.Drawing.Point(587, 26);
            this.btnMoveBig.Name = "btnMoveBig";
            this.btnMoveBig.Size = new System.Drawing.Size(100, 23);
            this.btnMoveBig.TabIndex = 2;
            this.btnMoveBig.Text = "MoveBigFile";
            this.btnMoveBig.UseVisualStyleBackColor = true;
            this.btnMoveBig.Click += new System.EventHandler(this.btnMoveBig_Click);
            // 
            // btnSatrt
            // 
            this.btnSatrt.Location = new System.Drawing.Point(407, 26);
            this.btnSatrt.Name = "btnSatrt";
            this.btnSatrt.Size = new System.Drawing.Size(75, 23);
            this.btnSatrt.TabIndex = 3;
            this.btnSatrt.Text = "Start";
            this.btnSatrt.UseVisualStyleBackColor = true;
            this.btnSatrt.Click += new System.EventHandler(this.btnSatrt_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(407, 55);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnRunRightnow
            // 
            this.btnRunRightnow.Location = new System.Drawing.Point(488, 26);
            this.btnRunRightnow.Name = "btnRunRightnow";
            this.btnRunRightnow.Size = new System.Drawing.Size(75, 23);
            this.btnRunRightnow.TabIndex = 5;
            this.btnRunRightnow.Text = "立即執行";
            this.btnRunRightnow.UseVisualStyleBackColor = true;
            this.btnRunRightnow.Click += new System.EventHandler(this.btnRunRightnow_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 414);
            this.Controls.Add(this.btnRunRightnow);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnSatrt);
            this.Controls.Add(this.btnMoveBig);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.btnMoveHAKKAFile);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "新聞檔名序列化";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMoveHAKKAFile;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Button btnMoveBig;
        private System.Windows.Forms.Button btnSatrt;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnRunRightnow;
    }
}

