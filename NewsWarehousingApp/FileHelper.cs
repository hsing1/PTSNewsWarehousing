﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NewsWarehousingApp
{
    internal class FileHelper
    {
        public void GetMXF(string Source, string Target)
        {

            try
            {
                FileStream outputStream = new FileStream(Target, FileMode.Create);
                FtpWebRequest ftp;
                ftp = (FtpWebRequest)FtpWebRequest.Create(new Uri(Source));
                ftp.UseBinary = true;
                ftp.Method = WebRequestMethods.Ftp.DownloadFile;

                ftp.Credentials = new NetworkCredential("mxfmovie", "mxfmovie");
                FtpWebResponse response = (FtpWebResponse)ftp.GetResponse();
                System.IO.Stream ftpStream = response.GetResponseStream();

                long cl = response.ContentLength;
                int bufferSize = 2 * 1024;
                int readCount;
                byte[] buffer = new byte[bufferSize];
                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0) 
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }
                
                ftpStream.Close();
                outputStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                Utility.SaveException("GetMXF", Source, ex);
                throw new Exception("GetMXF Error!");
            }

                        

        }

        public void GetMP4(string Source, string TargetPath, string TargetName)
        {

            try
            {
                File.Copy(Source, Path.Combine(TargetPath, TargetName), true);
                File.Copy(Source, Path.Combine(Properties.Settings.Default["IISMP4Folder"].ToString(), TargetName), true);
            }
            catch (Exception ex)
            {
                Utility.SaveException("GetMP4",Source, ex);
                throw new Exception("GetMP4 Error!");
            }

        }

    }
}