﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.DBUtility;

namespace NewsWarehousingWeb.FUN
{
    public partial class UserSchFrm : System.Web.UI.Page
    {

        DBOperate objDBControl = new DBOperate("PTSHR");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.hfObjectName1.Value = Request.QueryString["ObjectName1"];
                SetPageData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SetPageData();
        }

        protected void SetPageData()
        {

            var dic = new Dictionary<string, string>();
            string strSQL = "";

            strSQL = @"SELECT TOP 100 CName
                             ,DeptName
	                         ,DeptID
	                         ,NTAccount LoginID
                         FROM vwEmpInfo ";
            strSQL += " WHERE 1 = 1 AND OnDuty = 1 ";

            if (!string.IsNullOrEmpty(this.txtSearchField.Text.Trim()))
            {
                strSQL += "AND (CName Like @CName OR NTAccount Like @NTAccount) ";
                dic.Add("CName", '%' + this.txtSearchField.Text.Trim() + '%');
                dic.Add("NTAccount", '%' + this.txtSearchField.Text.Trim() + '%');
            }

            if (dic.Count == 0) dic = null;

            this.GridView1.DataSource = objDBControl.DBGetTable(strSQL, dic);
            this.GridView1.DataBind();

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var objLiteral = e.Row.FindControl("lalSelect") as Literal;

                if (objLiteral != null)
                {
                    objLiteral.Text = string.Format("<a href=\"javascript: ReturnValue('{0}');\">選擇</a>", DataBinder.Eval(e.Row.DataItem, "LoginID"));

                }
            }
        }

    }
}