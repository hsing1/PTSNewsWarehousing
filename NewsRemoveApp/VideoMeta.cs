//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace NewsRemoveApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class VideoMeta
    {
        public int VideoID { get; set; }
        public string NewsType { get; set; }
        public string OriginalFile { get; set; }
        public string OriginalLow { get; set; }
        public string ArchivePath { get; set; }
        public string ArchiveName { get; set; }
        public string MP4Path { get; set; }
        public string MP4Name { get; set; }
        public string CameraDate { get; set; }
        public string Class1 { get; set; }
        public string Class2 { get; set; }
        public string Class3 { get; set; }
        public string Channel { get; set; }
        public string Dept { get; set; }
        public string Nature { get; set; }
        public string Summary { get; set; }
        public string KeyWord { get; set; }
        public string Location { get; set; }
        public string Copyright { get; set; }
        public string Description { get; set; }
        public string MediaInfo { get; set; }
        public int NewsID { get; set; }
        public int FileStstus { get; set; }
        public string Timecode { get; set; }
        public string Duration { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public string ModifyUser { get; set; }
        public bool ApproveStatus { get; set; }
        public Nullable<System.DateTime> ApproveDate { get; set; }
        public string ApproveUser { get; set; }
        public Nullable<int> PreArchiveID { get; set; }
        public bool DeleteMark { get; set; }
        public bool AlreadyDelete { get; set; }
        public int ArchivePosition { get; set; }
        public bool SerialMark { get; set; }
        public string BDBarcode { get; set; }
        public int BDBurnStatus { get; set; }
    }
}
