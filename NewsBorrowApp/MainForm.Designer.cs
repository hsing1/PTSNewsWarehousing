﻿namespace NewsBorrowApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.BorrowTimer = new System.Windows.Forms.Timer(this.components);
            this.BorrowMainWorker = new System.ComponentModel.BackgroundWorker();
            this.BorrowSubWorker1 = new System.ComponentModel.BackgroundWorker();
            this.BorrowSubWorker2 = new System.ComponentModel.BackgroundWorker();
            this.BorrowSubWorker3 = new System.ComponentModel.BackgroundWorker();
            this.BorrowSubWorker4 = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblMain = new System.Windows.Forms.Label();
            this.lblSub1 = new System.Windows.Forms.Label();
            this.lblSub2 = new System.Windows.Forms.Label();
            this.lblSub3 = new System.Windows.Forms.Label();
            this.lblSub4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnStopTimer = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sslVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BorrowTimer
            // 
            this.BorrowTimer.Enabled = true;
            this.BorrowTimer.Interval = 60000;
            this.BorrowTimer.Tick += new System.EventHandler(this.BorrowTimer_Tick);
            // 
            // BorrowMainWorker
            // 
            this.BorrowMainWorker.WorkerReportsProgress = true;
            this.BorrowMainWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BorrowMainWorker_DoWork);
            this.BorrowMainWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BorrowMainWorker_ProgressChanged);
            this.BorrowMainWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BorrowMainWorker_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "BorrowTimer :";
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Location = new System.Drawing.Point(93, 37);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(33, 12);
            this.lblTimer.TabIndex = 1;
            this.lblTimer.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "BorrowMainWorker :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(382, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "BorrowMainWorker1 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(382, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "BorrowMainWorker2 :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(382, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "BorrowMainWorker3 :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(382, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "BorrowMainWorker4 :";
            // 
            // lblMain
            // 
            this.lblMain.AutoSize = true;
            this.lblMain.Location = new System.Drawing.Point(287, 37);
            this.lblMain.Name = "lblMain";
            this.lblMain.Size = new System.Drawing.Size(33, 12);
            this.lblMain.TabIndex = 7;
            this.lblMain.Text = "label7";
            // 
            // lblSub1
            // 
            this.lblSub1.AutoSize = true;
            this.lblSub1.Location = new System.Drawing.Point(495, 37);
            this.lblSub1.Name = "lblSub1";
            this.lblSub1.Size = new System.Drawing.Size(33, 12);
            this.lblSub1.TabIndex = 8;
            this.lblSub1.Text = "label7";
            // 
            // lblSub2
            // 
            this.lblSub2.AutoSize = true;
            this.lblSub2.Location = new System.Drawing.Point(495, 64);
            this.lblSub2.Name = "lblSub2";
            this.lblSub2.Size = new System.Drawing.Size(33, 12);
            this.lblSub2.TabIndex = 9;
            this.lblSub2.Text = "label7";
            // 
            // lblSub3
            // 
            this.lblSub3.AutoSize = true;
            this.lblSub3.Location = new System.Drawing.Point(495, 91);
            this.lblSub3.Name = "lblSub3";
            this.lblSub3.Size = new System.Drawing.Size(33, 12);
            this.lblSub3.TabIndex = 10;
            this.lblSub3.Text = "label7";
            // 
            // lblSub4
            // 
            this.lblSub4.AutoSize = true;
            this.lblSub4.Location = new System.Drawing.Point(495, 118);
            this.lblSub4.Name = "lblSub4";
            this.lblSub4.Size = new System.Drawing.Size(33, 12);
            this.lblSub4.TabIndex = 11;
            this.lblSub4.Text = "label7";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(14, 168);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(418, 239);
            this.textBox1.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "Log";
            // 
            // btnStopTimer
            // 
            this.btnStopTimer.Location = new System.Drawing.Point(453, 168);
            this.btnStopTimer.Name = "btnStopTimer";
            this.btnStopTimer.Size = new System.Drawing.Size(107, 23);
            this.btnStopTimer.TabIndex = 14;
            this.btnStopTimer.Text = "Start/Stop Timer";
            this.btnStopTimer.UseVisualStyleBackColor = true;
            this.btnStopTimer.Click += new System.EventHandler(this.btnStopTimer_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sslVersion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 425);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(584, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sslVersion
            // 
            this.sslVersion.Name = "sslVersion";
            this.sslVersion.Size = new System.Drawing.Size(128, 17);
            this.sslVersion.Text = "toolStripStatusLabel1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 447);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnStopTimer);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblSub4);
            this.Controls.Add(this.lblSub3);
            this.Controls.Add(this.lblSub2);
            this.Controls.Add(this.lblSub1);
            this.Controls.Add(this.lblMain);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "新聞片庫-影片調用排程";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer BorrowTimer;
        private System.ComponentModel.BackgroundWorker BorrowMainWorker;
        private System.ComponentModel.BackgroundWorker BorrowSubWorker1;
        private System.ComponentModel.BackgroundWorker BorrowSubWorker2;
        private System.ComponentModel.BackgroundWorker BorrowSubWorker3;
        private System.ComponentModel.BackgroundWorker BorrowSubWorker4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblMain;
        private System.Windows.Forms.Label lblSub1;
        private System.Windows.Forms.Label lblSub2;
        private System.Windows.Forms.Label lblSub3;
        private System.Windows.Forms.Label lblSub4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnStopTimer;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel sslVersion;
    }
}