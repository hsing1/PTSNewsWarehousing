﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("drives")]
    public class DriveList
    {
        public string totalCount { get; set; }

        [System.Xml.Serialization.XmlElement("drive")]
        public List<Drive> drives { get; set; }
    }
}
