﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsWarehousingApp
{
    internal sealed class FTPServer
    {

        private FTPServer()
        {

        }

        private static object _syncRoot = new object();
        private static List<FtpServerIP> _ftpserverip;

        public static List<FtpServerIP> FtpServerIP
        {
            get
            {
                if (_ftpserverip == null)
                {
                    lock (_syncRoot)
                    {
                        // 永保安康, 再確認一次 null
                        if (_ftpserverip == null)
                        {
                            GetFtpServerIP();
                        }

                    }
                }
                return _ftpserverip;
            }
        }

        private static void GetFtpServerIP()
        {
            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                _ftpserverip = DB.SysReference.Where(x => x.ItemType == "110").Select(x => new FtpServerIP { NewsType = x.ItemValue, IP = x.ItemName }).ToList();
            }
    
        }



    }

    internal class FtpServerIP
    {
        public string NewsType { get; set; }
        public string IP { get; set; }
    }
}
