﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

using PTS.Modules.DBUtility;
using PTS.Modules.ExtMethods;
using PTS.Modules.Utilities;
using PTS.Modules.Employee;
using System.Data;
using NewsWarehousingWeb.Models;

namespace NewsWarehousingWeb
{
    public partial class InStorage : System.Web.UI.Page
    {
        DBOperate db = new DBOperate();
        
        protected override void OnInit(EventArgs e)
        {
            if (IsPostBack) return;

            ControlInitial.Build(this.Panel1);

            ddlNewsType.DataSource = db.DBGetTable("SELECT ItemValue, ItemName FROM SysReference WHERE ItemType = '100' ");
            ddlNewsType.DataBind();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) {
                lblMsg.Text = "";
               
            }
            else
            {
                var objEmp = new EmployeeInfo();

                var role = JsonConvert.DeserializeObject<UserRule.Rule>(objEmp.Rule);

                if (role.IsApprover)
                {
                    Panel2.Visible = true;
                }
                else
                {
                    Panel2.Visible = false;
                }

                if (objEmp.DepartmentName.Contains("客家"))
                {
                    ddlNewsType.SelectedValue = "9003";
                    btnExcel.Visible = true;
                }
                else if (objEmp.DepartmentName.Contains("宏觀"))
                {
                    ddlNewsType.SelectedValue = "9004";
                }
                else
                {
                    ddlNewsType.SelectedValue = "9002";
                }

                hfPage.Value = "0";

                ViewState["VideoID"] = "DESC";
                ViewState["Slogan"] = "ASC";
                ViewState["CameraDate"] = "ASC";
                ViewState["DeptName"] = "ASC";
                ViewState["CreateDate"] = "ASC";
                ViewState["Approve"] = "DESC";
                ViewState["ApproveDate"] = "ASC";
                //CalendarB.Text = DateTime.Now.AddDays(-2).ToString("yyyyMMdd");
                //CalendarE.Text = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");

                GetData();
            }
        }

        void GetData(string field = "")
        {

            var dic = new Dictionary<string, string>();
            var sql = @"  SELECT Top 200 VideoMeta.VideoID VideoID,
		                         NewsMeta.Slogan Slogan,
		                         CameraDate,
		                         s4.ItemName DeptName,
		                         CreateDate,
                                 CASE WHEN ApproveStatus = 1 THEN '是' ELSE '否' END Approve,
                                 ApproveUser,
                                 ApproveDate,
                                 s1.ItemName FileStstus
                          FROM VideoMeta
                               JOIN NewsMeta on NewsMeta.NewsID = VideoMeta.NewsID
                               JOIN SysReference s1 on s1.ItemValue = VideoMeta.FileStstus and s1.ItemType ='340'
                               LEFT JOIN SysReference s4 on s4.ItemValue = Dept and s4.ItemType ='502'
                          WHERE 1=1 
                            AND FileStstus = 2 
                            AND DeleteMark = 0
                            AND VideoMeta.NewsType = @NewsType ";
                            

            dic.Add("NewsType", ddlNewsType.SelectedValue);

            if (QrySlogan.Text != "")
            {
                sql += " AND NewsMeta.Slogan like '%'+@Slogan+'%' ";
                dic.Add("Slogan", QrySlogan.Text);
            }
            if (this.CalendarB.Text != "")
            {
                if (this.CalendarE.Text != "")
                {
                    sql += "  AND NewsMeta.NewsDate between @CalendarB and @CalendarE ";
                    dic.Add("CalendarB", CalendarB.Text);
                    dic.Add("CalendarE", CalendarE.Text);
                }
                else
                {
                    sql += "  AND NewsMeta.NewsDate >= @CalendarB ";
                    dic.Add("CalendarB", CalendarB.Text);
                }
            }

            if(cbNonApproveStatus.Checked)
            {
                sql += " AND VideoMeta.ApproveStatus = @ApproveStatus ";
                dic.Add("ApproveStatus", "0");
            }

            if (!string.IsNullOrEmpty(field))
            {
                sql += " ORDER BY ";

                switch (field)
                {
                    case "VideoID":
                        sql += "VideoMeta.VideoID " + ViewState["VideoID"].ToString();
                        break;
                    case "Slogan":
                        sql += "NewsMeta.Slogan " + ViewState["Slogan"].ToString();
                        break;
                    case "CameraDate":
                        sql += "CameraDate " + ViewState["CameraDate"].ToString();
                        break;
                    case "DeptName":
                        sql += "VideoMeta.Dept " + ViewState["DeptName"].ToString();
                        break;
                    case "Approve":
                        sql += "ApproveStatus " + ViewState["Approve"].ToString(); 
                        break;
                    case "CreateDate":
                        sql += "CreateDate " + ViewState["CreateDate"].ToString();
                        break;
                    case "ApproveDate":
                        sql += "ApproveDate " + ViewState["ApproveDate"].ToString();
                        break;
                    default:
                        break;
                }
                
            }
            else
            {
                sql += " ORDER BY ";


                sql += "VideoMeta.VideoID " + ViewState["VideoID"].ToString();
            }

            var data = db.DBGetTable(sql, dic);
            GridView1.DataSource = data;
            GridView1.DataBind();
        }

        void GetDetailData(string VideoID)
        {

            var sql = @"  SELECT NewsMeta.NewsID,
                                 NewsMeta.Slogan Slogan,
                                 substring(CameraDate,1,4) YY,
		                         substring(CameraDate,6,2) MM,
		                         substring(CameraDate,9,2) DD,
		                         Class1,
		                         s1.ItemName className1,
		                         Class2,
		                         s2.ItemName className2,
		                         Class3,
		                         s3.ItemName className3,
		                         Channel,
		                         Dept,
		                         Nature,
		                         Summary,
		                         KeyWord,
		                         Location,
		                         v1.Tracks1 Tracks1,
		                         v1.Tracks1_Lang Tracks1_Lang,
		                         v1.Tracks2 Tracks2,
		                         v1.Tracks2_Lang Tracks2_Lang,
		                         v1.Tracks3 Tracks3,
		                         v1.Tracks3_Lang Tracks3_Lang,
		                         v1.Tracks4 Tracks4,
		                         v1.Tracks4_Lang Tracks4_Lang,
		                         Copyright,
		                         Description,
		                         Body,
                                 VideoMeta.Mp4Name,
                                 CASE WHEN ApproveStatus = 1 THEN '1' ELSE '0' END ApproveStatus
                          FROM VideoMeta
                          JOIN NewsMeta on NewsMeta.NewsID = VideoMeta.NewsID
                          left JOIN SysReference s1 on s1.ItemValue = Class1 and s1.ItemType ='500'
                          left JOIN SysReference s2 on s2.ItemValue = Class2 and s2.ItemType ='500'
                          left JOIN SysReference s3 on s3.ItemValue = Class3 and s3.ItemType ='500'
                          left JOIN SysReference s4 on s4.ItemValue = Dept and s4.ItemType ='502'
                          left JOIN VideoTracks v1 on v1.VideoID = VideoMeta.VideoID
                          WHERE VideoMeta.VideoID = @VideoID";
            var data = db.DBGetTable(sql, new Dictionary<string, string> { { "VideoID", VideoID } }).ExTableToDictionary();
            FormUtility.SetFormData(this.Master.FindControl("ContentPlaceHolder1"), ref data);
            if (data["Copyright"].Trim() != "")
            {
                Copyright.SelectedValue = data["Copyright"];
            }
            else
            {
                Copyright.SelectedIndex = -1;
            }

            hfNewsID.Value = data["NewsID"];

            rblApprove.SelectedValue = data["ApproveStatus"];

            var v = "http://newsmam.pts.org.tw/mp4/" + data["Mp4Name"].ToString();
            lallowVideo.Text = $"<video id=\"lowVideo\" autoplay controls  width=\"431\" height=\"307\" ><source src=\"{v}\"type =\"video/mp4\"></video>";
            QueryReport();


        }
        void updateData()
        {
            var emp = new EmployeeInfo();

            var dicVideoMeta = new Dictionary<string, string>();
            var dicNewsMeta = new Dictionary<string, string>();
            var dicVideoReporter = new Dictionary<string, string>();
            var dicVideoTracks = new Dictionary<string, string>();

            dicNewsMeta.Add("NewsID", hfNewsID.Value);
            dicNewsMeta.Add("Slogan", Slogan.Text);
            dicNewsMeta.Add("Body", Body.Text);

            var date = "";

            if (!(string.IsNullOrEmpty(YY.Text.Trim()) || string.IsNullOrEmpty(MM.Text.Trim()) || string.IsNullOrEmpty(DD.Text.Trim())))
            {
                date = YY.Text.Trim() + "-" + MM.Text.Trim().PadLeft(2, '0') + "-" + DD.Text.Trim().PadLeft(2, '0');
            }

            dicVideoMeta.Add("CameraDate", date);
            dicVideoMeta.Add("Class1", class1.Text);
            dicVideoMeta.Add("Class2", class2.Text);
            dicVideoMeta.Add("Class3", class3.Text);

            dicVideoMeta.Add("Channel", Channel.Text);
            dicVideoMeta.Add("Dept", Dept.Text);
            dicVideoMeta.Add("Nature", Nature.Text);
            dicVideoMeta.Add("Summary", Summary.Text);
            dicVideoMeta.Add("KeyWord", KeyWord.Text);
            dicVideoMeta.Add("Location", Location.Text);
            dicVideoMeta.Add("ModifyDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            dicVideoMeta.Add("ModifyUser", emp.EmployeeID);


            dicVideoTracks.Add("Tracks1", Tracks1.Text);
            dicVideoTracks.Add("Tracks2", Tracks2.Text);
            dicVideoTracks.Add("Tracks3", Tracks3.Text);
            dicVideoTracks.Add("Tracks4", Tracks4.Text);

            dicVideoTracks.Add("Tracks1_Lang", TracksLang1.Text);
            dicVideoTracks.Add("Tracks2_Lang", TracksLang2.Text);
            dicVideoTracks.Add("Tracks3_Lang", TracksLang3.Text);
            dicVideoTracks.Add("Tracks4_Lang", TracksLang4.Text);

            dicVideoTracks.Add("VideoID", pkIndex.Value);
            
            dicVideoReporter.Add("ReporterType", Report.Text);
            dicVideoReporter.Add("ReporterName", ReportName.Text);

            dicVideoMeta.Add("Copyright", Copyright.SelectedValue);
            dicVideoMeta.Add("Description", Description.Text);
            dicVideoMeta.Add("VideoID", pkIndex.Value);
            
            var cnt = db.DBGetValue<int>(@"SELECT count(*) cnt
                                             FROM VideoTracks
                                            WHERE VideoID = @videoid"
                                        , new Dictionary<string, string> { { "videoid", pkIndex.Value } }
                                        , 0);
            try
            {
                db.DBUpdate(dicVideoMeta, "VideoMeta", "VideoID");
                db.DBUpdate(dicNewsMeta, "NewsMeta", "NewsID");

                if (cnt > 0)
                {
                    db.DBUpdate(dicVideoTracks, "VideoTracks", "VideoID");
                }
                else
                {
                    db.DBInsert(dicVideoTracks, "VideoTracks");
                }
                
                lallowVideo.Text = "";
                lblMsg.Text = "修改成功!";
            }
            catch (Exception ex)
            {
                lallowVideo.Text = "";
                lblMsg.Text = "修改失敗!";
            }


        }

        void InsertReport()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("VideoID", pkIndex.Value);
            dic.Add("ReporterType", Report.Text);
            dic.Add("ReporterName", ReportName.Text);
            db.DBInsert(dic, "VideoReporter");
            QueryReport();
        }

        void QueryReport()
        {
            //var sql2 = @"   SELECT sr.ItemName ReporterType,
            //                      ReporterName,
            //                      ReporterID 
            //                FROM VideoReporter
            //                join SysReference sr on sr.ItemValue= VideoReporter.ReporterType and sr.ItemType='506'
            //              WHERE VideoID = @VideoID
            //              ORDER BY ReporterID ";
            //GridView2.DataSource = db.DBGetTable(sql2, new Dictionary<string, string> { { "VideoID", VideoID } });

            //GridView2.DataBind();

            var sql = @"SELECT SUBSTRING(SysReference.ItemName, 1, 2) ReporterType,
                               ReporterName,
                               ReporterID 
                          FROM VideoReporter
                               JOIN SysReference
                                    ON ItemType = '506'
                                   AND ReporterType = ItemValue
                          WHERE VideoID = @VideoID
                          ORDER BY ReporterID desc";
            GridView2.DataSource = db.DBGetTable(sql, new Dictionary<string, string> { { "VideoID", pkIndex.Value } });
            GridView2.DataBind();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToString() == "Select")
            {
                int pk_index = Convert.ToInt32(e.CommandArgument);
                pkIndex.Value = pk_index.ToString();
                GetDetailData(pk_index.ToString());
            }
            else
            {

                lallowVideo.Text = "";
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hfPage.Value = "0";

            GetData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            updateData();
            GridView1.PageIndex = int.Parse(hfPage.Value);
            GetData();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            var emp = new EmployeeInfo();

            db.DBUpdate(new Dictionary<string, string> { { "VideoID", pkIndex.Value }, { "ApproveStatus", rblApprove.SelectedValue }, { "ApproveDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") }, { "ApproveUser", emp.EmployeeID } }
                       , "VideoMeta"
                       , "VideoID");

            GridView1.PageIndex = int.Parse(hfPage.Value);
            GetData();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            var SQL = @"SELECT VideoID 檔案編號
                              ,nm.Slogan 標題
	                          ,sr1.ItemName 性質
	                          ,u.DomainID + ' ' + u.EmpName 建檔者
                              ,ApproveDate 建檔日期
                          FROM VideoMeta
                               JOIN NewsMeta nm
	                                ON VideoMeta.NewsID = nm.NewsID
                               JOIN SysReference sr1
	                                ON sr1.ItemType = 503
		                           AND sr1.ItemValue = Nature
	                           JOIN Users u
	                                ON u.EmpID = ApproveUser
                         WHERE VideoMeta.NewsType = '9003'
                           AND ApproveStatus = 1 
	                       AND DeleteMark = 0 
                           AND CameraDate BETWEEN @Date1 AND @Date2 ";

            var dic = new Dictionary<string, string>();

            dic.Add("Date1", CalendarB.Text.Substring(0, 4) + '-' + CalendarB.Text.Substring(4, 2) + '-' + CalendarB.Text.Substring(6, 2));
            dic.Add("Date2", CalendarE.Text.Substring(0, 4) + '-' + CalendarE.Text.Substring(4, 2) + '-' + CalendarE.Text.Substring(6, 2));

            var dt = db.DBGetTable(SQL, dic);

            IWorkbook workbook = new HSSFWorkbook();

            ISheet sheet1 = workbook.CreateSheet("Sheet1");

            IRow headRow = sheet1.CreateRow(0);

            int i = 0;

            foreach (DataColumn dc in dt.Columns)
            {
                headRow.CreateCell(i).SetCellValue(dc.ColumnName);
                i++;
            }

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                IRow dataRow = sheet1.CreateRow(j + 1);

                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    sheet1.AutoSizeColumn(j);
                    dataRow.CreateCell(k).SetCellValue(dt.Rows[j][k].ToString());
                }
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=Data{0}.xls", DateTime.Now.ToString("yyyyMMdd")));
            Response.BinaryWrite(ms.ToArray());
            workbook = null;
            ms.Close();
            ms.Dispose();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var items = JsonConvert.DeserializeObject<List<int>>(hfDelete.Value);

            var db = new DBOperate();

            if (items.Count > 0)
            {
                var m = new List<Dictionary<string, string>>();

                foreach (var item in items)
                {
                    var dic = new Dictionary<string, string>();

                    dic.Add("VideoID", item.ToString());
                    dic.Add("DeleteMark", "true");
                    
                    m.Add(dic);
                }

                db.DBMultiUpdate(ref m, "VideoMeta", "VideoID");
            }

            GetData();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.Header)
            {
                List<ImageButton> liBtn = new List<ImageButton>(); ;

                for (int i = 0; i < e.Row.Controls.Count; i++)
                {
                    liBtn.AddRange(e.Row.Controls[i].Controls.OfType<ImageButton>().ToList());
                }

                foreach (var item in liBtn)
                {
                    if(ViewState[item.CommandArgument.ToString()].ToString() == "ASC")
                    {
                        item.ImageUrl = "~/App_Images/arrowup.png";
                    }
                    else
                    {
                        item.ImageUrl = "~/App_Images/arrowdown.png";
                    }
                }
                
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var c1 = e.Row.FindControl("lalCreateDate") as Literal;

                if(c1 != null)
                {
                    DateTime d1;
                    if (DateTime.TryParse(DataBinder.Eval(e.Row.DataItem, "CreateDate").ToString(), out d1))
                    {
                        var s1 = d1.ToString("yyyy-MM-dd,HH:mm:ss");
                        c1.Text = $"{s1.Split(',')[0]}<br />{s1.Split(',')[1]}";
                    }
                }
                var c2 = e.Row.FindControl("lalApproveDate") as Literal;

                if (c2 != null)
                {
                    DateTime d2;
                    if (DateTime.TryParse(DataBinder.Eval(e.Row.DataItem, "ApproveDate").ToString(), out d2))
                    {
                        var s2 = d2.ToString("yyyy-MM-dd,HH:mm:ss");
                        c2.Text = $"{s2.Split(',')[0]}<br />{s2.Split(',')[1]}";
                    }
                }

                var c3 = e.Row.FindControl("lalDelete") as Literal;

                if (c3 != null)
                {
                    var id = DataBinder.Eval(e.Row.DataItem, "VideoID").ToString();

                    c3.Text =$"<input name=\"cbDelete_{id}\" id=\"cbDelete_{id}\" type=\"checkbox\" value=\"{id}\" />";
                    
                }
            }
        }

        protected void GridView1_PageIndexChanged(object sender, EventArgs e)
        {

        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            hfPage.Value = e.NewPageIndex.ToString();
            lallowVideo.Text = "";
            GridView1.SelectedIndex = -1;
            GetData();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            InsertReport();
            Report.Text = "";
            ReportName.Text = "";
        }
        
        protected void GridView2_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView2.PageIndex = e.NewPageIndex;
            QueryReport();
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            db.DBDelete(new Dictionary<string, string> { { "ReporterID", e.CommandArgument.ToString() } }, "VideoReporter");
            QueryReport();
        }

        protected void Sort_Command(object sender, CommandEventArgs e)
        {
            if(ViewState[e.CommandArgument.ToString()].ToString() == "ASC")
            {
                ViewState[e.CommandArgument.ToString()] = "DESC";
            }
            else
            {
                ViewState[e.CommandArgument.ToString()] = "ASC";
            }

            GetData(e.CommandArgument.ToString());
        }


    }
}