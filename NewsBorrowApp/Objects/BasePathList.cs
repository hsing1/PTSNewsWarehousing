﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("basePaths")]
    public class BasePathList
    {
        public BasePathList()
        {
        }
        [System.Xml.Serialization.XmlElement("basePath")]
        public List<BasePath> basePaths { get; set; }

    }
}
