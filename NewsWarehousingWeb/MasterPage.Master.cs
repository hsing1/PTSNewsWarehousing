﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.Employee;
using Newtonsoft.Json;
using NewsWarehousingWeb.Models;

namespace NewsWarehousingWeb
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        EmployeeInfo objEmp = new EmployeeInfo();
        protected override void OnInit(EventArgs e)
        {
            BulidNAV(objEmp.Rule);

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            
            this.lblUserName.Text = objEmp.EmployeeName;
            
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            //Response.Write("<script language='javascript'>window.open('','_self','');window.close();</script>");
            //Response.End();
        }
        
        void BulidNAV(string json)
        {

            var role = JsonConvert.DeserializeObject<UserRule.Rule>(json);

            phNAV.Controls.Add(new LiteralControl("<table><tr><td style=\"width:100px; text-align:center;\">"));

            phNAV.Controls.Add(new HyperLink() { ID = "linkSearch", NavigateUrl= "~/Search.aspx", Text = "檢索調用" });

            phNAV.Controls.Add(new LiteralControl("</td><td style=\"width: 150px; text-align:center;\">"));

            phNAV.Controls.Add(new HyperLink() { ID = "linkBorrowList", NavigateUrl = "~/BorrowList.aspx", Text = "調用確認及結果" });

            phNAV.Controls.Add(new LiteralControl("</td><td style=\"width: 100px; text-align:center;\">"));

            phNAV.Controls.Add(new HyperLink() { ID = "linkUpload", NavigateUrl = "~/Upload.aspx", Text = "手動上傳" });

            phNAV.Controls.Add(new LiteralControl("</td>"));

            if (role.IsEditor || role.IsApprover)
            {
                phNAV.Controls.Add(new LiteralControl("<td style=\"width: 100px; text-align:center;\">"));

                phNAV.Controls.Add(new HyperLink() { ID = "linkInStorage", NavigateUrl = "~/InStorage.aspx", Text = "入庫審核" });

                phNAV.Controls.Add(new LiteralControl("</td>"));
            }

            if (role.IsPreArchive)
            {
                phNAV.Controls.Add(new LiteralControl("<td style=\"width: 100px; text-align:center;\">"));

                phNAV.Controls.Add(new HyperLink() { ID = "linkPreArchive", NavigateUrl = "~/PreArchive.aspx", Text = "每日入庫" });

                phNAV.Controls.Add(new LiteralControl("</td>"));
            }

            if (role.IsAdmin)
            {
                phNAV.Controls.Add(new LiteralControl("<td style=\"width: 120px; text-align:center;\">"));

                phNAV.Controls.Add(new HyperLink() { ID = "linkUser", NavigateUrl = "~/FUN/User.aspx", Text = "使用者權限" });

                phNAV.Controls.Add(new LiteralControl("</td>"));
            }
            
            phNAV.Controls.Add(new LiteralControl("</tr></table>"));

            
            
        }

        
    }
}