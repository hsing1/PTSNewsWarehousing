﻿using PTS.Modules.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NewsWarehousingWeb.Models;

namespace NewsWarehousingWeb
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (IsPostBack) return;

            if (Request["SID"] == null) return;

            try
            {
                var sid = Request["SID"];

                var objEmp = new LoginFromEIP(sid);

                var objRule = new UserRule();

                var rule = objRule.GetUserRule(objEmp.DomainID);


                Response.Redirect("Default.aspx", false);


            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx", false);
            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (this.txtLoginName.Text.Trim() == "")
            {
                lblMsg.Text = "請輸入帳號!";
                return;
            }

            if (this.txtLoginPassword.Text.Trim() == "")
            {
                lblMsg.Text = "請輸入密碼!";
                return;
            }

            try
            {
                var objEmp = new EmployeeLogin(this.txtLoginName.Text.Trim(), this.txtLoginPassword.Text.Trim());

                var objRule = new UserRule();

                var rule = objRule.GetUserRule(this.txtLoginName.Text.Trim());

                objEmp.HRToCookie(rule);

                Response.Redirect("Default.aspx");
                
            }
            catch
            {
                lblMsg.Text = "登入失敗!";
            }

        }

        protected void txtLoginName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}