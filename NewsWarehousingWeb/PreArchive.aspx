﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="PreArchive.aspx.cs" Inherits="NewsWarehousingWeb.PreArchive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="tblBase" style="width: 1050px; margin: 0 auto;">
        <tr>
            <td class="tdHeader-Green">
                <asp:Label ID="lblTitle" runat="server" Text="iNews文稿下載" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tdHeader-Blue" style="width: 20%">入庫位置
                        </td>
                        <td class="tdValue-Blue" style="width: 30%">
                            <asp:PTSDropDownList ID="ddlNewsType" runat="server" InitialFields="NewsType">
                            </asp:PTSDropDownList>
                        </td>
                        <td class="tdHeader-Blue" style="width: 20%">僅顯未確認
                        </td>
                        <td class="tdValue-Blue" style="width: 30%">
                            <asp:CheckBox ID="cbNonConfirm" runat="server" Checked="True" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="tdHeader-Blue" style="text-align:center">
                            <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />&nbsp;
                          <asp:Button ID="btnDownload" runat="server" Text="下載清單" OnClick="btnDownload_Click" />&nbsp;
                          <asp:Button ID="btnConfirm" runat="server" Text="確認" OnClientClick="return GetValue();" OnClick="btnConfirm_Click" />
                            <asp:HiddenField ID="hfValues" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tdValue-Blue">
                <asp:GridView ID="gvResults" runat="server"
                    BackColor="White" BorderColor="#999999" BorderStyle="None"
                    BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" OnRowDataBound="gvResults_RowDataBound">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                        <asp:TemplateField HeaderText="匯入">
                            <HeaderTemplate>
                                匯入<br />
                                <table style="width:100%">
                                    <tr>
                                        <td style="width:25%; text-align:left;">
                                            <input id="AllConfirm" type="checkbox" onclick="ConfirmAll(this);" />
                                        </td>
                                        <td style="width:50%; text-align: center;">
                                            全選
                                        </td>
                                        <td style="width:25%; text-align:right;">
                                            <input id="AllDelete" type="checkbox" onclick="DeleteAll(this);" />
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="lalCheck" runat="server"></asp:Literal>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="入庫位置" DataField="NewsType">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Slogan" DataField="Slogan">
                            <ItemStyle Width="200px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="MXFName" DataField="MXFName">
                            <ItemStyle Width="220px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="狀態" DataField="ArchiveStatusName">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="建檔時間" DataField="CreateDate" />
                        <asp:BoundField HeaderText="確認人員" DataField="ConfirmUser" />
                        <asp:BoundField HeaderText="確認時間" DataField="ConfirmDate">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script>
        $(document).ready(function () {
            $("#<% =btnDownload.ClientID %>").click(function () {
                $.blockUI({ message: '<h1><img src="http://newsmam.pts.org.tw/App_Images/Loding.gif" /> 檔案下載中, 約1~2分鐘, 請稍候...</h1>' });
            });
        });

        function ConfirmAll(obj) {
            if ($(obj).prop("checked")) {
                $("input[id^='cbTransfer_']").each(function () {
                    $(this).prop("checked", true);
                });

                $("input[id^='cbDelete_']").each(function () {
                    $(this).prop("checked", false);
                });

                $("#AllDelete").prop("checked", false);

            } else {
                $("input[id^='cbTransfer_']").each(function () {
                    $(this).prop("checked", false);
                });
                 
            }
        }

        function DeleteAll(obj) {
            if ($(obj).prop("checked")) {
                $("input[id^='cbDelete_']").each(function () {
                    $(this).prop("checked", true);
                });

                $("input[id^='cbTransfer_']").each(function () {
                    $(this).prop("checked", false);
                });

                $("#AllConfirm").prop("checked", false);

            } else {

                $("input[id^='cbDelete_']").each(function () {
                    $(this).prop("checked", false);
                });
            }
        }

        function Change1(id) {
            if ($('#cbTransfer_' + id).prop("checked")) {
                $('#cbDelete_' + id).prop("checked", false);
            }
        }

        function Change2(id) {
            if ($('#cbDelete_' + id).prop("checked")) {
                $('#cbTransfer_' + id).prop("checked", false);
            }
        }

        function GetValue() {
            var value = {
                approveItems: [],
                deleteItems: []
            };

            $("input[id^='cbTransfer_']").each(function () {
                if ($(this).prop("checked")) {
                    value.approveItems.push($(this).prop("value"));
                }
            });

            $("input[id^='cbDelete_']").each(function () {
                if ($(this).prop("checked")) {
                    value.deleteItems.push($(this).prop("value"));
                }
            });

            if (value.approveItems.length === 0 && value.deleteItems.length === 0) {
                alert('請最少選擇一個項目!');
                return false;
            } else {
                $('#<% =hfValues.ClientID %>').val(JSON.stringify(value));
                return true;
            }
        }
    </script>
</asp:Content>
