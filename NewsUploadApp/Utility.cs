﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace NewsUploadApp
{
    public class Utility
    {
        public static void SaveException(string Method, Exception ex)
        {
            SaveException(Method, "", ex);
        }

        public static void SaveException(string Method, string FullFileName, Exception ex)
        {
            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                DB.ExceptionLog.Add(new ExceptionLog() { MethodName = Method, ExceptionMessage = ex.ToString(), FullFileName = FullFileName, CreateDate = DateTime.Now });

                DB.SaveChanges();
            }
        }
    }
}
