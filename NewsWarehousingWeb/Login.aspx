﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NewsWarehousingWeb.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
    <meta name="author" content="Codrops" />
    <link rel="stylesheet" type="text/css" href="App_CSS/demo.css" />
    <link rel="stylesheet" type="text/css" href="App_CSS/style.css" />
    <link rel="stylesheet" type="text/css" href="App_CSS/animate-custom.css" />
    <style type="text/css">
        body, td, th {
            font-family: "微軟正黑體";
        }
    </style>
    <title></title>
</head>
<body>
    <div class="container">
        <br />
        <br />

        <header>
            <h1>公視「新聞片庫」<span>後台登入系統</span></h1>
        </header>
        <section>
            <div id="container_demo">
                <a class="hiddenanchor" id="toregister"></a>
                <a class="hiddenanchor" id="tologin"></a>
                <div id="wrapper">
                    <div id="login" class="animate form">
                        <form autocomplete="on" runat="server" id="main">
                            <h1>請輸入你的帳號與密碼</h1>
                            <p>
                                <label for="username" class="uname">帳號 </label>
                                <asp:TextBox ID="txtLoginName" runat="server" placeholder="請登錄帳號名稱"></asp:TextBox>
                            </p>
                            <p>
                                <label for="password" class="youpasswd">密碼 </label>
                                <asp:TextBox ID="txtLoginPassword" runat="server" TextMode="Password" placeholder="請輸入您設定的密碼"></asp:TextBox>
                            </p>
                            <p class="login button">
                                <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="red"></asp:Label>
                                &nbsp;
                                <asp:Button ID="btnLogin" runat="server" Text="登入" OnClick="btnLogin_Click" />
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
</body>
</html>
<script type="text/javascript" src="App_Js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#btnLogin").click(function () {
            $("#lblMsg").text('登入中請稍後...');
        });
    });
</script>
