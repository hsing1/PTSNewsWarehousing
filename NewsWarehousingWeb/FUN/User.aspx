﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="NewsWarehousingWeb.FUN.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <table width="100%" class="tblBase">
        <tr>
            <td class="tdHeader-Green">
                <asp:Label ID="lblTitle" runat="server" Text="使用者權限" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" class="tblBase">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td class="tdHeader-Blue" width="15%">
                                        姓名、帳號或員編
                                    </td>
                                    <td class="tdValue-Blue" width="85%">
                                        <asp:TextBox ID="txtKeyWord" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />&nbsp;
                                        <asp:Button ID="btnNewUser" runat="server" Text="新增" OnClick="btnNewUser_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="tdValue-Blue" width="100%">
                            <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="功能">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnModify" runat="server" Text="修改" 
                                                CommandArgument='<%# Bind("EmpID") %>' CommandName="Modify" OnCommand="btnModify_Command"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="帳號" DataField="DomainID">
                                        <ItemStyle Width="120px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="姓名" DataField="EmpName">
                                        <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="部門" DataField="UnitFullName">
                                        <ItemStyle Width="250px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="權限">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>編輯權限：</td>
                                                    <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("IsEditor") %>'></asp:Label></td>
                                                    <td>&nbsp;|&nbsp;</td>
                                                    <td>審核權限：</td>
                                                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("IsApprover") %>'></asp:Label></td>
                                                    <td>&nbsp;|&nbsp;</td>
                                                    <td>入庫權限：</td>
                                                    <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("IsPreArchive") %>'></asp:Label></td>
                                                    <td>&nbsp;|&nbsp;</td>
                                                    <td>最高權限：</td>
                                                    <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("IsAdmin") %>'></asp:Label></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle BackColor="#E3EAEB" />
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
</asp:Content>
