﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewsWarehousingApp
{
    public partial class MainForm : Form
    {
        private string NewsArchivePath;
        private string NewsMp4Path;
        string appPath;
        private List<FtpServer> ftpsvr = null;

        public MainForm()
        {
            InitializeComponent();

            CustomizedInit();
        }

        protected void CustomizedInit()
        {
            NewsArchivePath = @"\\10.13.200.15\Archive";
            NewsMp4Path = @"\\10.13.200.15\Archive\MP4";

            appPath = Application.StartupPath;

            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                ftpsvr = DB.SysReference.Where(x => x.ItemType == "110").Select(x => new FtpServer { NewsType = x.ItemValue, IP = x.ItemName }).ToList();
            }

            //NetHelper.NetUse(@"\\10.13.24.229\homes\pts", "admin", "pts870701");

            NetHelper.NetUse(@"\\10.13.200.15\Archive", "ArchiveUser", "Archive870701");

            NetHelper.NetUse(@"\\10.1.164.34\PTS_Import_New", "admin", "pts2012");

            label2.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd");
        }


        protected override void OnLoad(EventArgs e)
        {
            label4.Text = PreArchiveTimer.Enabled.ToString();
            label6.Text = BorrowTimer.Enabled.ToString();
            label8.Text = MainTimer.Enabled.ToString();

            base.OnLoad(e);
            
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {

            textBox1.AppendText($"MainTimer Hour : {DateTime.Now.Hour.ToString()} \r\n");

            //bool Flag = false;
            if (DateTime.Now.Hour == 00)
            {
                //    using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                //    {
                //        var q = DB.ScheduleLog.Where(x => DbFunctions.TruncateTime(x.ScheduleTime).Value.CompareTo(DateTime.Today) >= 0).Select(x => x).FirstOrDefault();

                //        if (q == null) Flag = true;

                //    }


                if (!FTPWorkerMain.IsBusy)
                {
                    textBox1.AppendText("Start RunDownload!\r\n");
                    FTPWorkerMain.RunWorkerAsync();
                }
            }

            //if(Flag)
            //{

            //}
        }

        private void FTPWorkerMain_DoWork(object sender, DoWorkEventArgs e)
        {
            RunDownload();
        }

        private void FTPWorkerMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            textBox1.AppendText("End RunDownload!\r\n");
        }

        private void FTPWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var item = (VideoMeta)e.Argument;

            try
            {
                var obj = new FileHelper();

                var filename = Guid.NewGuid().ToString("N");

                item.ArchiveName = filename + ".mxf";
                item.MP4Name = filename + ".mp4";

                switch (item.NewsType)
                {
                    case "9001":
                    case "9002":
                        item.ArchivePath = Path.Combine(NewsArchivePath, "PTS");
                        break;
                    case "9003":
                    case "9005":
                        item.ArchivePath = Path.Combine(NewsArchivePath, "Hakka");
                        break;
                    case "9004":
                        item.ArchivePath = Path.Combine(NewsArchivePath, "Macroview");
                        break;
                    default:
                        break;
                }

                
                item.MP4Path = NewsMp4Path;

                var server = ftpsvr.Where(x => x.NewsType == item.NewsType).Select(x => x).FirstOrDefault();

                item.FileStstus = 1;

                FTPWorker1.ReportProgress(0, item);

                var fullName = Path.Combine(item.ArchivePath, item.ArchiveName);

                obj.GetMXF("ftp://" + server.IP + "/" + item.OriginalFile, fullName);

                var cmd = appPath + '\\' + $"MediaInfo --Output=XML --LogFile={appPath}\\{filename}.xml {fullName}";

                ProcessStartInfo psi = new ProcessStartInfo(@"C:\Windows\System32\cmd");
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = ProcessWindowStyle.Minimized;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;

                Process process = Process.Start(psi);

                process.StandardInput.WriteLine(cmd);
                SpinWait.SpinUntil(() => false, 10000);

                process.StandardInput.WriteLine("exit");
                //SpinWait.SpinUntil(() => false, 10000);

                string output = process.StandardOutput.ReadToEnd();

                process.WaitForExit();

                if (process.HasExited)
                {
                    process.Close();
                    process.Dispose();
                }

                var info = new FileInfo($"{appPath}\\{filename}.xml");

                int i = 0;

                while (IsFileLocked(info))
                {
                    if (i > 20) break;

                    SpinWait.SpinUntil(() => false, 2000);
                    i++;
                }

                if (i <= 20)
                {
                    item.MediaInfo = File.ReadAllText($"{appPath}\\{filename}.xml");
                    info.Delete();
                }

                obj.GetMP4(item.OriginalLow, NewsMp4Path, item.MP4Name);

                item.FileStstus = 2;

                e.Result = item;
            }
            catch (Exception ex)
            {
                Utility.SaveException("FTPWorker1_DoWork", ex);
                item.FileStstus = 99;
                e.Result = item;

            }

        }

        private void FTPWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var obj = (VideoMeta)e.UserState;
            DoCahnge(obj);
            textBox1.AppendText("File:" + obj.OriginalFile + " Start! \r\n");
        }

        private void FTPWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            var obj = (VideoMeta)e.Result;
            DoCahnge(obj);
            if (obj.FileStstus == 99) textBox1.AppendText("File:" + obj.OriginalFile + " Error!\r\n");
            else textBox1.AppendText("File:" + obj.OriginalFile + " Finish!\r\n");

        }

        private void FTPWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            var item = (VideoMeta)e.Argument;

            try
            {
                var obj = new FileHelper();

                var filename = Guid.NewGuid().ToString("N");

                item.ArchiveName = filename + ".mxf";
                item.MP4Name = filename + ".mp4";

                switch (item.NewsType)
                {
                    case "9001":
                    case "9002":
                        item.ArchivePath = Path.Combine(NewsArchivePath, "PTS");
                        break;
                    case "9003":
                    case "9005":
                        item.ArchivePath = Path.Combine(NewsArchivePath, "Hakka");
                        break;
                    case "9004":
                        item.ArchivePath = Path.Combine(NewsArchivePath, "Macroview");
                        break;
                    default:
                        break;
                }

                item.MP4Path = NewsMp4Path;

                var server = ftpsvr.Where(x => x.NewsType == item.NewsType).Select(x => x).FirstOrDefault();

                item.FileStstus = 1;

                FTPWorker2.ReportProgress(0, item);

                var fullName = Path.Combine(item.ArchivePath, item.ArchiveName);

                obj.GetMXF("ftp://" + server.IP + "/" + item.OriginalFile, fullName);

                var cmd = appPath + '\\' + $"MediaInfo --Output=XML --LogFile={appPath}\\{filename}.xml {fullName}";

                ProcessStartInfo psi = new ProcessStartInfo(@"C:\Windows\System32\cmd");
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = ProcessWindowStyle.Minimized;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;

                Process process = Process.Start(psi);

                process.StandardInput.WriteLine(cmd);
                SpinWait.SpinUntil(() => false, 10000);

                process.StandardInput.WriteLine("exit");
                //SpinWait.SpinUntil(() => false, 10000);

                string output = process.StandardOutput.ReadToEnd();

                process.WaitForExit();

                if (process.HasExited)
                {
                    process.Close();
                    process.Dispose();
                }

                var info = new FileInfo($"{appPath}\\{filename}.xml");

                int i = 0;

                while (IsFileLocked(info))
                {
                    if (i > 20) break;

                    SpinWait.SpinUntil(() => false, 2000);
                    i++;
                }

                if (i <= 20)
                {
                    item.MediaInfo = File.ReadAllText($"{appPath}\\{filename}.xml");
                    info.Delete();
                }

                obj.GetMP4(item.OriginalLow, NewsMp4Path, item.MP4Name);

                item.FileStstus = 2;

                e.Result = item;
            }
            catch (Exception ex)
            {
                Utility.SaveException("FTPWorker2_DoWork", ex);
                item.FileStstus = 99;
                e.Result = item;

            }
            
        }

        private void FTPWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var obj = (VideoMeta)e.UserState;
            DoCahnge(obj);
            textBox1.AppendText("File:" + obj.OriginalFile + " Start! \r\n");
        }

        private void FTPWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            var obj = (VideoMeta)e.Result;
            DoCahnge(obj);
            if (obj.FileStstus == 99) textBox1.AppendText("File:" + obj.OriginalFile + " Error!\r\n");
            else textBox1.AppendText("File:" + obj.OriginalFile + " Finish!\r\n");

        }

        private void iNewsWorker_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void iNewsWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void iNewsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void DoCahnge(VideoMeta item)
        {
            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                DB.Entry(item).State = EntityState.Modified;

                var pre = DB.PreArchive.Find(item.PreArchiveID);

                switch (item.FileStstus)
                {
                    case 1:
                        pre.ArchiveStatus = 4;
                        break;
                    case 2:
                        pre.ArchiveStatus = 5;
                        break;
                    case 99:
                        pre.ArchiveStatus = 99;
                        break;
                    default:
                        break;
                }
                
                DB.SaveChanges();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var obj = new NewsHelper();

            var s = obj.iNewsFileGet("9002", "9002");

            textBox1.Text = s;

            obj.iNewsFileResolve(ref s, "", "", "9002");

            

            //var A1 = new List<string>() { @"\\PROXYSVR\proxy\a9b3ba30a1d04dfa9987f57aafa33241\proxy.mp4", @"\\PROXYSVR\proxy\a7c9e14f6b314fb8b8d17afe785606da\proxy.mp4" };
            //var A2 = new List<string>() { @"F:\0001.mp4", @"F:\0002.mp4" };

            //var A1 = new List<string>() { @"ftp://10.232.2.75/ONAIR/JACK_00000002", @"ftp://10.232.2.75/ONAIR/News-HL2_00000001" };
            //var A2 = new List<string>() { @"F:\0001.mxf", @"F:\0002.mxf" };

            //for (var i = 0; i < 2; i++)
            //{
            //var obj = new FileHelper();

            //obj.GetMXF(@"ftp://10.232.1.75/ONAIR/1230尼伯特海警(公)_000000DM", @"D:\1230尼伯特海警(公)_000000DM.mxf");
            //}

            //bool Flag = false;
            //if (DateTime.Now.Hour == 19)
            //{
            //    using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            //    {
            //        var q = DB.ScheduleLog.Where(x => DbFunctions.TruncateTime(x.ScheduleTime).Value.CompareTo(DateTime.Today) >= 0).Select(x => x).FirstOrDefault();

            //        if (q == null) Flag = true;

            //    }
            //}

            //if (Flag)
            //{

            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!FTPWorkerMain.IsBusy)
            {
                textBox1.AppendText("Start RunDownload!\r\n");
                FTPWorkerMain.RunWorkerAsync();
            }
            else
            {
                textBox1.AppendText("RunDownload Is Busy!\r\n");
            }

        }

        void RunDownload()
        {
            //Task.Run(() =>
            //{
            List<VideoMeta> results = null;

            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                results = DB.VideoMeta.Where(x => x.FileStstus == 0).Select(x => x).ToList();
            }

            if (results == null || results.Count == 0) return;

            var RemoveItems = new List<VideoMeta>();

            while (true)
            {
                foreach (var item in results)
                {
                    if (!FTPWorker1.IsBusy)
                    {
                        FTPWorker1.RunWorkerAsync(item);
                        RemoveItems.Add(item);
                        continue;
                    }

                    if (!FTPWorker2.IsBusy)
                    {
                        FTPWorker2.RunWorkerAsync(item);
                        RemoveItems.Add(item);
                        continue;
                    }

                }

                foreach (var item in RemoveItems)
                {
                    results.Remove(item);
                }

                RemoveItems.Clear();

                if (results.Count == 0) break;
            }
            //});
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                List<VideoMeta> results = null;

                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    results = DB.VideoMeta.Where(x => x.FileStstus == 95).Select(x => x).ToList();
                }

                if (results == null || results.Count == 0) return;

                var RemoveItems = new List<VideoMeta>();

                while (true)
                {
                    foreach (var item in results)
                    {
                        if (!ReLoadWorker1.IsBusy)
                        {
                            ReLoadWorker1.RunWorkerAsync(item);
                            RemoveItems.Add(item);
                            continue;
                        }

                        //if (!ReLoadWorker2.IsBusy)
                        //{
                        //    ReLoadWorker2.RunWorkerAsync(item);
                        //    RemoveItems.Add(item);
                        //    continue;
                        //}

                    }

                    foreach (var item in RemoveItems)
                    {
                        results.Remove(item);
                    }

                    RemoveItems.Clear();

                    if (results.Count == 0) break;
                }
            });
        }

        private void ReLoadWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var item = (VideoMeta)e.Argument;

            try
            {
                var obj = new FileHelper();
                
                if(string.IsNullOrEmpty(item.ArchiveName))
                {
                    var filename = Guid.NewGuid().ToString("N");
                    item.ArchiveName = filename + ".mxf";
                    item.MP4Name = filename + ".mp4";
                    item.ArchivePath = NewsArchivePath;
                    item.MP4Path = NewsMp4Path;
                }

                
                var server = ftpsvr.Where(x => x.NewsType == item.NewsType).Select(x => x).FirstOrDefault();

                item.FileStstus = 1;

                ReLoadWorker1.ReportProgress(0, item);

                if (!File.Exists(Path.Combine(NewsArchivePath, item.ArchiveName)))
                {
                    obj.GetMXF("ftp://" + server.IP + "/" + item.OriginalFile, Path.Combine(NewsArchivePath, item.ArchiveName));
                }

                if (!File.Exists(Path.Combine(NewsMp4Path, item.MP4Name)))
                {
                    obj.GetMP4(item.OriginalLow, NewsMp4Path, item.MP4Name);
                }


                item.FileStstus = 2;
                e.Result = item;
            }
            catch (Exception ex)
            {
                Utility.SaveException("ReLoadWorker1_DoWork", ex);
                item.FileStstus = 99;
                e.Result = item;
                e.Cancel = true;
            }


        }

        private void ReLoadWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var obj = (VideoMeta)e.UserState;
            DoCahnge(obj);
            textBox1.AppendText("File:" + obj.OriginalFile + " ReStart! \r\n");
        }

        private void ReLoadWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            var obj = (VideoMeta)e.Result;
            DoCahnge(obj);
            if (obj.FileStstus == 99) textBox1.AppendText("File:" + obj.OriginalFile + " Error!\r\n");
            else textBox1.AppendText("File:" + obj.OriginalFile + " Finish!\r\n");

        }

        private void ReLoadWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            var item = (VideoMeta)e.Argument;

            try
            {
                var obj = new FileHelper();

                if (string.IsNullOrEmpty(item.ArchiveName))
                {
                    var filename = Guid.NewGuid().ToString("N");
                    item.ArchiveName = filename + ".mxf";
                    item.MP4Name = filename + ".mp4";
                    item.ArchivePath = NewsArchivePath;
                    item.MP4Path = NewsMp4Path;
                }


                var server = ftpsvr.Where(x => x.NewsType == item.NewsType).Select(x => x).FirstOrDefault();

                item.FileStstus = 1;

                ReLoadWorker2.ReportProgress(0, item);

                if (!File.Exists(Path.Combine(NewsArchivePath, item.ArchiveName)))
                {
                    obj.GetMXF("ftp://" + server.IP + "/" + item.OriginalFile, Path.Combine(NewsArchivePath, item.ArchiveName));
                }

                if (!File.Exists(Path.Combine(NewsMp4Path, item.MP4Name)))
                {
                    obj.GetMP4(item.OriginalLow, NewsMp4Path, item.MP4Name);
                }


                item.FileStstus = 2;
            }
            catch (Exception ex)
            {
                Utility.SaveException("ReLoadWorker2_DoWork", ex);
                item.FileStstus = 99;
                e.Result = item;
                e.Cancel = true;
            }

            e.Result = item;
        }

        private void ReLoadWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var obj = (VideoMeta)e.UserState;
            DoCahnge(obj);
            textBox1.AppendText("File:" + obj.OriginalFile + " ReStart! \r\n");
        }

        private void ReLoadWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var obj = (VideoMeta)e.Result;
            DoCahnge(obj);
            if(obj.FileStstus == 99) textBox1.AppendText("File:" + obj.OriginalFile + " Error!\r\n");
            else textBox1.AppendText("File:" + obj.OriginalFile + " Finish!\r\n");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var obj = new NewsHelper();

            var s = obj.iNewsFileGet("9003", "9003");

            textBox1.Text = s;

            obj.iNewsFileResolve(ref s, "", "", "9003");

        }

        private void button5_Click(object sender, EventArgs e)
        {

            //RunBorrow();
        }

        void RunBorrow(BackgroundWorker worker, DoWorkEventArgs e)
        {

            List<vwBorrowList> results;

            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                results = DB.vwBorrowList
                            .Where(x => x.BorrowStatus == 1)
                            .Select(x => x)
                            .ToList();
                
            }

            if (results == null || results.Count == 0) return;

            var RemoveItems = new List<vwBorrowList>();

            while (true)
            {

                if (worker.CancellationPending) 
                {
                    e.Cancel = true;
                    break;
                }

                foreach (var item in results)
                {
                    if (!BorrowWorker1.IsBusy)
                    {
                        BorrowWorker1.RunWorkerAsync(item);
                        RemoveItems.Add(item);
                        continue;
                    }

                    if (!BorrowWorker2.IsBusy)
                    {
                        BorrowWorker2.RunWorkerAsync(item);
                        RemoveItems.Add(item);
                        continue;
                    }

                }

                foreach (var item in RemoveItems)
                {
                    results.Remove(item);
                }

                RemoveItems.Clear();
                
                if (results.Count == 0) break;
            }



        }

        private void BorrowWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var item = (vwBorrowList)e.Argument;
            var path = "";
            
            try
            {
                
                if (item.BorrowType == "A")
                {
                    path = Path.Combine(@"\\10.13.200.15\ShareFolder\Borrow", item.UserID);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, DateTime.Now.ToString("yyyyMMdd"));

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                }
                else
                {
                    path = @"\\10.1.164.34\PTS_Import_New\Archive_new";
                    
                }

                

                item.BorrowStatus = 2;

                BorrowWorker1.ReportProgress(0, item);

                if (!File.Exists(Path.Combine(path, item.Slogan + ".mxf")))
                {
                    File.Copy(Path.Combine(item.ArchivePath, item.ArchiveName), Path.Combine(path, item.Slogan + ".mxf"), true);

                    item.BorrowStatus = 3;
                    item.FilePosition = path;
                }
                else
                {
                    item.BorrowStatus = 88;
                }


                e.Result = item;
            }
            catch (Exception ex)
            {
                Utility.SaveException("BorrowWorker1_DoWork", ex);
                item.BorrowStatus = 99;
                e.Result = item;

            }

        }

        private void BorrowWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                var obj = (vwBorrowList)e.UserState;
                DoBorrowCahnge(obj);
            }
            catch (Exception ex)
            {
                Utility.SaveException("BorrowWorker1_ProgressChanged", ex);
            }
            //textBox1.AppendText("File:" + obj.ArchiveName + " Borrow1Start! \r\n");
        }

        private void BorrowWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var obj = (vwBorrowList)e.Result;
                DoBorrowCahnge(obj);
            }
            catch (Exception ex)
            {

                Utility.SaveException("BorrowWorker1_RunWorkerCompleted", ex);
            }
            //if (obj.BorrowStatus == 99) textBox1.AppendText("File:" + obj.ArchiveName + " Borrow1Error!\r\n");
            //else textBox1.AppendText("File:" + obj.ArchiveName + " Borrow1Finish!\r\n");
        }

        private void BorrowWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            var item = (vwBorrowList)e.Argument;
            var path = "";

            try
            {
                if (item.BorrowType == "A")
                {
                    path = Path.Combine(@"\\10.13.200.15\ShareFolder\Borrow", item.UserID);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, DateTime.Now.ToString("yyyyMMdd"));

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                }
                else
                {
                    path = @"\\10.1.164.34\PTS_Import_New\Archive_new";

                }

                item.BorrowStatus = 2;
                
                BorrowWorker2.ReportProgress(0, item);

                if (!File.Exists(Path.Combine(path, item.Slogan + ".mxf")))
                {
                    File.Copy(Path.Combine(item.ArchivePath, item.ArchiveName), Path.Combine(path, item.Slogan + ".mxf"), true);

                    item.BorrowStatus = 3;
                    item.FilePosition = path;
                }
                else
                {
                    item.BorrowStatus = 88;
                }

                e.Result = item;
            }
            catch (Exception ex)
            {
                Utility.SaveException("BorrowWorker2_DoWork", ex);
                item.BorrowStatus = 99;
                e.Result = item;

            }


        }

        private void BorrowWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                var obj = (vwBorrowList)e.UserState;
                DoBorrowCahnge(obj);
            }
            catch (Exception ex)
            {
                Utility.SaveException("BorrowWorker2_ProgressChanged", ex);
            }
            //textBox1.AppendText("File:" + obj.ArchiveName + " Borrow2Start! \r\n");
        }

        private void BorrowWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var obj = (vwBorrowList)e.Result;
                DoBorrowCahnge(obj);
            }
            catch (Exception ex)
            {
                Utility.SaveException("BorrowWorker2_RunWorkerCompleted", ex);
            }
            //if (obj.BorrowStatus == 99) textBox1.AppendText("File:" + obj.ArchiveName + " Borrow2Error!\r\n");
            //else textBox1.AppendText("File:" + obj.ArchiveName + " Borrow2Finish!\r\n");
        }

        void DoBorrowCahnge(vwBorrowList item)
        {
            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                var r = DB.BorrowList.Find(item.BorrowID);

                r.BorrowStatus = item.BorrowStatus;
                r.FilePosition = item.FilePosition;

                DB.SaveChanges();
            }
        }

        private void BorrowWorkerMain_DoWork(object sender, DoWorkEventArgs e)
        {
            if (BorrowWorkerMain.CancellationPending) //如果被中斷...
                e.Cancel = true;

            BackgroundWorker worker = (BackgroundWorker)sender;

            RunBorrow(worker, e);
        }

        private void BorrowWorkerMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            textBox1.AppendText("BorrowWorkerMain End " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd") + "\r\n");
        }

        private void BorrowTimer_Tick(object sender, EventArgs e)
        {
            if (!BorrowWorkerMain.IsBusy)
            {
                textBox1.AppendText("BorrowWorkerMain Start " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd") + "\r\n");
                BorrowWorkerMain.RunWorkerAsync();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                var item = DB.BorrowList.Find(1);

                textBox2.Text = item.UserID;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var NewsDB = new PTSNewsWarehousingEntities();

            var items = NewsDB.PreArchive
                              .Where(x => x.ArchiveConfirm == true && x.ArchiveStatus == 1)
                              .Select(x => x);


            var GVDB = new MediaFrameEntities();

            var endDate = DateTime.Now.AddDays(-14);

            var gvItems = GVDB.URN2URI
                              .Where(x => x.URIDevice == "SUB02A" || x.URIDevice == "SUBK2C31")
                              .Where(x => x.URILocation == "V:/ONAIR")
                              .Where(x => x.CreationDate >= endDate)
                              .OrderByDescending(x => x.CreationDate)
                              .Select(x => new { x.URN, x.URIAssetID, x.CreationDate, x.URIDevice })
                              .ToList();

            var device = NewsDB.SysReference
                               .Where(x => x.ItemType == "120" && x.IsExist == true)
                               .Select(x => x)
                               .ToDictionary(x => x.ItemValue, x => x.ItemName);


            foreach (var item in items)
            {
                var site = device[item.NewsType];
                var slogan = item.Slogan.Trim() + '_';

                var match = gvItems.Where(x => x.URIAssetID.StartsWith(slogan))
                                   .Where(x => x.URIDevice == site)
                                   .OrderByDescending(x => x.CreationDate)
                                   .Take(1)
                                   .FirstOrDefault();


                if (match == null)
                {
                    item.ArchiveStatus = 990;
                }
                else
                {
                    item.MXFName = match.URIAssetID;
                    item.MP4Name = match.URN.ToString().Replace("-", "");
                    item.ArchiveStatus = 2;
                }
            }

            NewsDB.SaveChanges();

            NewsDB.Dispose();
            GVDB.Dispose();
            
        }


        private void button8_Click(object sender, EventArgs e)
        {

            var help = new NewsHelper();
            help.NewsResolve();
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            BorrowTimer.Enabled = false;
            BorrowWorkerMain.CancelAsync();

        }

        private void PreArchiveTimer_Tick(object sender, EventArgs e)
        {
            if (!PreArchiveWorker.IsBusy)
            {
                textBox1.AppendText("PreArchiveWorker Start " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd") + "\r\n");
                PreArchiveWorker.RunWorkerAsync();
            }
        }

        private void PreArchiveWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var NewsDB = new PTSNewsWarehousingEntities();

            var items = NewsDB.PreArchive
                              .Where(x => x.ArchiveConfirm == true && x.ArchiveStatus == 1)
                              .Select(x => x).ToList();


            if (items.Count == 0) return;

            var GVDB = new MediaFrameEntities();

            var endDate = DateTime.Now.AddDays(-30);

            var gvItems = GVDB.URN2URI
                              .Where(x => x.URIDevice == "SUB02A" || x.URIDevice == "SUBK2C31")
                              .Where(x => x.URILocation == "V:/ONAIR")
                              .Where(x => x.CreationDate >= endDate)
                              .OrderByDescending(x => x.CreationDate)
                              .Select(x => new { x.URN, x.URIAssetID, x.CreationDate, x.URIDevice })
                              .ToList();

            var device = NewsDB.SysReference
                   .Where(x => x.ItemType == "120" && x.IsExist == true)
                   .Select(x => x)
                   .ToDictionary(x => x.ItemValue, x => x.ItemName);

            foreach (var item in items)
            {
                var site = device[item.NewsType];
                var slogan = item.Slogan.Trim() + '_';

                var match = gvItems.Where(x => x.URIAssetID.StartsWith(slogan))
                                   .Where(x => x.URIDevice == site)
                                   .OrderByDescending(x => x.CreationDate)
                                   .Take(1)
                                   .FirstOrDefault();


                if (match == null)
                {
                    item.ArchiveStatus = 990;
                }
                else
                {
                    item.MXFName = match.URIAssetID;
                    item.MP4Name = match.URN.ToString().Replace("-", "");
                   
                    item.ArchiveStatus = 2;
                }
            }

            NewsDB.SaveChanges();

            NewsDB.Dispose();
            GVDB.Dispose();


            var help = new NewsHelper();
            help.NewsResolve();
        }

        private void PreArchiveWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            textBox1.AppendText("PreArchiveWorker End " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd") + "\r\n");
        }

        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            var help = new NewsHelper();
            help.NewsResolve(int.Parse(textBox3.Text.Trim()));
        }
    }
}
