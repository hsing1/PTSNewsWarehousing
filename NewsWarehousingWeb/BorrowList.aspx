﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="BorrowList.aspx.cs" Inherits="NewsWarehousingWeb.BorrowList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%" class="tblBase">
        <tr>
            <td class="tdHeader-Green">
                <asp:Label ID="lblTitle" runat="server" Text="調用確認及結果" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdHeader-Green" style="text-align:left">
                <table style="width:100%">
                    <tr>
                        <td style="text-align:left;">
                            <asp:Button ID="btnConfirm" runat="server" Text="確認調用" OnClick="btnConfirm_Click" OnClientClick="return GetValue();" />
                            <asp:HiddenField ID="hfValue" runat="server" />
                        </td>
                        <td style="text-align:right;">
                            <asp:LinkButton ID="btnReload" runat="server" OnClick="btnReload_Click">重整網頁</asp:LinkButton>
                        </td>
                    </tr>
                </table>
                
            </td>
        </tr>
        <tr>
            <td class="tdValue-Blue" style="width:600px">
                <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table style="width:100%">
                                    <tr>
                                        <td style="width:25%; text-align:left;">
                                            <input id="AllApprove" type="checkbox" onclick="ApproveAll(this);" />
                                        </td>
                                        <td style="width:50%; text-align: center;">
                                            全選
                                        </td>
                                        <td style="width:25%; text-align:right;">
                                            <input id="AllDelete" type="checkbox" onclick="DeleteAll(this);" />
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="lalMenu" runat="server"></asp:Literal>
                            </ItemTemplate>
                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="系統編號" DataField="BorrowID" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="標題" DataField="Slogan" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="調用狀態" DataField="ItemName" >
                        <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="調用方式" DataField="BorrowType" >
                        <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="檔案位置" DataField="FilePosition" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SortedAscendingCellStyle BackColor="#EDF6F6" />
                    <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                    <SortedDescendingCellStyle BackColor="#D6DFDF" />
                    <SortedDescendingHeaderStyle BackColor="#002876" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script>

        function ApproveAll(obj) {
            if ($(obj).prop("checked")) {
                $("input[id^='cbApprove_']").each(function () {
                    $(this).prop("checked", true);
                });

                $("input[id^='cbDelete_']").each(function () {
                    $(this).prop("checked", false);
                });

                $("#AllDelete").prop("checked", false);
            } else {
                $("input[id^='cbApprove_']").each(function () {
                    $(this).prop("checked", false);
                });
            }
        }

        function DeleteAll(obj) {
            if ($(obj).prop("checked")) {
                $("input[id^='cbDelete_']").each(function () {
                    $(this).prop("checked", true);
                });

                $("input[id^='cbApprove_']").each(function () {
                    $(this).prop("checked", false);
                });

                $("#AllApprove").prop("checked", false);
            } else {
                $("input[id^='cbDelete_']").each(function () {
                    $(this).prop("checked", false);
                });
            }
        }

        function Change1(id) {
            if ($('#cbApprove_' + id).prop("checked")) {
                $('#cbDelete_' + id).prop("checked", false);
            }
        }

        function Change2(id) {
            if ($('#cbDelete_' + id).prop("checked")) {
                $('#cbApprove_' + id).prop("checked", false);
            }
        }

        function GetValue() {
            var value = {
                approveItems: [],
                deleteItems: []
            };

            $("input[id^='cbApprove_']").each(function () {
                if ($(this).prop("checked")) {
                    value.approveItems.push($(this).prop("value"));
                }
            });

            $("input[id^='cbDelete_']").each(function () {
                if ($(this).prop("checked")) {
                    value.deleteItems.push($(this).prop("value"));
                }
            });

            if (value.approveItems.length === 0 && value.deleteItems.length === 0) {
                alert('請最少選擇一個項目!');
                return false;
            } else {
                $('#<% =hfValue.ClientID %>').val(JSON.stringify(value));
                return true;
            }
        }

    </script>
</asp:Content>
