﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using NewsChangeLocation.Models;


namespace NewsChangeLocation
{
    public partial class MainForm : Form
    {

        delegate void PrintHandler(TextBox tb, string text);
        private object _syncRoot = new object();

        private int Move12AMSec;
        private System.Threading.Timer _ThreadTimerMove12AM = null;

        public MainForm()
        {
            InitializeComponent();

            DateTime ToDay = DateTime.Now;

            var Move12AM = Convert.ToDateTime(ToDay.AddDays(1).ToString("yyyy-MM-dd 00:00:00"));

            Move12AMSec = (int)Math.Round((Move12AM - ToDay).TotalSeconds, 0);

            _ThreadTimerMove12AM = new System.Threading.Timer(new System.Threading.TimerCallback(RunAllMove), null, 1000 * Move12AMSec, 1000 * 60 * 60 * 24);

            txtMessage.Text = $"執行時間:{Move12AM.ToString()}";

            btnSatrt.Enabled = false;
        }


        

        void PrintMessage(TextBox tb, string text)
        {
            //判斷這個物件是否在同一個執行緒上
            if (tb.InvokeRequired)
            {
                //當InvokeRequired為true時，表示在不同的執行緒上，所以進行委派的動作!!
                PrintHandler ph = new PrintHandler(PrintMessage);
                tb.Invoke(ph, tb, text);
            }
            else
            {
                lock (_syncRoot)
                {
                    //表示在同一個執行緒上了，所以可以正常的呼叫到這個物件
                    tb.AppendText(text + Environment.NewLine);
                }

            }
        }

        private  void btnSatrt_Click(object sender, EventArgs e)
        {
            _ThreadTimerMove12AM = new System.Threading.Timer(new System.Threading.TimerCallback(RunAllMove), null, 1000 * Move12AMSec, 1000 * 60 * 60 * 24);
            btnSatrt.Enabled = false;
            btnStop.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _ThreadTimerMove12AM.Change(-1, 0);
            btnSatrt.Enabled = true;
            btnStop.Enabled = false;
        }
        private async void btnRunRightnow_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;
            await Task.Run(() => RunAllMove());
            ((Button)sender).Enabled = true;
        }

        void RunAllMove(object state = null)
        {
            PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

            var SQL = @"SELECT *
                          FROM VideoMeta
                         WHERE 1 = 1
                           AND ApproveStatus = 1
                           AND DeleteMark <> 1
                           AND SerialMark = 0
                         ORDER BY VideoID ";


            var res = DB.VideoMeta.SqlQuery(SQL).AsNoTracking().ToList();

            var dates = res.Select(x => x.CreateDate.ToString("yyyy-MM-dd"))
                           .Distinct()
                           .Select(x => new { Year = x.Split('-')[0], Month = x.Split('-')[1], Day = x.Split('-')[2] })
                           .ToList();

            var paths = res.Select(x => x.ArchivePath)
                           .Distinct()
                           .ToList();



            foreach (var path in paths)
            {

                foreach (var year in dates.Select(x => x.Year).Distinct())
                {
                    if (!Directory.Exists(Path.Combine(path, year)))
                    {
                        Directory.CreateDirectory(Path.Combine(path, year));
                    }

                    foreach (var month in dates.Where(x => x.Year == year).Select(x => x.Month).Distinct())
                    {
                        if (!Directory.Exists(Path.Combine(path, year, month)))
                        {
                            Directory.CreateDirectory(Path.Combine(path, year, month));
                        }

                        foreach (var day in dates.Where(x => x.Year == year && x.Month == month).Select(x => x.Day).Distinct())
                        {
                            if (!Directory.Exists(Path.Combine(path, year, month, day)))
                            {
                                Directory.CreateDirectory(Path.Combine(path, year, month, day));
                            }
                        }

                    }

                }
            }
            //long size = 0;

            var titles = DB.SysReference.Where(x => x.ItemType == "510").Select(x => x).ToList();

            foreach (var item in res)
            {
                if (string.IsNullOrEmpty(item.Channel)) continue;

                Task t = Task.Run(() =>
                {
                    PTSNewsWarehousingEntities DB1 = new PTSNewsWarehousingEntities();

                    var arrDate = item.CreateDate.ToString("yyyy-MM-dd").Split('-');

                    var date = new { Year = arrDate[0], Month = arrDate[1], Day = arrDate[2] };

                    var FileSrNo = DB1.usp_GetFileSrNo(date.Year, date.Month, date.Day, item.Channel).FirstOrDefault();

                    var title = titles.Where(x => x.ItemName == item.Channel).Select(x => x.ItemValue).FirstOrDefault();

                    var fileName = $"{title}{date.Year}{date.Month}{date.Day}{FileSrNo}.mxf";

                    var targetPath = Path.Combine(item.ArchivePath, date.Year, date.Month, date.Day);

                    var target = Path.Combine(targetPath, fileName);

                    var source = Path.Combine(item.ArchivePath, item.ArchiveName);

                    PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Start:{source} to {target}");

                    File.Move(source, target);

                    PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Finish:{source} to {target}");


                    var info = new FileInfo(target);

                    var video = DB1.VideoMeta.Find(item.VideoID);

                    video.ArchivePath = targetPath;

                    video.ArchiveName = fileName;

                    video.ArchivePosition = 1;

                    video.SerialMark = true;

                    video.FileSize = info.Length;



                    DB1.SaveChanges();


                    DB1.Dispose();
                });
                t.Wait();

                //var source = Path.Combine(item.ArchivePath, item.ArchiveName);

                //size += (new FileInfo(source)).Length;
            }

            DB.Dispose();

            PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} All Finish!");
            //txtMessage.Text = Math.Round(size / 1024.0 / 1024.0 / 1024.0).ToString();
        }

        private async void btnMoveBig_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;
            await Task.Run(() => MoveBigNews());
            ((Button)sender).Enabled = true;
        }

        void MoveBigNews()
        {
            var paths = new Dictionary<string, string>() { { "9001", "PTS" }, { "9002", "PTS" }, { "9003", "Hakka" }, { "9004", "Macroview" }, { "9005", "HakkaMgz" } };

            PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

            var SQL = @"SELECT *
                          FROM VideoMeta
                         WHERE DATEDIFF(MINUTE,CONVERT(TIME,'00:05:00.000'),CONVERT(TIME,Duration)) > 0
                           AND ApproveStatus = 1
                           AND Nature <> 'A'
                           AND DeleteMark <> 1
						   AND SerialMark = 1
						   AND CreateDate < '2018-01-01'
                         ORDER BY NewsType ";

            var res = DB.VideoMeta.SqlQuery(SQL).AsNoTracking().ToList();


            foreach (var type in paths)
            {
                var dates = res.Where(x => x.NewsType == type.Key)
                               .Select(x => x.CreateDate.ToString("yyyy-MM-dd"))
                               .Distinct()
                               .Select(x => new { Year = x.Split('-')[0], Month = x.Split('-')[1], Day = x.Split('-')[2] })
                               .ToList();



                var path = $@"\\10.13.200.98\mamspace\News\{type.Value}";


                foreach (var year in dates.Select(x => x.Year).Distinct())
                {
                    if (!Directory.Exists(Path.Combine(path, year)))
                    {
                        Directory.CreateDirectory(Path.Combine(path, year));
                    }

                    foreach (var month in dates.Where(x => x.Year == year).Select(x => x.Month).Distinct())
                    {
                        if (!Directory.Exists(Path.Combine(path, year, month)))
                        {
                            Directory.CreateDirectory(Path.Combine(path, year, month));
                        }

                        foreach (var day in dates.Where(x => x.Year == year && x.Month == month).Select(x => x.Day).Distinct())
                        {
                            if (!Directory.Exists(Path.Combine(path, year, month, day)))
                            {
                                Directory.CreateDirectory(Path.Combine(path, year, month, day));
                            }
                        }

                    }

                }

                foreach (var item in res.Where(x => x.NewsType == type.Key))
                {
                    if (item.ArchivePath.IndexOf("10.13.200.98") >= 0) continue;

                    Task t = Task.Run(() =>
                    {

                        var arrDate = item.CreateDate.ToString("yyyy-MM-dd").Split('-');

                        var date = new { Year = arrDate[0], Month = arrDate[1], Day = arrDate[2] };

                        var targetPath = Path.Combine(path, date.Year, date.Month, date.Day);

                        var target = Path.Combine(targetPath, item.ArchiveName);

                        var source = Path.Combine(item.ArchivePath, item.ArchiveName);

                        PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Start:{source} to {target}");

                        File.Copy(source, target);

                        
                        if (File.Exists(target))
                        {
                            if ((new FileInfo(source)).Length != (new FileInfo(target)).Length)
                            {
                                File.Delete(target);

                                PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Fail:{source} to {target}");
                            }
                            else
                            {
                                PTSNewsWarehousingEntities DB1 = new PTSNewsWarehousingEntities();

                                var video = DB1.VideoMeta.Find(item.VideoID);

                                video.ArchivePath = targetPath;

                                DB1.SaveChanges();

                                File.Delete(source);

                                DB1.Dispose();

                                PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Finish:{source} to {target}");
                            }
                        }
                        else
                        {
                            PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Fail:{source} to {target}");
                        }


                    });
                    t.Wait();

                }


            }


        }

        private async void btnMoveHAKKAFile_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;
            await Task.Run(() => RunMove("B"));
            ((Button)sender).Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

            var list = new List<int>() { 3677, 3678, 3679, 3682, 3683, 3734, 3735, 3736, 3745, 3746, 3760, 3761, 3762, 3767, 3854, 3855, 3856, 3870, 3871, 3872, 3874, 3875, 3967, 3968, 4048, 4049, 4050, 4299, 4300, 4301, 4324, 4325, 4326, 4327, 4423, 4424, 4425, 4426, 4428, 4429, 4499, 4500 };
            var res = DB.VideoMeta
                        .Where(x => list.Contains(x.VideoID))
                        .Select(x => x)
                        .ToList();



            var dates = res.Select(x => x.CreateDate.ToString("yyyy-MM-dd"))
                           .Distinct()
                           .Select(x => new { Year = x.Split('-')[0], Month = x.Split('-')[1], Day = x.Split('-')[2] })
                           .ToList();

            var path = @"\\10.13.200.30\HakkaNewsBackup";


            foreach (var year in dates.Select(x => x.Year).Distinct())
            {
                if (!Directory.Exists(Path.Combine(path, year)))
                {
                    Directory.CreateDirectory(Path.Combine(path, year));
                }

                foreach (var month in dates.Where(x => x.Year == year).Select(x => x.Month).Distinct())
                {
                    if (!Directory.Exists(Path.Combine(path, year, month)))
                    {
                        Directory.CreateDirectory(Path.Combine(path, year, month));
                    }

                    foreach (var day in dates.Where(x => x.Year == year && x.Month == month).Select(x => x.Day).Distinct())
                    {
                        if (!Directory.Exists(Path.Combine(path, year, month, day)))
                        {
                            Directory.CreateDirectory(Path.Combine(path, year, month, day));
                        }
                    }

                }

            }

            foreach (var item in res)
            {
                

                Task t = Task.Run(() =>
                {
                    PTSNewsWarehousingEntities DB1 = new PTSNewsWarehousingEntities();

                    var arrDate = item.CreateDate.ToString("yyyy-MM-dd").Split('-');

                    var date = new { Year = arrDate[0], Month = arrDate[1], Day = arrDate[2] };

                    var FileSrNo = DB1.usp_GetFileSrNo(date.Year, date.Month, date.Day, "B").FirstOrDefault();

                    var fileName = $"HN{date.Year}{date.Month}{date.Day}{FileSrNo}.mxf";

                    var targetPath = Path.Combine(path, date.Year, date.Month, date.Day);

                    var target = Path.Combine(targetPath, fileName);

                    var source = Path.Combine(item.ArchivePath, item.ArchiveName);

                    //PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Start:{source} to {target}");

                    File.Move(source, target);

                    //PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Finish:{source} to {target}");



                    var video = DB1.VideoMeta.Find(item.VideoID);

                    video.ArchivePath = targetPath;

                    video.ArchiveName = fileName;

                    video.ArchivePosition = 2;

                    video.SerialMark = true;

                    DB1.SaveChanges();


                    DB1.Dispose();
                });
                t.Wait();

            }

        }



        void RunMove(string Channel)
        {
            PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

            var SQL = @"SELECT *
                          FROM VideoMeta
                         WHERE DATEDIFF(MINUTE,CONVERT(TIME,'00:05:00.000'),CONVERT(TIME,Duration)) > 0
                           AND ApproveStatus = 1
                           AND Nature <> 'A'
                           AND Channel = @p0
                           AND DeleteMark <> 1
                           AND CameraDate <= '2017-07-31'
                         ORDER BY VideoID ";

            object[] parameter = { Channel };

            var res = DB.VideoMeta.SqlQuery(SQL, parameter).AsNoTracking().ToList();



            var dates = res.Select(x => x.CreateDate.ToString("yyyy-MM-dd"))
                           .Distinct()
                           .Select(x => new { Year = x.Split('-')[0], Month = x.Split('-')[1], Day = x.Split('-')[2] })
                           .ToList();

            var path = @"\\10.13.200.30\HakkaNewsBackup";


            foreach (var year in dates.Select(x => x.Year).Distinct())
            {
                if (!Directory.Exists(Path.Combine(path, year)))
                {
                    Directory.CreateDirectory(Path.Combine(path, year));
                }

                foreach (var month in dates.Where(x => x.Year == year).Select(x => x.Month).Distinct())
                {
                    if (!Directory.Exists(Path.Combine(path, year, month)))
                    {
                        Directory.CreateDirectory(Path.Combine(path, year, month));
                    }

                    foreach (var day in dates.Where(x => x.Year == year && x.Month == month).Select(x => x.Day).Distinct())
                    {
                        if (!Directory.Exists(Path.Combine(path, year, month, day)))
                        {
                            Directory.CreateDirectory(Path.Combine(path, year, month, day));
                        }
                    }

                }

            }

            //long size = 0;

            foreach (var item in res)
            {
                if (item.ArchivePath.IndexOf("10.13.200.30") >= 0) continue;

                Task t = Task.Run(() =>
                {
                    PTSNewsWarehousingEntities DB1 = new PTSNewsWarehousingEntities();

                    var arrDate = item.CreateDate.ToString("yyyy-MM-dd").Split('-');

                    var date = new { Year = arrDate[0], Month = arrDate[1], Day = arrDate[2] };

                    var FileSrNo = DB1.usp_GetFileSrNo(date.Year, date.Month, date.Day, "B").FirstOrDefault();

                    var fileName = $"HN{date.Year}{date.Month}{date.Day}{FileSrNo}.mxf";

                    var targetPath = Path.Combine(path, date.Year, date.Month, date.Day);

                    var target = Path.Combine(targetPath, fileName);

                    var source = Path.Combine(item.ArchivePath, item.ArchiveName);

                    PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Start:{source} to {target}");

                    File.Copy(source, target);

                    PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} Finish:{source} to {target}");

                    if (File.Exists(target))
                    {
                        if ((new FileInfo(source)).Length != (new FileInfo(target)).Length)
                        {
                            File.Delete(target);
                        }
                        else
                        {

                            var video = DB1.VideoMeta.Find(item.VideoID);

                            video.ArchivePath = targetPath;

                            video.ArchiveName = fileName;

                            video.ArchivePosition = 2;

                            video.SerialMark = true;

                            DB1.SaveChanges();



                            File.Delete(source);
                        }
                    }

                    DB1.Dispose();
                });
                t.Wait();

                //var source = Path.Combine(item.ArchivePath, item.ArchiveName);

                //size += (new FileInfo(source)).Length;
            }

            DB.Dispose();

            PrintMessage(txtMessage, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} All Finish!");
            //txtMessage.Text = Math.Round(size / 1024.0 / 1024.0 / 1024.0).ToString();
        }


    }
}
