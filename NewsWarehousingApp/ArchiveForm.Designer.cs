﻿namespace NewsWarehousingApp
{
    partial class ArchiveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArchiveForm));
            this.ArchiveTimer = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.FTPWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.FTPWorker1 = new System.ComponentModel.BackgroundWorker();
            this.FTPWorker2 = new System.ComponentModel.BackgroundWorker();
            this.PreArchiveTimer = new System.Windows.Forms.Timer(this.components);
            this.PreArchiveWorker = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblArchive = new System.Windows.Forms.Label();
            this.lblPreArchive = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMainWork = new System.Windows.Forms.Label();
            this.lblWork1 = new System.Windows.Forms.Label();
            this.lblWork2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblPreWorker = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sslVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.lblAppStartTime = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ArchiveTimer
            // 
            this.ArchiveTimer.Enabled = true;
            this.ArchiveTimer.Interval = 1200000;
            this.ArchiveTimer.Tick += new System.EventHandler(this.ArchiveTimer_Tick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 129);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(441, 269);
            this.textBox1.TabIndex = 0;
            // 
            // FTPWorkerMain
            // 
            this.FTPWorkerMain.WorkerReportsProgress = true;
            this.FTPWorkerMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FTPWorkerMain_DoWork);
            this.FTPWorkerMain.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.FTPWorkerMain_ProgressChanged);
            this.FTPWorkerMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.FTPWorkerMain_RunWorkerCompleted);
            // 
            // FTPWorker1
            // 
            this.FTPWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FTPWorker1_DoWork);
            this.FTPWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.FTPWorker1_RunWorkerCompleted);
            // 
            // FTPWorker2
            // 
            this.FTPWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FTPWorker2_DoWork);
            this.FTPWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.FTPWorker2_RunWorkerCompleted);
            // 
            // PreArchiveTimer
            // 
            this.PreArchiveTimer.Enabled = true;
            this.PreArchiveTimer.Interval = 300000;
            this.PreArchiveTimer.Tick += new System.EventHandler(this.PreArchiveTimer_Tick);
            // 
            // PreArchiveWorker
            // 
            this.PreArchiveWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.PreArchiveWorker_DoWork);
            this.PreArchiveWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.PreArchiveWorker_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "ArchiveTimer :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(394, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "PreArchiveTimer :";
            // 
            // lblArchive
            // 
            this.lblArchive.AutoSize = true;
            this.lblArchive.Location = new System.Drawing.Point(100, 28);
            this.lblArchive.Name = "lblArchive";
            this.lblArchive.Size = new System.Drawing.Size(33, 12);
            this.lblArchive.TabIndex = 3;
            this.lblArchive.Text = "label3";
            // 
            // lblPreArchive
            // 
            this.lblPreArchive.AutoSize = true;
            this.lblPreArchive.Location = new System.Drawing.Point(491, 28);
            this.lblPreArchive.Name = "lblPreArchive";
            this.lblPreArchive.Size = new System.Drawing.Size(33, 12);
            this.lblPreArchive.TabIndex = 4;
            this.lblPreArchive.Text = "label3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "FTPWorkerMain :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(227, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "FTPWorker1 :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(227, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "FTPWorker2 :";
            // 
            // lblMainWork
            // 
            this.lblMainWork.AutoSize = true;
            this.lblMainWork.Location = new System.Drawing.Point(305, 28);
            this.lblMainWork.Name = "lblMainWork";
            this.lblMainWork.Size = new System.Drawing.Size(33, 12);
            this.lblMainWork.TabIndex = 8;
            this.lblMainWork.Text = "label3";
            // 
            // lblWork1
            // 
            this.lblWork1.AutoSize = true;
            this.lblWork1.Location = new System.Drawing.Point(305, 62);
            this.lblWork1.Name = "lblWork1";
            this.lblWork1.Size = new System.Drawing.Size(33, 12);
            this.lblWork1.TabIndex = 9;
            this.lblWork1.Text = "label3";
            // 
            // lblWork2
            // 
            this.lblWork2.AutoSize = true;
            this.lblWork2.Location = new System.Drawing.Point(305, 96);
            this.lblWork2.Name = "lblWork2";
            this.lblWork2.Size = new System.Drawing.Size(33, 12);
            this.lblWork2.TabIndex = 10;
            this.lblWork2.Text = "label3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(386, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "PreArchiveWorker :";
            // 
            // lblPreWorker
            // 
            this.lblPreWorker.AutoSize = true;
            this.lblPreWorker.Location = new System.Drawing.Point(491, 62);
            this.lblPreWorker.Name = "lblPreWorker";
            this.lblPreWorker.Size = new System.Drawing.Size(33, 12);
            this.lblPreWorker.TabIndex = 12;
            this.lblPreWorker.Text = "label3";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(472, 129);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Start / Stop Timer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(472, 186);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "立即下載";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sslVersion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 410);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(625, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sslVersion
            // 
            this.sslVersion.Name = "sslVersion";
            this.sslVersion.Size = new System.Drawing.Size(128, 17);
            this.sslVersion.Text = "toolStripStatusLabel1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "App Start Time:";
            // 
            // lblAppStartTime
            // 
            this.lblAppStartTime.AutoSize = true;
            this.lblAppStartTime.Location = new System.Drawing.Point(18, 86);
            this.lblAppStartTime.Name = "lblAppStartTime";
            this.lblAppStartTime.Size = new System.Drawing.Size(0, 12);
            this.lblAppStartTime.TabIndex = 17;
            // 
            // ArchiveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 432);
            this.Controls.Add(this.lblAppStartTime);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblPreWorker);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblWork2);
            this.Controls.Add(this.lblWork1);
            this.Controls.Add(this.lblMainWork);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPreArchive);
            this.Controls.Add(this.lblArchive);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ArchiveForm";
            this.Text = "新聞片庫-影片入庫排程";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer ArchiveTimer;
        private System.Windows.Forms.TextBox textBox1;
        private System.ComponentModel.BackgroundWorker FTPWorkerMain;
        private System.ComponentModel.BackgroundWorker FTPWorker1;
        private System.ComponentModel.BackgroundWorker FTPWorker2;
        private System.Windows.Forms.Timer PreArchiveTimer;
        private System.ComponentModel.BackgroundWorker PreArchiveWorker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblArchive;
        private System.Windows.Forms.Label lblPreArchive;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMainWork;
        private System.Windows.Forms.Label lblWork1;
        private System.Windows.Forms.Label lblWork2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblPreWorker;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel sslVersion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblAppStartTime;
    }
}