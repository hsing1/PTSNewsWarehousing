﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTS.News.Utilities
{
    public class NetHelper
    {
        /// <summary>
        /// 連線網路主機
        /// </summary>
        /// <param name="host">主機[IP][Host Name]</param>
        /// <param name="domain_uid">網域使用者</param>
        /// <param name="pwd">密碼</param>
        /// <returns>成功或失敗</returns>
        public static bool NetUse(string host, string domain_uid, string pwd)
        {
            Process pr = new Process();
            pr.StartInfo.FileName = "net";
            pr.StartInfo.Arguments = @"use " + host + " /user:" + domain_uid + " " + pwd;
            pr.StartInfo.UseShellExecute = false;
            //pr.StartInfo.RedirectStandardInput = true;
            //pr.StartInfo.RedirectStandardOutput = true;
            pr.StartInfo.RedirectStandardError = true;
            pr.StartInfo.CreateNoWindow = false;
            pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            pr.Start();

            pr.WaitForExit(3000);
            if (!pr.HasExited)
            {
                pr.Kill();
            }


            if (pr.StandardError.ReadToEnd().ToString() == "")
            { return true; }
            else
            { return false; }
        }
    }
}
