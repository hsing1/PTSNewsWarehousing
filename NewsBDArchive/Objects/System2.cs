﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("system")]
    public class System2
    {
        public string systemId { get; set; }
        public string systemName { get; set; }
        public string systemKind { get; set; }			// L10/L30/...
        public string systemStatus { get; set; }
        public string numOfDrives { get; set; }
        public string numOfIESlots { get; set; }
        public string numOfStorageSlots { get; set; }
        public string errorMessage { get; set; }
        public string host { get; set; }				// Library Host
        public string shutdownRequest { get; set; }
        public string wakeupRequest { get; set; }
        public string shutdownAndStartStatus { get; set; }
        public string shutdownAndStartEnable { get; set; }	// "enable-terminate"/"disable-terminate"/"enable-start"/"disable-start"
        public string cancelEnableForShutdown { get; set; }	// "enable"/"disable", if canceling of shutdown is acceptable, set to "enable"
        public string totalRunningJobs { get; set; }
        public string totalStandbyAndProcessingJobs { get; set; }

        public System2() { }

        public System2(string name, string type, string status, string message)
        {
            this.systemName = name;
            this.systemKind = type;
            this.systemStatus = status;
            this.errorMessage = message;
        }


    }
}
