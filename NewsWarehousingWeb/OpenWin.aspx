﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenWin.aspx.cs" Inherits="NewsWarehousingWeb.OpenWin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="ItemValue,ItemName" OnRowDataBound="GridView1_RowDataBound">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <input id="btnSelect" runat="server" type="button" value="選取" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="分類號">
                    <ItemTemplate>
                        <asp:Label ID="ItemValue" runat="server" Text='<%# Bind("ItemValue") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="名稱">
                    <ItemTemplate>
                        <asp:Label ID="ItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:HiddenField runat="server" ID="ItemValue"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="ItemName"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="Class"></asp:HiddenField>
    </form>
</body>
</html>

<script language="javascript">
    function senddata(strSn, strNM,Class)
    {
        window.opener.document.getElementById(Class).value = strSn;
        window.opener.document.getElementById(Class + 'Name').value = strNM;
        window.close();
    }
</script>
