﻿namespace NewsUploadApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SearchFolderWorker = new System.ComponentModel.BackgroundWorker();
            this.SearchFolderTimer = new System.Windows.Forms.Timer(this.components);
            this.PreConvertWorker = new System.ComponentModel.BackgroundWorker();
            this.PreConvertTimer = new System.Windows.Forms.Timer(this.components);
            this.ToConvertWorker = new System.ComponentModel.BackgroundWorker();
            this.ToConvertTimer = new System.Windows.Forms.Timer(this.components);
            this.ToWarehouseWorker = new System.ComponentModel.BackgroundWorker();
            this.ToWarehouseTimer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTime1 = new System.Windows.Forms.Label();
            this.labelTime2 = new System.Windows.Forms.Label();
            this.labelTime3 = new System.Windows.Forms.Label();
            this.labelTime4 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelRun1 = new System.Windows.Forms.Label();
            this.labelRun2 = new System.Windows.Forms.Label();
            this.labelRun3 = new System.Windows.Forms.Label();
            this.labelRun4 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sslVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 158);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(420, 265);
            this.textBox1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(454, 158);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "SearchFloder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(454, 206);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "MoveToPreConvert";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(454, 254);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(115, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "MoveToConvert";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(454, 302);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(115, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "MoveToWarehouse";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // SearchFolderWorker
            // 
            this.SearchFolderWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SearchFolderWorker_DoWork);
            this.SearchFolderWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SearchFolderWorker_RunWorkerCompleted);
            // 
            // SearchFolderTimer
            // 
            this.SearchFolderTimer.Enabled = true;
            this.SearchFolderTimer.Interval = 50000;
            this.SearchFolderTimer.Tick += new System.EventHandler(this.SearchFolderTimer_Tick);
            // 
            // PreConvertWorker
            // 
            this.PreConvertWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.PreConvertWorker_DoWork);
            this.PreConvertWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.PreConvertWorker_RunWorkerCompleted);
            // 
            // PreConvertTimer
            // 
            this.PreConvertTimer.Enabled = true;
            this.PreConvertTimer.Interval = 55000;
            this.PreConvertTimer.Tick += new System.EventHandler(this.PreConvertTimer_Tick);
            // 
            // ToConvertWorker
            // 
            this.ToConvertWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ToConvertWorker_DoWork);
            this.ToConvertWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ToConvertWorker_RunWorkerCompleted);
            // 
            // ToConvertTimer
            // 
            this.ToConvertTimer.Enabled = true;
            this.ToConvertTimer.Interval = 60000;
            this.ToConvertTimer.Tick += new System.EventHandler(this.ToConvertTimer_Tick);
            // 
            // ToWarehouseWorker
            // 
            this.ToWarehouseWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ToWarehouseWorker_DoWork);
            this.ToWarehouseWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ToWarehouseWorker_RunWorkerCompleted);
            // 
            // ToWarehouseTimer
            // 
            this.ToWarehouseTimer.Enabled = true;
            this.ToWarehouseTimer.Interval = 65000;
            this.ToWarehouseTimer.Tick += new System.EventHandler(this.ToWarehouseTimer_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "SearchFolderTimer :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "PreConvertTimer :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "ToConvertTimer :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "ToWarehouseTimer :";
            // 
            // labelTime1
            // 
            this.labelTime1.AutoSize = true;
            this.labelTime1.Location = new System.Drawing.Point(155, 32);
            this.labelTime1.Name = "labelTime1";
            this.labelTime1.Size = new System.Drawing.Size(33, 12);
            this.labelTime1.TabIndex = 9;
            this.labelTime1.Text = "label5";
            // 
            // labelTime2
            // 
            this.labelTime2.AutoSize = true;
            this.labelTime2.Location = new System.Drawing.Point(155, 58);
            this.labelTime2.Name = "labelTime2";
            this.labelTime2.Size = new System.Drawing.Size(33, 12);
            this.labelTime2.TabIndex = 10;
            this.labelTime2.Text = "label5";
            // 
            // labelTime3
            // 
            this.labelTime3.AutoSize = true;
            this.labelTime3.Location = new System.Drawing.Point(155, 84);
            this.labelTime3.Name = "labelTime3";
            this.labelTime3.Size = new System.Drawing.Size(33, 12);
            this.labelTime3.TabIndex = 11;
            this.labelTime3.Text = "label5";
            // 
            // labelTime4
            // 
            this.labelTime4.AutoSize = true;
            this.labelTime4.Location = new System.Drawing.Point(155, 110);
            this.labelTime4.Name = "labelTime4";
            this.labelTime4.Size = new System.Drawing.Size(33, 12);
            this.labelTime4.TabIndex = 12;
            this.labelTime4.Text = "label5";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(608, 156);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(113, 23);
            this.button5.TabIndex = 13;
            this.button5.Text = "StopAllTimer";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "SearchFolderWorker :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(258, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "PreConvertWorker :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(260, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "ToConvertWorker :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(246, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 12);
            this.label8.TabIndex = 17;
            this.label8.Text = "ToWarehouseWorker :";
            // 
            // labelRun1
            // 
            this.labelRun1.AutoSize = true;
            this.labelRun1.Location = new System.Drawing.Point(364, 32);
            this.labelRun1.Name = "labelRun1";
            this.labelRun1.Size = new System.Drawing.Size(33, 12);
            this.labelRun1.TabIndex = 18;
            this.labelRun1.Text = "label9";
            // 
            // labelRun2
            // 
            this.labelRun2.AutoSize = true;
            this.labelRun2.Location = new System.Drawing.Point(364, 58);
            this.labelRun2.Name = "labelRun2";
            this.labelRun2.Size = new System.Drawing.Size(33, 12);
            this.labelRun2.TabIndex = 19;
            this.labelRun2.Text = "label9";
            // 
            // labelRun3
            // 
            this.labelRun3.AutoSize = true;
            this.labelRun3.Location = new System.Drawing.Point(364, 84);
            this.labelRun3.Name = "labelRun3";
            this.labelRun3.Size = new System.Drawing.Size(33, 12);
            this.labelRun3.TabIndex = 20;
            this.labelRun3.Text = "label9";
            // 
            // labelRun4
            // 
            this.labelRun4.AutoSize = true;
            this.labelRun4.Location = new System.Drawing.Point(364, 110);
            this.labelRun4.Name = "labelRun4";
            this.labelRun4.Size = new System.Drawing.Size(33, 12);
            this.labelRun4.TabIndex = 21;
            this.labelRun4.Text = "label9";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(608, 206);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(113, 23);
            this.button6.TabIndex = 22;
            this.button6.Text = "Open/Close Function";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sslVersion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 433);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(758, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 23;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sslVersion
            // 
            this.sslVersion.Name = "sslVersion";
            this.sslVersion.Size = new System.Drawing.Size(128, 17);
            this.sslVersion.Text = "toolStripStatusLabel1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 455);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.labelRun4);
            this.Controls.Add(this.labelRun3);
            this.Controls.Add(this.labelRun2);
            this.Controls.Add(this.labelRun1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.labelTime4);
            this.Controls.Add(this.labelTime3);
            this.Controls.Add(this.labelTime2);
            this.Controls.Add(this.labelTime1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "新聞片庫-手動上傳排程";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.ComponentModel.BackgroundWorker SearchFolderWorker;
        private System.Windows.Forms.Timer SearchFolderTimer;
        private System.ComponentModel.BackgroundWorker PreConvertWorker;
        private System.Windows.Forms.Timer PreConvertTimer;
        private System.ComponentModel.BackgroundWorker ToConvertWorker;
        private System.Windows.Forms.Timer ToConvertTimer;
        private System.ComponentModel.BackgroundWorker ToWarehouseWorker;
        private System.Windows.Forms.Timer ToWarehouseTimer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelTime1;
        private System.Windows.Forms.Label labelTime2;
        private System.Windows.Forms.Label labelTime3;
        private System.Windows.Forms.Label labelTime4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelRun1;
        private System.Windows.Forms.Label labelRun2;
        private System.Windows.Forms.Label labelRun3;
        private System.Windows.Forms.Label labelRun4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel sslVersion;
    }
}