﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsWarehousingApp
{
    public class MatchItem
    {
        public string MXFName { get; set; }
        public string MP4Name { get; set; }
    }

    public class GVDB
    {
        private string _connectString = "Data Source=10.232.1.30;Initial Catalog=MediaFrame;User ID=NewsWarehousing;Password=Warehousing##870701";

        private SqlConnection _conn;
        private SqlCommand _cmd;

        public GVDB()
        {
            _conn = new SqlConnection(_connectString);
            _cmd = new SqlCommand
            {
                Connection = _conn
            };
        }
        
        public MatchItem Select(string slogan, string device)
        {
            MatchItem res = null;

            var sql = $@"SELECT TOP 1 URIAssetID, URN
                           FROM URN2URI
                          WHERE URIDevice = N'{device}'
                            AND URILocation = N'V:/ONAIR'
                            AND URIAssetID LIKE N'{slogan}_0%'
                          ORDER BY CreationDate DESC";

            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.CommandText = sql;
            _cmd.Connection.Open();
            SqlDataReader reader = _cmd.ExecuteReader();

            try
            {
                if (reader.HasRows)
                {
                    res = new MatchItem();

                    while (reader.Read())
                    {
                        res.MXFName = reader[0].ToString();
                        res.MP4Name = reader[1].ToString().Replace("-", "");
                    }
                }
            }
            finally
            {
                
                reader.Close();
                _cmd.Connection.Close();
            }


            return res;
        }

        public void Close()
        {
            _cmd.Dispose();
            _conn.Dispose();
        }
    }
}
