﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    class BorrowProcess
    {
        private vwBorrowList _item;

        public BorrowProcess(vwBorrowList item)
        {
            _item = item;
        }

        public void Run()
        {
            var path = "";

            PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities();

            var info = DB.BorrowList.Find(_item.BorrowID);

            var borrowPath = DB.SysReference
                               .Where(x => x.ItemType == "150")
                               .Select(x => x)
                               .ToDictionary(x => x.ItemValue, x => x.ItemName);

            try
            {
                
                if (_item.BorrowType == "A")
                {
                    path = Path.Combine(borrowPath[_item.BorrowType], _item.UserID);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, DateTime.Now.ToString("yyyyMMdd"));

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                }
                else
                {
                    path = borrowPath[_item.BorrowType];

                }



                info.BorrowStatus = 2;

                DB.SaveChanges();
                

                if (!System.IO.File.Exists(Path.Combine(path, _item.Slogan + ".mxf")))
                {
                    System.IO.File.Copy(Path.Combine(_item.ArchivePath, _item.ArchiveName), Path.Combine(path, _item.Slogan + ".mxf"), true);

                    info.BorrowStatus = 3;
                    info.FilePosition = path;
                }
                else
                {
                    info.BorrowStatus = 88;
                }

                DB.SaveChanges();

            }
            catch (Exception ex)
            {   

                DB.ExceptionLog.Add(new ExceptionLog() { MethodName = "BorrowWorker_DoWork", ExceptionMessage = ex.ToString(), FullFileName = _item.ArchiveName, CreateDate = DateTime.Now });

                info.BorrowStatus = 99;

                DB.SaveChanges();
            }
            finally
            {
                DB.Dispose();
            }
        }

    }
}
