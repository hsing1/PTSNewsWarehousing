﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UserDTL.aspx.cs" Inherits="NewsWarehousingWeb.FUN.UserDTL" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" class="tblBase">
        <tr>
            <td class="tdHeader-Green">
                <asp:Label ID="lblTitle" runat="server" Text="" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdHeader-Blue" style="text-align:left;">
                <asp:Button ID="btnNew" runat="server" Text="新增" OnClick="btnNew_Click" OnClientClick="return CheckFields();" />&nbsp;
                <asp:Button ID="btnModify" runat="server" Text="修改" OnClick="btnModify_Click" />&nbsp;
                <input id="Button1" type="button" value="回上頁" onclick="location.href = 'http://iis2012.pts.org.tw/NewsWarehousing/FUN/User.aspx';"; />
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="tdHeader-Blue" width="15%">
                            員編
                        </td>
                        <td class="tdValue-Blue" width="15%">
                            <asp:PTSTextBox ID="txtEmpID" runat="server" DBColumn="EmpID"></asp:PTSTextBox>
                            <asp:ImageButton ID="btnOption" runat="server" ImageUrl="~/App_Images/options.png"
                                OnClientClick="OpenPopupDialog1(); return false;" />
                        </td>
                        <td class="tdHeader-Blue" width="15%">
                            帳號
                        </td>
                        <td class="tdValue-Blue" width="15%">
                            <asp:PTSTextBox ID="txtDomainID" runat="server" DBColumn="DomainID" ModifyTag="N"></asp:PTSTextBox>
                        </td>
                        <td class="tdHeader-Blue" width="15%">
                            姓名
                        </td>
                        <td class="tdValue-Blue">
                            <asp:PTSTextBox ID="txtEmpName" runat="server" DBColumn="EmpName" ModifyTag="N"></asp:PTSTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="tdHeader-Blue" width="15%">
                            部門
                        </td>
                        <td class="tdValue-Blue" width="85%">
                            <asp:PTSTextBox ID="txtUnitFullName" runat="server" DBColumn="UnitFullName" ModifyTag="N" Width="300px"></asp:PTSTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="tdHeader-Blue" width="15%">
                            權限
                        </td>
                        <td class="tdValue-Blue" width="85%">
                            <table>
                                <tr>
                                    <td style="width:20%">
                                        <asp:PTSCheckBox ID="cbIsEditor" runat="server" Text="編輯權限" DBColumn="IsEditor" SelectedValue="1" UnSelectedValue="0" />
                                    </td>
                                    <td style="width:20%">
                                        <asp:PTSCheckBox ID="cbIsApprover" runat="server" Text="審核權限" DBColumn="IsApprover" SelectedValue="1" UnSelectedValue="0" />
                                    </td>
                                    <td style="width:20%">
                                        <asp:PTSCheckBox ID="cbIsPreArchive" runat="server" Text="入庫權限" DBColumn="IsPreArchive" SelectedValue="1" UnSelectedValue="0" />
                                    </td>
                                    <td style="width:20%">
                                        <asp:PTSCheckBox ID="cbOverallOut" runat="server" Text="最高權限" DBColumn="IsAdmin" SelectedValue="1" UnSelectedValue="0" />
                                    </td>
                                    <td style="width:20%">
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtReturnValue" runat="server" AutoPostBack="True" Style="display: none"
        OnTextChanged="txtReturnValue_TextChanged"></asp:TextBox>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $('#<% =txtEmpID.ClientID %>').prop("disabled", true);
            $('#<% =txtDomainID.ClientID %>').prop("disabled", true);
            $('#<% =txtEmpName.ClientID %>').prop("disabled", true);
            $('#<% =txtUnitFullName.ClientID %>').prop("disabled", true);
        });

        function CheckFields() {
            var strMsg = "";

            if ($.trim($('#<% =txtDomainID.ClientID %>').val()) == "")
                strMsg += "請選擇一位員工!\n";

            if (strMsg == "") {
                if ($.trim($('#<% =txtEmpName.ClientID %>').val()) == "")
                    strMsg += "請選擇一位員工!\n";
            }


            if (strMsg != "") {
                alert(strMsg);
                return false;
            } else {
                CallBlockUI();
                return true;
            }
        }

        function OpenPopupDialog1() {

            var strUrl = 'UserSchFrm.aspx?ObjectName1=' + '<%=this.txtReturnValue.ClientID %>';

            var win_width = 550;
            var win_height = 520;

            var win = window.open(strUrl,
			'SearchForm',
			'toolbar=no,width=' + win_width + ',height=' + win_height + ',top=' + (((screen.height - win_height) / 2) - 150) + ',left=' + (screen.width - win_width) / 2 + ',directories=no,status=no,scrollbars=yes,resizable=1,menubar=no')

            win.focus();
        }
    </script>
</asp:Content>
