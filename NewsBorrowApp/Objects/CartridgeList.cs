﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("cartridges")]
    public class CartridgeList
    {
        public CartridgeList()
        {
        }

        public string totalCount { get; set; }
        public string count { get; set; }
        [System.Xml.Serialization.XmlElement("cartridge")]
        public List<Cartridge> cartridges { get; set; }
    }
}
