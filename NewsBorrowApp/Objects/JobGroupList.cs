﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("jobGroups")]
    public class JobGroupList
    {
        public string totalCount { get; set; }
        public string count { get; set; }
        [System.Xml.Serialization.XmlElement("jobGroup")]
        public List<JobGroup> jobGroups { get; set; }

        public JobGroupList() { }
    }
}
