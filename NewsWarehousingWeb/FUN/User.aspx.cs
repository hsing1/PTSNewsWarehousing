﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.DBUtility;

namespace NewsWarehousingWeb.FUN
{
    public partial class User : System.Web.UI.Page
    {
        DBOperate objDB = new DBOperate();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            SetPageData();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SetPageData();
        }

        protected void btnNewUser_Click(object sender, EventArgs e)
        {
            Session["Type"] = "New";
            Session["EmpID"] = null;

            Response.Redirect("~/FUN/UserDTL.aspx");
        }

        protected void btnModify_Command(object sender, CommandEventArgs e)
        {
            Session["Type"] = "Modify";
            Session["EmpID"] = e.CommandArgument.ToString();

            Response.Redirect("~/FUN/UserDTL.aspx");
        }

        void SetPageData()
        {
            var SQL = @"SELECT EmpID
                              ,EmpName
                              ,DomainID
                              ,UnitFullName
                              ,CASE IsEditor WHEN 1 THEN 'Y' ELSE 'N' END IsEditor
							  ,CASE IsApprover WHEN 1 THEN 'Y' ELSE 'N' END IsApprover
                              ,CASE IsPreArchive WHEN 1 THEN 'Y' ELSE 'N' END IsPreArchive
							  ,CASE IsAdmin WHEN 1 THEN 'Y' ELSE 'N' END IsAdmin
                          FROM Users
                         WHERE 1 = 1 ";

            var dic = new Dictionary<string, string>();

            if (txtKeyWord.Text.Trim().Length > 0)
            {
                SQL += " AND (EmpID LIKE @Keyword OR EmpName LIKE @Keyword OR DomainID LIKE @Keyword)";
                dic.Add("Keyword", "%" + txtKeyWord.Text.Trim() + "%");
            }

            this.gvResults.DataSource = objDB.DBGetTable(SQL, dic);
            this.gvResults.DataBind();
        }


    }
}