﻿<%@ Page Language="C#"   MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="mainfrm.aspx.cs" Inherits="NewsWarehousingWeb.mainfrm" %>
<%@ Register Assembly="PTS.Controls" Namespace="PTS.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        article, aside, figure, figcaption, footer, header, hgroup, menu, nav, section {
            display: block;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width:1500px;">
        <div style="float:left">
            <table class="tblBase-Blue"style="width:1050px;">
                <tr>
                    <td class="tdHeader-Blue">查詢條件</td>
                    <td class="tdValue-Blue">
                        <input id="Query" type="text"/>
                    </td>
                    <td class="tdHeader-Blue">播映日期</td>
                    <td class="tdValue-Blue">
                        <asp:TextBox ID="CalendarB" Style="width: 100px" runat="server"></asp:TextBox>
                        ~
                        <asp:TextBox ID="CalendarE" Style="width: 100px" runat="server"></asp:TextBox> &nbsp;
                        <asp:Button ID="btnQuery" runat="server" Text="搜尋" OnClick="btnQuery_Click" /></td>
                </tr>
                <tr>
                    <td colspan="4" class="tdValue-Blue">
                        <asp:GridView ID="GridView2" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" 
                            Width="1034px" AutoGenerateColumns="False" OnPageIndexChanged="GridView2_PageIndexChanged" OnPageIndexChanging="GridView2_PageIndexChanging" AllowPaging="True" OnRowCommand="GridView2_RowCommand" PageSize="20">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnPreview" runat="server" CausesValidation="False"
                                          Width="40px" CommandName="Preview" Text="預覽" CommandArgument='<%# Eval("VideoID") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Slogan" HeaderText="標題"  ItemStyle-Width="100px">
<ItemStyle Width="100px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CameraDate" HeaderText="撥映日期" ItemStyle-Width="80px">
<ItemStyle Width="80px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="長度" />
                                <asp:BoundField HeaderText="檔案類型" />
                                <asp:BoundField DataField="Body" HeaderText="內容" ItemStyle-Width="420px">
<ItemStyle Width="420px"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnBorrow" runat="server" CausesValidation="False"
                                          Width="40px" CommandName="Borrow" Text="調用" CommandArgument='<%# Eval("VideoID") %>'></asp:LinkButton><br />
                                        <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red" Font-Size="Small"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />                  
                        </asp:GridView>
                    </td>
                </tr>   
                <asp:HiddenField ID="pkIndex" runat="server" />  
            </table>
        </div>
        <div style="float: left; width: 440px">
            <table class="tblBase-Blue" style="width: 440px;">
                <tr>
                    <td class="tdHeader-Blue" style="text-align:center">入庫影像</td>
                </tr>
                <tr>
                    <td style="height:439px">
                        <asp:Literal ID="lallowVideo" runat="server"></asp:Literal>

                    </td>
                    <%--<textarea id="video" cols="51" rows="20"></textarea>--%>
                </tr>
            </table>
        </div>
        <%--        <div>
            <table class="tblBase-Blue"style="width:360px;">
                <tr>
                    <td class="tdHeader-Blue">熱門關鍵詞</td>
                    <td class="tdValue-Blue">
                        <select id="Select4" class="auto-style1" >
                          <option value="一週內">一週內</option>
                          <option value="一個月內">一個月內</option>
                          <option value="一年內">一年內</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tdValue-Blue" colspan="2" style="height:186px">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Height="110px" Width="100%">
                            <Columns>
                                <asp:BoundField HeaderText="關鍵詞"/>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>--%>
    </div>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptArea" runat="server">
    <script src="App_Js/jquery-ui.min.js"></script>
    <script>



        var myVar = setInterval(ClearMsg, 5000);

        function ClearMsg() {
            $("span[id^='ContentPlaceHolder1_GridView2_lblMsg']").each(function () {
                $(this).html('');
            });
        }

        $(document).ready(function () {
            $.datepicker.regional['zh-TW'] = {
                dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                prevText: "上月",
                nextText: "次月",
                weekHeader: "週"
            };
            $.datepicker.setDefaults($.datepicker.regional["zh-TW"]);

            $('#<% =CalendarB.ClientID %>').datepicker({
                showOn: 'both', dateFormat: "yy-mm-dd", showMonthAfterYear: true,
                showOtherMonths: true, selectOtherMonths: true,
                buttonImageOnly: true, buttonImage: "calendar.png"
            });
            $('#<% =CalendarE.ClientID %>').datepicker({
                showOn: 'both', dateFormat: "yy-mm-dd", showMonthAfterYear: true,
                showOtherMonths: true, selectOtherMonths: true,
                buttonImageOnly: true, buttonImage: "calendar.png"
            });
        });
  </script>
</asp:Content>