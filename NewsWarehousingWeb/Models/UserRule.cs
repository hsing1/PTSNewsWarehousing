﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PTS.Modules.DBUtility;
using PTS.Modules.ExtMethods;
using Newtonsoft.Json;

namespace NewsWarehousingWeb.Models
{
    public class UserRule
    {

        public class Rule
        {
            public bool IsEditor { get; set; } = false;
            public bool IsApprover { get; set; }
            public bool IsPreArchive { get; set; }
            public bool IsUploadBigger30 { get; set; }
            public bool IsUploadBigger60 { get; set; }
            public bool IsAdmin { get; set; }
        }

        public string GetUserRule(string DomainID)
        {
            Rule rule;

            var db = new DBOperate();

            var dic = new Dictionary<string, string> { { "DomainID", DomainID } };
            var sql = @"SELECT COUNT(*) FROM Users WHERE DomainID = @DomainID";

            var check = db.DBGetValue<int>(sql, dic);

            if (check == 0)
            {
                rule = new Rule();
            }
            else
            {
                sql = @"SELECT CASE IsEditor WHEN 1 THEN 'Y' ELSE 'N' END IsEditor
                              , CASE IsApprover WHEN 1 THEN 'Y' ELSE 'N' END IsApprover
                              , CASE IsPreArchive WHEN 1 THEN 'Y' ELSE 'N' END IsPreArchive
                              , CASE IsUploadBigger30 WHEN 1 THEN 'Y' ELSE 'N' END IsUploadBigger30
                              , CASE IsUploadBigger60 WHEN 1 THEN 'Y' ELSE 'N' END IsUploadBigger60
                              , CASE IsAdmin WHEN 1 THEN 'Y' ELSE 'N' END IsAdmin 
                          FROM Users WHERE DomainID = @DomainID ";



                var emp = db.DBGetTable(sql, dic).ExTableToDictionary();
                rule = new Rule() { IsEditor = emp["IsEditor"] == "Y" ? true : false
                                  , IsApprover = emp["IsApprover"] == "Y" ? true : false
                                  , IsPreArchive = emp["IsPreArchive"] == "Y" ? true : false 
                                  , IsUploadBigger30 = emp["IsUploadBigger30"] == "Y" ? true : false
                                  , IsUploadBigger60 = emp["IsUploadBigger60"] == "Y" ? true : false
                                  , IsAdmin = emp["IsAdmin"] == "Y" ? true : false };
            }

            return JsonConvert.SerializeObject(rule); 

        }
    }
}