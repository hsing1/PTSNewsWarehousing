﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NewsBDArchive
{
    class WebServiceControl
    {
        public event EventHandler<MessageEventArgs> MessageShow;

        private void OnMessageShow(string message) =>
            MessageShow?.Invoke(this, new MessageEventArgs() { Message = message });


        public WebServiceControl()
        {
        }

        /// <summary>
        /// get base path list
        /// </summary>
        /// <param name="URL">URL string to get base path list</param>
        /// <returns>base path list</returns>
        /// <remarks>
        /// </remarks>
        public BasePathList getBasePaths(string URL)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));

                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(BasePathList));
                    //deserialize xml
                    BasePathList basePaths = (BasePathList)serializer.Deserialize(reader);
                    return basePaths;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        string xmlString = sr.ReadToEnd();
                        OnMessageShow("get BasePaths NG" + ":" + xmlString);
                    }
                    else
                        OnMessageShow("get BasePaths NG");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }

        }

        private BasePath getBasePathFromXml(string xmlString)
        {
            XmlReader reader = XmlReader.Create(new StringReader(xmlString));

            //create object for XmlSerialier
            System.Xml.Serialization.XmlSerializer serializer =
                new System.Xml.Serialization.XmlSerializer(typeof(BasePath));
            //deserialize xml
            BasePath obj = (BasePath)serializer.Deserialize(reader);
            return obj;
        }

        /// <summary>
        /// add or edit base path
        /// </summary>
        /// <param name="URL">URL string to get base path</param>
        /// <param name="basePath">a base path to be added or edited</param>
        /// <param name="mode">"add" is added the base path, otherwise the base path is edited</param>
        /// <returns>added or edited base path</returns>
        /// <remarks>
        /// </remarks>
        public BasePath addAndEditBasePath(string URL, BasePath basePath, string mode)
        {

            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");

            if (mode.Equals("add"))
                webRequest.Method = "POST";
            else
                webRequest.Method = "PUT";
            webRequest.ContentType = "application/xml";
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(BasePath));
            //XmlWriter writer = XmlWriter.Create(new StringWriter());
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, basePath);

            string repString = writer.ToString().Replace("utf-16", "utf-8");
            byte[] data = Encoding.UTF8.GetBytes(repString);
            webRequest.ContentLength = data.LongLength;

            Stream reqStream = webRequest.GetRequestStream();
            reqStream.Write(data, 0, data.Length);
            reqStream.Close();


            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                Stream st = webResponse.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (xmlString != null && xmlString != "")
                    {
                        //BasePath result = getBasePathFromXml(xmlString);
                        XmlReader reader = XmlReader.Create(new StringReader(xmlString));

                        //create object for XmlSerialier
                        serializer = new System.Xml.Serialization.XmlSerializer(typeof(BasePath));
                        //deserialize xml
                        BasePath result = (BasePath)serializer.Deserialize(reader);
                        if (result != null)
                        {
                            //    OnMessageShow("added virtualPath:" + result.virtualPath + " physicalPath:" + result.physicalPath + " isFileToBeDeleted:" + result.isFileToBeDeleted);
                        }
                        return result;
                    }
                }
                else
                {
                    OnMessageShow("Error! " + xmlString);
                }
                return null;
            }
            catch (WebException e)
            {
                //OnMessageShow("e.Message:" + e.Message + " e.Response:" + e.Response.ToString());
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }

        }

        /// <summary>
        /// delete a get base path from existing base path list
        /// </summary>
        /// <param name="URL">URL string to delete base path</param>
        /// <param name="basePath">a base path to be deleted</param>
        /// <returns>deleted base path</returns>
        /// <remarks>
        /// </remarks>
        public BasePath deleteBasePath(string URL, BasePath basePath)
        {

            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "DELETE";

            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();

                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    if (xmlString != null && xmlString != "")
                    {
                        BasePath result = getBasePathFromXml(xmlString);

                        return result;
                    }
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            return null;
        }

        /// <summary>
        /// get file list under the specified parent path. the parent path includes a base path as a root directory.
        /// </summary>
        /// <param name="URL">URL string to get file list</param>
        /// <returns>file list</returns>
        /// <remarks>
        /// </remarks>
        public FileList getFiles(string URL, string parentPath)
        {
            string parentPath2 = System.Web.HttpUtility.UrlEncode(parentPath, Encoding.GetEncoding("UTF-8")); //ISO-8859-1,UTF-8,SJIS
            //string parentPath2 = System.Web.HttpUtility.HtmlEncode(parentPath);
            //byte[] data = Encoding.UTF8.GetBytes(parentPath);
            //string text = System.Text.Encoding.UTF8.GetString(data);
            //string parentPath2 = Uri.EscapeUriString(parentPath);
            //parentPath2 = Uri.EscapeUriString(parentPath2);
            string requestURL = URL.Replace("files?", "files?parentPath=" + parentPath2 + "&");
            //requestURL = System.Web.HttpUtility.UrlEncode(requestURL, Encoding.GetEncoding("UTF-8"));
            //requestURL = System.Web.HttpUtility.UrlEncode(requestURL, Encoding.UTF8);
            //Uri uri = new Uri(requestURL);
            var webRequest = (HttpWebRequest)WebRequest.Create(requestURL);


            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "GET";
            //webRequest.ContentType = "text/x-www-form-urlencoded";
            //webRequest.ContentType = "text/xml";

            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(FileList));
                    //deserialize xml
                    FileList fileList = (FileList)serializer.Deserialize(reader);
                    return fileList;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get cartridge list under location.
        /// </summary>
        /// <param name="URL">URL string to get cartridge list</param>
        /// <param name="location">Drive, IE Slot,  (Storage Slot), System(=Drive|IE Slot|Storage Slot) , Shelf. Storage Slot does not exist in ODS-L10 </param>
        /// <returns>cartridge list</returns>
        /// <remarks>
        /// </remarks>
        public CartridgeList getCartriges(string URL, string location)
        {
            string requestURI = URL;
            if (location != "")
                requestURI = URL.Replace("cartridges?", "cartridges?location=" + location + "&");

            var webRequest = (HttpWebRequest)WebRequest.Create(requestURI);
            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(CartridgeList));
                    //deserialize xml
                    CartridgeList list = (CartridgeList)serializer.Deserialize(reader);
                    return list;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get cartridge information wich cartridgeId.
        /// </summary>
        /// <param name="URL">URL string to get cartridge information</param>
        /// <param name="cartridgeId">cartridge ID</param>
        /// <returns>cartridge information</returns>
        /// <remarks>
        /// </remarks>
        public Cartridge getCartrige(string URL, string cartridgeId)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL.Replace("cartridges?", "cartridges/" + cartridgeId + "?"));
            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(Cartridge));
                    //deserialize xml
                    Cartridge cartridge = (Cartridge)serializer.Deserialize(reader);
                    return cartridge;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get catalog list with cartridgeId and catalogId.
        /// </summary>
        /// <param name="URL">URL string to get catalog list information</param>
        /// <param name="cartridgeId">cartridge ID</param>
        /// <param name="cartridgeId">catalog ID</param>
        /// <returns>catalog list</returns>
        /// <remarks>
        /// </remarks>
        public CatalogList getCatalogs(string URL, string cartridgeId, string catalogId)
        {
            //var webRequest = null;
            string requestURL = "";
            if (catalogId == "")
                requestURL = URL.Replace("cartridgeId=", "cartridgeId=" + cartridgeId);
            else
            {
                requestURL = URL.Replace("cartridgeId=", "cartridgeId=" + cartridgeId);
                requestURL = requestURL.Replace("parentId=", "parentId=" + catalogId);
            }

            var webRequest = (HttpWebRequest)WebRequest.Create(requestURL);


            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(CatalogList));
                    //deserialize xml
                    CatalogList list = (CatalogList)serializer.Deserialize(reader);
                    return list;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get catalog with catalogId.
        /// </summary>
        /// <param name="URL">URL string to get catalog information. catalog is a archived file in a cartridge</param>
        /// <param name="catalogId">catalog ID</param>
        /// 
        /// <returns>catalog detail. catalog detail contains metada information</returns>
        /// <remarks>
        /// </remarks>
        public CatalogDetails getCatalog(string URL, string catalogId)
        {
            //var webRequest = null;
            string requestURL = URL.Replace("catalogs", "catalogs/" + catalogId);
            var webRequest = (HttpWebRequest)WebRequest.Create(requestURL);

            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(CatalogDetails));
                    //deserialize xml
                    CatalogDetails catalog = (CatalogDetails)serializer.Deserialize(reader);
                    return catalog;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// archive files in a cartridge
        /// </summary>
        /// <param name="URL">URL string to archive files</param>
        /// <param name="request">request information for archive</param>
        /// 
        /// <returns>catalog list</returns>
        /// <remarks>
        /// </remarks>
        public CatalogList addCatalogs(string URL, CatalogAddRequest request)
        {

            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "POST";
            webRequest.ContentType = "application/xml";
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(CatalogAddRequest));
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, request);

            string repString = writer.ToString().Replace("utf-16", "utf-8");
            byte[] data = Encoding.UTF8.GetBytes(repString);
            webRequest.ContentLength = data.LongLength;

            Stream reqStream = webRequest.GetRequestStream();
            reqStream.Write(data, 0, data.Length);
            reqStream.Close();


            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CatalogList));
                    //deserialize xml
                    CatalogList catalogList = (CatalogList)serializer.Deserialize(reader);
                    return catalogList;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }

        }


        /// <summary>
        /// retrieve catalog files in a cartridge 
        /// </summary>
        /// <param name="URL">URL string to retrieve files</param>
        /// <param name="request">request information for retrieve</param>
        /// 
        /// <returns>file list</returns>
        /// <remarks>
        /// </remarks>
        public FileList addFiles(string URL, FileAddRequest request)
        {

            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "POST";
            webRequest.ContentType = "application/xml";
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(FileAddRequest));
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, request);

            string repString = writer.ToString().Replace("utf-16", "utf-8");
            byte[] data = Encoding.UTF8.GetBytes(repString);
            webRequest.ContentLength = data.LongLength;

            Stream reqStream = webRequest.GetRequestStream();
            reqStream.Write(data, 0, data.Length);
            reqStream.Close();


            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(FileList));
                    //deserialize xml
                    FileList fileList = (FileList)serializer.Deserialize(reader);
                    return fileList;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }

        }

        /// <summary>
        /// get job groups
        /// </summary>
        /// <param name="URL">URL string to get job groups</param>
        /// 
        /// <returns>job groups list</returns>
        /// <remarks>
        /// </remarks>
        public JobGroupList getJobGroups(string URL)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");
            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(JobGroupList));
                    //deserialize xml
                    JobGroupList list = (JobGroupList)serializer.Deserialize(reader);
                    return list;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get jobs
        /// </summary>
        /// <param name="URL">URL string to get jobs</param>
        /// 
        /// <returns>job list</returns>
        /// <remarks>
        /// </remarks>
        public JobList getJobs(string URL)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");
            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(JobList));
                    //deserialize xml
                    JobList list = (JobList)serializer.Deserialize(reader);
                    return list;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get a job
        /// </summary>
        /// <param name="URL">URL string to get a job</param>
        /// <param name="jobId">job id</param>
        /// <returns>job</returns>
        /// <remarks>
        /// </remarks>
        public Job getJob(string URL, string jobId)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL.Replace("jobs", "jobs/" + jobId));
            Encoding enc = Encoding.GetEncoding("UTF-8");
            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(Job));
                    //deserialize xml
                    Job list = (Job)serializer.Deserialize(reader);
                    return list;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get I/E tray information
        /// </summary>
        /// <param name="URL">URL string to get I/E tray</param>
        /// 
        /// <returns>I/E tray information</returns>
        /// <remarks>
        /// </remarks>
        public Tray getTray(string URL)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");
            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(Tray));
                    //deserialize xml
                    Tray result = (Tray)serializer.Deserialize(reader);
                    return result;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// open/close I/E tray
        /// </summary>
        /// <param name="URL">URL string to open/close I/E tray</param>
        /// <param name="request">request information to open/close I/E tray</param>
        /// <returns>updated I/E tray information</returns>
        /// <remarks>
        /// </remarks>
        public Tray updateTray(string URL, TrayRequest request)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");

            webRequest.Method = "PUT";
            webRequest.ContentType = "application/xml";
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(TrayRequest));
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, request);

            string repString = writer.ToString().Replace("utf-16", "utf-8");
            byte[] data = Encoding.UTF8.GetBytes(repString);
            webRequest.ContentLength = data.LongLength;

            Stream reqStream = webRequest.GetRequestStream();
            reqStream.Write(data, 0, data.Length);
            reqStream.Close();

            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(Tray));
                    //deserialize xml
                    Tray result = (Tray)serializer.Deserialize(reader);
                    return result;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }

        }

        /// <summary>
        /// get drives information
        /// </summary>
        /// <param name="URL">URL string to get drives</param>
        /// 
        /// <returns>drive list</returns>
        /// <remarks>
        /// </remarks>
        public DriveList getDrives(string URL)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");
            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(DriveList));
                    //deserialize xml
                    DriveList list = (DriveList)serializer.Deserialize(reader);
                    return list;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// get system list. There must be one system in case of ODS-L10. this API is provided for extended system environment in future
        /// </summary>
        /// <param name="URL">URL string to get systems</param>
        /// 
        /// <returns>system list</returns>
        /// <remarks>
        /// </remarks>
        public SystemList getSystems(string URL)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Encoding enc = Encoding.GetEncoding("UTF-8");
            webRequest.Method = "GET";
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    st.Close();
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    //create object for XmlSerialier
                    System.Xml.Serialization.XmlSerializer serializer =
                        new System.Xml.Serialization.XmlSerializer(typeof(SystemList));
                    //deserialize xml
                    SystemList list = (SystemList)serializer.Deserialize(reader);
                    return list;
                }
                else
                {
                    Stream st = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(st, enc);
                    if (sr != null)
                    {
                        OnMessageShow("get method Error" + ":" + sr.ReadToEnd());
                    }
                    else
                        OnMessageShow("get method Error");
                    sr.Close();
                    st.Close();
                    return null;
                }
            }
            catch (WebException e)
            {
                WebResponse res = e.Response;
                Stream st = res.GetResponseStream();
                StreamReader sr = new StreamReader(st, enc);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                st.Close();
                OnMessageShow("Error! " + xmlString);

                return null;
            }
            catch (InvalidOperationException e)
            {
                OnMessageShow(e.Message + "\n" + e.InnerException.Message + "\n" + e.StackTrace);
                return null;
            }
        }
    }
}
