﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PTS.Modules.DBUtility;
using PTS.Modules.Employee;

namespace NewsWarehousingWeb.SRV
{
    public class Borrow : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var message = "";

            int id = 0;
            string type = "";

            if (context.Request.Form["id"] == null || context.Request.Form["type"] == null)
            {
                message = "參數不正確!";
            }
            else
            {
                if (int.TryParse(context.Request.Form["id"], out id))
                {
                    type = context.Request.Form["type"];

                    var objEmp = new EmployeeInfo();

                    var db = new DBOperate();

                    var i = db.DBGetValue<int>("SELECT COUNT(*) FROM BorrowList WHERE BorrowStatus = 0 AND VideoID = @VideoID AND BorrowType = @BorrowType AND UserID = @UserID  "
                                              , new Dictionary<string, string> { { "VideoID", id.ToString() }, { "BorrowType", type }, { "UserID", objEmp.EmployeeID } });


                    if (i > 0)
                    {
                        message = "已在排程中";
                    }
                    else
                    {

                        db.DBInsert(new Dictionary<string, string> { { "VideoID", id.ToString() }
                                                                   , { "UserID", objEmp.EmployeeID }
                                                                   , { "BorrowType", type } }, "BorrowList");

                        message = "加入排程";
                    }
                }
                else
                {
                    message = "影片編號不正確!";
                }

            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(message);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}