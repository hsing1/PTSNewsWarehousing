﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PTS.Modules.DBUtility;

namespace NewsWarehousingWeb
{
    public partial class Search : System.Web.UI.Page
    {
        private List<string> _keys = new List<string>();
        private DBOperate GVDB = new DBOperate("MediaFrame");
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            SetPageData();

        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            SetPageData();
        }

        void SetPageData()
        {
            DBOperate db = new DBOperate();

            var dic = new Dictionary<string, string>();
            var sql = @"SELECT TOP 500 NewsMeta.Slogan,
                               CameraDate,
	                           (case when len(Body) > 180 then SUBSTRING(Body,0,180)else Body end ) Body,
                               ISNULL(SC.ItemName, '未定') Copyright,
                               ISNULL(VideoMeta.Description, '') Description,
                               VideoMeta.VideoID,
                               VideoMeta.MP4Name,
                               NewsMeta.NewsID,
                               VideoMeta.Duration
                          FROM NewsMeta
                               JOIN VideoMeta ON NewsMeta.NewsID = VideoMeta.NewsID
                               LEFT OUTER JOIN SysReference SC ON SC.ItemType = '507' AND VideoMeta.Copyright = SC.ItemValue
                         WHERE 1 = 1 
                           AND FileStstus = 2
                           AND DeleteMark = 0 ";

            if (this.CalendarB.Text != "")
            {
                if (this.CalendarE.Text != "")
                {
                    sql += "  AND NewsMeta.NewsDate BETWEEN @CalendarB and @CalendarE ";
                    dic.Add("CalendarB", CalendarB.Text);
                    dic.Add("CalendarE", CalendarE.Text);
                }
                else
                {
                    sql += "  AND NewsMeta.NewsDate >= @CalendarB ";
                    dic.Add("CalendarB", CalendarB.Text);
                }
            }

            if (this.txtKeyWord.Text != "")
            {
                int i = 0;
                foreach (var key in txtKeyWord.Text.Split('+'))
                {
                    sql += "  AND CONTAINS((Slogan, Body), @KeyWord" + i.ToString() + ")   ";
                    dic.Add("KeyWord" + i.ToString(), key.Trim());
                    i++;
                }
                
            }
            sql += " ORDER BY NewsMeta.NewsID DESC ";


            if (!string.IsNullOrEmpty(txtKeyWord.Text.Trim()))
            {
                foreach (var key in txtKeyWord.Text.Split('+'))
                {
                    _keys.Add(key.Trim());
                }
            }

            gvResult.DataSource = db.DBGetTable(sql, dic);
            gvResult.DataBind();
            
        }

        protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.DataRow)
            {

                var id = DataBinder.Eval(e.Row.DataItem, "VideoID").ToString();
                var slogan = DataBinder.Eval(e.Row.DataItem, "Slogan").ToString();
                var body = DataBinder.Eval(e.Row.DataItem, "Body").ToString();


                if (e.Row.FindControl("LiteralA") is Literal la)
                {
                    la.Text = $"<a href=\"javascript: toBorrow('{id}', 'A')\">調用</a>";
                }

                if (e.Row.FindControl("LiteralB") is Literal lb)
                {
                    //GV資料庫的設定問題只能這樣下 
                    var i = GVDB.DBGetValue<int>($"SELECT COUNT(*) FROM URN2URI WHERE URILocation = N'V:/公視數位片庫' AND URIAssetID = N'{slogan}'", 0);

                    if(i == 0)
                    {
                        lb.Text = $"<a href=\"javascript: toBorrow('{id}', 'B')\">ToGV</a>";
                    }
                    else
                    {
                        lb.Text = $"已存在GV系統中";
                    }
                }

                if (e.Row.FindControl("lalCopyright") is Literal c)
                {
                    var copyright = DataBinder.Eval(e.Row.DataItem, "Copyright").ToString();
                    var description = DataBinder.Eval(e.Row.DataItem, "Description").ToString();

                    c.Text = copyright + (string.IsNullOrEmpty(description) ? "" : $"({description})");
                }

                if (e.Row.FindControl("lalMsg") is Literal m)
                {
                    m.Text = $"<span id=\"Msg_{id}\" style=\"color:Red;font-size:Small;\"><span>";
                }
                
                if (e.Row.FindControl("lalSlogan") is Literal s)
                {
                    s.Text = MarkKeyword(_keys, slogan);
                }
                
                if (e.Row.FindControl("lalBody") is Literal b)
                {
                    b.Text = MarkKeyword(_keys, body); ;
                }

                e.Row.Attributes.Add("id", "row_" + id);

            }
        }

        protected void gvResult_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvResult.PageIndex = e.NewPageIndex;
            SetPageData();
        }

        string MarkKeyword(List<string> Keywords, string content)
        {
            //將符合關鍵字的文字標成紅色
            var tmp = content;

            foreach (var key in Keywords)
            {
                var p = 0;

                while (tmp.IndexOf(key, p) > -1)
                {
                    p = tmp.IndexOf(key, p);

                    tmp = tmp.Insert(p, "<span style=\"color:Red\">");
                    p = p + key.Length + 24;
                    tmp = tmp.Insert(p, "</span>");
                    p = p + 7;
                }
            }

            return tmp;
        }
    }
}