﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBorrowApp
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("catalog")]
    public class Catalog
    {
        public Catalog()
        {
        }

        public string id { get; set; }
        public string cartridgeId { get; set; }
        public string cartridgeTitle { get; set; }
        public string cartridgeLocation { get; set; }
        public string name { get; set; }
        public string fullName { get; set; }
        public string parentId { get; set; }
        public string archiveRequestTime { get; set; }
        public string archiveStartTime { get; set; }
        public string archiveEndTime { get; set; }
        public string archiveFileUpdateTime { get; set; }
        public string originalFileName { get; set; }
        public string originalFileType { get; set; }
        public string originalFileUpdateTime { get; set; }
        public string status { get; set; }
        public string retrieveCounter { get; set; }
        public string lastRetrieveTime { get; set; }
        public string groupsId { get; set; }
        public string isDir { get; set; }
        public string fileSize { get; set; }
        public string startDiskNumber { get; set; }
        public string endDiskNumber { get; set; }
        public string hasMetadata { get; set; }
        public string jobId { get; set; }

    }
}
