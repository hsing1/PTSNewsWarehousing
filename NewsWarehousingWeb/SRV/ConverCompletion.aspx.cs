﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using NewsWarehousingWeb.Models;

namespace NewsWarehousingWeb.SRV
{
    public partial class ConverCompletion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            WriteLog(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            string JobGuid = "", JobName = "", Source = "", ArchiveName = "";

            if (Request.QueryString["Jobguid"] != null)
            {
                JobGuid = Request.QueryString["Jobguid"];
            }
            if (Request.QueryString["Jobname"] != null)
            {
                JobName = Request.QueryString["Jobname"];
            }
            if (Request.QueryString["Source"] != null)
            {
                Source = Request.QueryString["Source"];
            }

            WriteLog(Request.QueryString.ToString());

            Source = HttpUtility.UrlDecode(Source);

            ArchiveName = Source.Split('\\').Last();

            WriteLog($"{JobGuid} ; {JobName} ; {Source} ; {ArchiveName}");
            
            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                var item = DB.UploadTemp
                             .Where(x => x.ArchiveName == ArchiveName)
                             .Select(x => x)
                             .FirstOrDefault();

                //var code = DB.ConvertCode
                //             .Where(x => x.ConvertFolder == item.ConvertFolder)
                //             .Select(x => x)
                //             .FirstOrDefault();

                item.JobGuid = JobGuid;
                item.JobName = JobName;
                item.Source = Source;
                item.ConverterStatus = 4;
                
                //code.IsRunning = false;
                
                DB.SaveChanges();
            }
            
        }

        private void WriteLog(string strLine)
        {

            string strFileName = Server.MapPath("~/TempFiles/CompletionLog.txt");

            StreamWriter file = new StreamWriter(strFileName, true);

            file.WriteLine("\r\n" + strLine);

            file.Close();
        }
    }
}