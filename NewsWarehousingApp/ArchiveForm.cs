﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewsWarehousingApp
{
    public partial class ArchiveForm : Form
    {
        private const string VERSION = "20180222.1";

        private string NewsArchivePath;
        private string NewsMp4Path;
        string appPath;
        //private List<FtpServer> ftpsvr = null;

        public ArchiveForm()
        {
            InitializeComponent();

            CustomizedInit();
        }

        protected void CustomizedInit()
        {

            sslVersion.Text = "版本：" + VERSION;

            lblAppStartTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            NewsArchivePath = @"\\10.13.200.15\Archive";
            NewsMp4Path = @"\\10.13.200.15\Archive\MP4";

            appPath = Application.StartupPath;

            //using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            //{
            //    ftpsvr = DB.SysReference.Where(x => x.ItemType == "110").Select(x => new FtpServer { NewsType = x.ItemValue, IP = x.ItemName }).ToList();
            //}
            
            NetHelper.NetUse(@"\\10.13.200.15", "admin", "pts870701");
                        
        }

        protected override void OnLoad(EventArgs e)
        {
            lblArchive.Text = ArchiveTimer.Enabled.ToString();
            lblPreArchive.Text = PreArchiveTimer.Enabled.ToString();

            lblMainWork.Text = "Idle";
            lblWork1.Text = "Idle";
            lblWork2.Text = "Idle";
            lblPreWorker.Text = "Idle";
            
            base.OnLoad(e);

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (ArchiveTimer.Enabled || PreArchiveTimer.Enabled)
            {
                MessageBox.Show("還有排程在進行中，無法關閉!");
                e.Cancel = true;
            }

            if (FTPWorkerMain.IsBusy || FTPWorker1.IsBusy || FTPWorker2.IsBusy || PreArchiveWorker.IsBusy)
            {
                MessageBox.Show("還有背景程序在進行中，請稍後關閉!");
                e.Cancel = true;
            }

            base.OnClosing(e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ArchiveTimer.Enabled = !ArchiveTimer.Enabled;
            PreArchiveTimer.Enabled = !PreArchiveTimer.Enabled;

            lblArchive.Text = ArchiveTimer.Enabled.ToString();
            lblPreArchive.Text = PreArchiveTimer.Enabled.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!FTPWorkerMain.IsBusy)
            {
                textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : FTPWorkerMain_DoWork()\r\n");
                lblMainWork.Text = "Run";
                FTPWorkerMain.RunWorkerAsync();
                
            }
        }


        #region FTP入庫
        private void ArchiveTimer_Tick(object sender, EventArgs e)
        {
            bool mark = false;

            if (DateTime.Now.Hour == 02)
            {
                using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
                {
                    var now = DateTime.Now.ToString("yyyyMMdd");

                    var q = DB.ScheduleLog
                              .Where(x => x.ScheduleType == "Archive" && x.ScheduleDate == now)
                              .Select(x => x)
                              .FirstOrDefault();


                    if (q == null)
                    {
                        mark = true;

                        var log = new ScheduleLog();

                        log.ScheduleDate = now;
                        log.ScheduleTime = DateTime.Now;
                        log.ScheduleType = "Archive";

                        DB.ScheduleLog.Add(log);

                        DB.SaveChanges();
                    }

                }
            }


            if (mark)
            {
                if (!FTPWorkerMain.IsBusy)
                {
                    textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : FTPWorkerMain_DoWork()\r\n");
                    lblMainWork.Text = "Run";
                    FTPWorkerMain.RunWorkerAsync();
                }
            }

        }

        private void FTPWorkerMain_DoWork(object sender, DoWorkEventArgs e)
        {
            RunFTPSchedule((BackgroundWorker)sender);
        }
        
        private void FTPWorkerMain_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : FTPWorker" + e.ProgressPercentage.ToString() + "_DoWork()\r\n");
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : Download " + e.UserState.ToString() + "\r\n");

            switch (e.ProgressPercentage)
            {
                case 1:
                    lblWork1.Text = "Run";
                    break;
                case 2:
                    lblWork2.Text = "Run";
                    break;
            }

            
        }

        private void FTPWorkerMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblMainWork.Text = "Idle";
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : FTPWorkerMain_RunWorkerCompleted()\r\n");
        }

        void RunFTPSchedule(BackgroundWorker sender)
        {

            List<VideoMeta> results = null;

            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                results = DB.VideoMeta.Where(x => x.FileStstus == 0 && x.DeleteMark == false).Select(x => x).ToList();
            }

            if (results == null || results.Count == 0) return;

            var RemoveItems = new List<VideoMeta>();

            while (true)
            {
                foreach (var item in results)
                {
                    if (!FTPWorker1.IsBusy)
                    {
                        sender.ReportProgress(1, item.OriginalFile);
                        FTPWorker1.RunWorkerAsync(item);
                        RemoveItems.Add(item);
                        continue;
                    }

                    if (!FTPWorker2.IsBusy)
                    {
                        sender.ReportProgress(2, item.OriginalFile);
                        FTPWorker2.RunWorkerAsync(item);
                        RemoveItems.Add(item);
                        continue;
                    }

                }

                foreach (var item in RemoveItems)
                {
                    results.Remove(item);
                }

                RemoveItems.Clear();

                if (results.Count == 0) break;
            }

        }



        private void FTPWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var work = new FTPWork();
            work.Run((VideoMeta)e.Argument, appPath);
        }

        private void FTPWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblWork1.Text = "Idle";
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : FTPWorker1_RunWorkerCompleted()\r\n");
        }

        private void FTPWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            var work = new FTPWork();
            work.Run((VideoMeta)e.Argument, appPath);
        }

        private void FTPWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblWork2.Text = "Idle";
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : FTPWorker2_RunWorkerCompleted()\r\n");
        }

        #endregion

        private void PreArchiveTimer_Tick(object sender, EventArgs e)
        {
            if (!PreArchiveWorker.IsBusy)
            {
                textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : PreArchiveWorker.DoWork()\r\n");
                lblPreWorker.Text = "Run";
                PreArchiveWorker.RunWorkerAsync();
            }
        }

        private void PreArchiveWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var NewsDB = new PTSNewsWarehousingEntities();

            var items = NewsDB.PreArchive
                              .Where(x => x.ArchiveConfirm == true && x.ArchiveStatus == 1)
                              .Select(x => x).ToList();


            if (items.Count == 0) return;

            //var GVDB = new MediaFrameEntities();

            //var endDate = DateTime.Now.AddDays(-30);

            //var gvItems = GVDB.URN2URI
            //                  .Where(x => x.URIDevice == "SUB01A" || x.URIDevice == "SUB02A" || x.URIDevice == "SUBK2C31")
            //                  .Where(x => x.URILocation == "V:/ONAIR")
            //                  .Where(x => x.CreationDate >= endDate)
            //                  .OrderByDescending(x => x.CreationDate)
            //                  .Select(x => new { x.URN, x.URIAssetID, x.CreationDate, x.URIDevice })
            //                  .ToList();

            var GVDB = new GVDB();

            var device = NewsDB.SysReference
                               .Where(x => x.ItemType == "120" && x.IsExist == true)
                               .Select(x => x)
                               .ToDictionary(x => x.ItemValue, x => x.ItemName);

            foreach (var item in items)
            {
                var site = device[item.NewsType];
                var slogan = item.Slogan.Trim() + '_';

                //var match = gvItems.Where(x => x.URIAssetID.StartsWith(slogan))
                //                   .Where(x => x.URIDevice == site)
                //                   .OrderByDescending(x => x.CreationDate)
                //                   .Take(1)
                //                   .FirstOrDefault();

                var match = GVDB.Select(slogan, site);

                if (match == null)
                {
                    item.ArchiveStatus = 990;
                }
                else
                {
                    item.MXFName = match.MXFName;
                    item.MP4Name = match.MP4Name;

                    item.ArchiveStatus = 2;
                }
            }

            NewsDB.SaveChanges();

            NewsDB.Dispose();
            GVDB.Close();


            var help = new NewsHelper();
            help.NewsResolve();
        }

        private void PreArchiveWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : PreArchiveWorker.RunWorkerCompleted()\r\n");
            lblPreWorker.Text = "Idle";
        }
    }
}
