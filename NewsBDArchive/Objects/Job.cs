﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsBDArchive
{
    [Serializable]
    [System.Xml.Serialization.XmlRoot("job")]
    public class Job
    {
        public string id { get; set; }
        public string groupId { get; set; }
        public string type { get; set; }
        public string createTime { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string usersId { get; set; }
        public string status { get; set; }
        public string cancelable { get; set; }
        public string cancelRequired { get; set; }
        public string cartridgeId { get; set; }
        public string cartridgeTitle { get; set; }
        public string driveId { get; set; }
        public string fromFile { get; set; }
        public string fromFileAsPhysicalPath { get; set; }
        public string toFile { get; set; }
        public string toFileAsPhysicalPath { get; set; }
        public string catalogId { get; set; }
        public string catalogName { get; set; }
        public string isFileToBeDeleted { get; set; }
        public string progress { get; set; }
        public string jobOrder { get; set; }
        public string systemId { get; set; }
        public string errorKey { get; set; }
        public string errorMessage { get; set; }

        public Job() { }

        public Job(string id, string type, string name, string cartridge, string status, string progress, string usersId, string createTime, string startTime, string endTime)
        {

            this.id = id;
            this.type = type;
            this.catalogName = name;
            this.cartridgeTitle = cartridge;
            this.status = status;
            this.progress = progress;
            this.usersId = usersId;
            this.createTime = createTime;
            this.startTime = startTime;
            this.endTime = endTime;
        }

    }
}
