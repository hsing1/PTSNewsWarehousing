﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace PTS.NewsWarehousing.Utility
{
    public class NewsHelper
    {
        public string iNewsFileGet(string Source, string Target)
        {
            string retuenValue = "";

            try
            {
                ProcessStartInfo psi = new ProcessStartInfo(@"C:\Windows\System32\cmd");
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;

                Process process = Process.Start(psi);
                string strPath = Environment.CurrentDirectory;
                string cmdForTunnel = strPath + $"\\plink 10.1.163.1 -l newscg -pw p@ssw0rd \"/exc/doc -g `cat /list/{Source}`\" > {Target}.txt ";
                process.StandardInput.WriteLine(cmdForTunnel);
                //process.WaitForExit();
                Thread.Sleep(15000);

                //DoBusinessLogic();
                process.StandardInput.WriteLine("logout");
                Thread.Sleep(15000);

                if (process.HasExited)
                {
                    process.Close();
                    process.Dispose();
                }

                retuenValue = File.ReadAllText(Path.Combine(strPath, Target + ".txt"));
            }
            catch(Exception ex)
            {
                
            }

            return retuenValue;

        }

        public bool iNewsFileResolve(ref string Document, string DirName, string NewsTime, string NewsType)
        {
            string title = "", tmpDoc = "", storyid = "", body = "", slogan = "";

            var liData = new List<Dictionary<string, string>>();
            Dictionary<string, string> dicDetails;

            while (Document.Replace("\r\n", "").Length > 0)
            {
                tmpDoc = Document.Substring(0, Document.IndexOf("</nsml>") + 7);
                Document = Document.Substring(Document.IndexOf("</nsml>") + 7, Document.Length - Document.IndexOf("</nsml>") - 7);

                body = tmpDoc.Substring(tmpDoc.IndexOf("<body>"), tmpDoc.IndexOf("</body>") - (tmpDoc.IndexOf("<body>")));

                body = body.Replace("<pi>-1", String.Empty).Replace("<cc>-1", String.Empty);

                body = Regex.Replace(body, @"<[^>]*>", String.Empty);

                body = Regex.Replace(body, @"\[\[[^\]]+\]\]", String.Empty);

                body = Regex.Replace(body, @"\<[^\>]+\>", String.Empty);

                body = body.Replace("&&", String.Empty);

                body = body.Replace("##", String.Empty);

                body = body.Replace("&amp;", String.Empty);

                if (body.Replace(" ", "").Replace("\r\n", "").Length == 0) continue;

                dicDetails = new Dictionary<string, string>();

                dicDetails.Add("body", body);

                storyid = tmpDoc.Substring(tmpDoc.IndexOf("<storyid>") + 9, tmpDoc.IndexOf("</storyid>") - (tmpDoc.IndexOf("<storyid>") + 9));
                storyid = Regex.Replace(storyid, @"<[^>]*>", String.Empty);
                dicDetails.Add("storyid", storyid);

                slogan = tmpDoc.Substring(tmpDoc.IndexOf("\"title\">") + 8, tmpDoc.IndexOf("</string>", tmpDoc.IndexOf("\"title\">")) - (tmpDoc.IndexOf("\"title\">") + 8));
                slogan = Regex.Replace(slogan, @"<[^>]*>", String.Empty);
                dicDetails.Add("slogan", slogan);


                dicDetails.Add("title", title);
                dicDetails.Add("NewsDate", DateTime.Now.ToString("yyyyMMdd"));
                dicDetails.Add("NewsTime", NewsTime);
                dicDetails.Add("DirName", DirName);
                dicDetails.Add("NewsType", NewsType);
                liData.Add(dicDetails);

                List<FileData> liFileData = new List<FileData>();

                var mos = tmpDoc;

                while (true)
                {

                    if (mos.IndexOf("<mos>") == -1) break;

                    if (body.IndexOf("<clipPath>") == -1) break;

                    var start = body.IndexOf("<mos>");
                    var end = body.IndexOf("</mos>") + 6;

                    var obj = new FileData();

                    obj.mxfName = body.Substring(body.IndexOf("<clipPath>"), body.IndexOf("</clipPath>") + 11 - body.IndexOf("<clipPath>")).Replace("<clipPath>", "").Replace("</clipPath>", "");

                    obj.mp4Name = body.Substring(body.IndexOf("\"QT\">"), body.IndexOf("</objProxyPath>") + 15 - body.IndexOf("\"QT\">")).Replace("\"QT\">", "").Replace("</objProxyPath>", "");

                    mos = mos.Substring(end, mos.Length - end);

                }

            }



            return true;
        }
    }
}

class FileData
{
    public string mxfName { get; set; }
    public string mp4Name { get; set; }
}