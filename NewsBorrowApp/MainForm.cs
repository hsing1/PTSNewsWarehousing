﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PTS.News.Utilities;

namespace NewsBorrowApp
{
    public partial class MainForm : Form
    {

        private const string VERSION = "20161223.1";
        string NewsArchivePath, NewsMp4Path, NewsMp4Source, NewsMp4IISPath, NewsUploadPath, NewsPreConvertPath, NewsConvertPath, appPath;

        public MainForm()
        {
            InitializeComponent();

            CustomizedInit();
        }

        protected void CustomizedInit()
        {
            sslVersion.Text = "版本：" + VERSION;

            NewsArchivePath = @"\\10.13.200.15\Archive";
            NewsUploadPath = @"\\10.13.200.15\Upload";
            NewsPreConvertPath = @"\\10.13.200.15\PreConvert";
            NewsConvertPath = @"\\10.13.200.56";
            NewsMp4Source = @"\\10.13.200.56\WatchOut";
            NewsMp4Path = @"\\10.13.200.15\Archive\MP4";
            NewsMp4IISPath = @"\\10.13.200.58\MP4";

            appPath = Application.StartupPath;

            NetHelper.NetUse(@"\\10.13.200.15", "admin", "pts870701");

            NetHelper.NetUse(@"\\10.1.164.34", "pts", "pts2010");
            
        }

        protected override void OnLoad(EventArgs e)
        {
            BorrowSubWorker1.DoWork += BorrowSubWorker_DoWork;
            BorrowSubWorker2.DoWork += BorrowSubWorker_DoWork;
            BorrowSubWorker3.DoWork += BorrowSubWorker_DoWork;
            BorrowSubWorker4.DoWork += BorrowSubWorker_DoWork;

            BorrowSubWorker1.RunWorkerCompleted += BorrowSubWorker_RunWorkerCompleted;
            BorrowSubWorker2.RunWorkerCompleted += BorrowSubWorker_RunWorkerCompleted;
            BorrowSubWorker3.RunWorkerCompleted += BorrowSubWorker_RunWorkerCompleted;
            BorrowSubWorker4.RunWorkerCompleted += BorrowSubWorker_RunWorkerCompleted;

            lblTimer.Text = BorrowTimer.Enabled.ToString();
            lblMain.Text = "Idle";
            lblSub1.Text = "Idle";
            lblSub2.Text = "Idle";
            lblSub3.Text = "Idle";
            lblSub4.Text = "Idle";

            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (BorrowTimer.Enabled)
            {
                MessageBox.Show("還有排程在進行中，無法關閉!");
                e.Cancel = true;
            }

            if (BorrowMainWorker.IsBusy || BorrowSubWorker1.IsBusy || BorrowSubWorker2.IsBusy || BorrowSubWorker3.IsBusy || BorrowSubWorker4.IsBusy)
            {
                MessageBox.Show("還有背景程序在進行中，請稍後關閉!");
                e.Cancel = true;
            }

            base.OnClosing(e);
        }

        private void btnStopTimer_Click(object sender, EventArgs e)
        {
            BorrowTimer.Enabled = !BorrowTimer.Enabled;
            lblTimer.Text = BorrowTimer.Enabled.ToString();
        }

        private void BorrowTimer_Tick(object sender, EventArgs e)
        {
            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : BorrowTimer_Tick()\r\n");

            if (!BorrowMainWorker.IsBusy)
            {
                textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : BorrowMainWorker.RunWorkerAsync()\r\n");
                lblMain.Text = "Run";
                BorrowMainWorker.RunWorkerAsync();
            }
        }

        private void BorrowMainWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<vwBorrowList> results;

            using (PTSNewsWarehousingEntities DB = new PTSNewsWarehousingEntities())
            {
                results = DB.vwBorrowList
                            .Where(x => x.BorrowStatus == 1)
                            .Select(x => x)
                            .ToList();

            }

            if (results == null || results.Count == 0) return;

            var RemoveItems = new List<vwBorrowList>();

            while (true)
            {

                foreach (var item in results)
                {
                    if (!BorrowSubWorker1.IsBusy)
                    {
                        BorrowMainWorker.ReportProgress(1);
                        BorrowSubWorker1.RunWorkerAsync(new WorkerObject() { BorrowItem = item, WorkerId = 1 });
                        RemoveItems.Add(item);
                        continue;
                    }

                    if (!BorrowSubWorker2.IsBusy)
                    {
                        BorrowMainWorker.ReportProgress(2);
                        BorrowSubWorker2.RunWorkerAsync(new WorkerObject() { BorrowItem = item, WorkerId = 2 });
                        RemoveItems.Add(item);
                        continue;
                    }

                    if (!BorrowSubWorker3.IsBusy)
                    {
                        BorrowMainWorker.ReportProgress(3);
                        BorrowSubWorker3.RunWorkerAsync(new WorkerObject() { BorrowItem = item, WorkerId = 3 });
                        RemoveItems.Add(item);
                        continue;
                    }

                    if (!BorrowSubWorker4.IsBusy)
                    {
                        BorrowMainWorker.ReportProgress(4);
                        BorrowSubWorker4.RunWorkerAsync(new WorkerObject() { BorrowItem = item, WorkerId = 4 });
                        RemoveItems.Add(item);
                        continue;
                    }

                }

                foreach (var item in RemoveItems)
                {
                    results.Remove(item);
                }

                RemoveItems.Clear();

                if (results.Count == 0) break;
            }
        }
        
        private void BorrowMainWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 1:
                    lblSub1.Text = "Run";
                    break;
                case 2:
                    lblSub2.Text = "Run";
                    break;
                case 3:
                    lblSub3.Text = "Run";
                    break;
                case 4:
                    lblSub4.Text = "Run";
                    break;

            }

            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : BorrowSubWorker" + e.ProgressPercentage.ToString() + ".RunWorkerAsync()\r\n");
        }

        private void BorrowMainWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblMain.Text = "Idle";

            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : BorrowMainWorker_RunWorkerCompleted()\r\n");
        }

        private void BorrowSubWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var obj = (WorkerObject)e.Argument;

            var work = new BorrowProcess(obj.BorrowItem);
            work.Run();

            e.Result = obj.WorkerId;
        }

        private void BorrowSubWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch ((int)e.Result)
            {
                case 1:
                    lblSub1.Text = "Idle";
                    break;
                case 2:
                    lblSub2.Text = "Idle";
                    break;
                case 3:
                    lblSub3.Text = "Idle";
                    break;
                case 4:
                    lblSub4.Text = "Idle";
                    break;

            }

            textBox1.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : BorrowSubWorker" + e.Result.ToString() + ".RunWorkerCompleted()\r\n");
        }

    }
}

